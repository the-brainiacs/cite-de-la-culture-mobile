/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Date;

/**
 *
 * @author missaoui
 */
public class Theatre {

    
    private Integer id;
    private String genre;
    private Date anneeSortie;
    private String realisateurs;
    private String auteurs;
    private String artistes;

    public Theatre() {
    }

    public Theatre(Integer id) {
        this.id = id;
    }

    public Theatre(Integer id, String genre, Date anneeSortie, String realisateurs, String auteurs, String artistes) {
        this.id = id;
        this.genre = genre;
        this.anneeSortie = anneeSortie;
        this.realisateurs = realisateurs;
        this.auteurs = auteurs;
        this.artistes = artistes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Date getAnneeSortie() {
        return anneeSortie;
    }

    public void setAnneeSortie(Date anneeSortie) {
        this.anneeSortie = anneeSortie;
    }

    public String getRealisateurs() {
        return realisateurs;
    }

    public void setRealisateurs(String realisateurs) {
        this.realisateurs = realisateurs;
    }

    public String getAuteurs() {
        return auteurs;
    }

    public void setAuteurs(String auteurs) {
        this.auteurs = auteurs;
    }

    public String getArtistes() {
        return artistes;
    }

    public void setArtistes(String artistes) {
        this.artistes = artistes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Theatre)) {
            return false;
        }
        Theatre other = (Theatre) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.Theatre[ id=" + id + " ]";
    }
    
}
