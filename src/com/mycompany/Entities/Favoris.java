/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;


/**
 *
 * @author missaoui
 */
public class Favoris {

    private Integer id;
    private String categorie;
    private User idMembre;

    public Favoris() {
    }

    public Favoris(Integer id) {
        this.id = id;
    }

    public Favoris(Integer id, String categorie) {
        this.id = id;
        this.categorie = categorie;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public User getIdMembre() {
        return idMembre;
    }

    public void setIdMembre(User idMembre) {
        this.idMembre = idMembre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Favoris)) {
            return false;
        }
        Favoris other = (Favoris) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.Favoris[ id=" + id + " ]";
    }
    
}
