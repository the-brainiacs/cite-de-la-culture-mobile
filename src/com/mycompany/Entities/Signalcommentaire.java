/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

/**
 *
 * @author missaoui
 */
public class Signalcommentaire {

    
    private Integer id;
    private Commentaire idCommentaire;
    private User userId;

    public Signalcommentaire() {
    }

    public Signalcommentaire(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Commentaire getIdCommentaire() {
        return idCommentaire;
    }

    public void setIdCommentaire(Commentaire idCommentaire) {
        this.idCommentaire = idCommentaire;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Signalcommentaire)) {
            return false;
        }
        Signalcommentaire other = (Signalcommentaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.Signalcommentaire[ id=" + id + " ]";
    }
    
}
