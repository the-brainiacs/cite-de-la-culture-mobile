/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Collection;
import java.util.Date;

/**
 *
 * @author missaoui
 */
public class Commentaire {

    
    private Integer id;
    private String contenu;
    private Date dateCreation;
    private String etat;
    private int csignal;
    private Article articleId;
    private User userId;
    private Collection<Signalcommentaire> signalcommentaireCollection;
    private Collection<Avis> avisCollection;

    public Commentaire() {
    }

    public Commentaire(Integer id) {
        this.id = id;
    }

    public Commentaire(Integer id, String contenu, Date dateCreation, String etat, int csignal) {
        this.id = id;
        this.contenu = contenu;
        this.dateCreation = dateCreation;
        this.etat = etat;
        this.csignal = csignal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public int getCsignal() {
        return csignal;
    }

    public void setCsignal(int csignal) {
        this.csignal = csignal;
    }

    public Article getArticleId() {
        return articleId;
    }

    public void setArticleId(Article articleId) {
        this.articleId = articleId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Collection<Signalcommentaire> getSignalcommentaireCollection() {
        return signalcommentaireCollection;
    }

    public void setSignalcommentaireCollection(Collection<Signalcommentaire> signalcommentaireCollection) {
        this.signalcommentaireCollection = signalcommentaireCollection;
    }

    public Collection<Avis> getAvisCollection() {
        return avisCollection;
    }

    public void setAvisCollection(Collection<Avis> avisCollection) {
        this.avisCollection = avisCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commentaire)) {
            return false;
        }
        Commentaire other = (Commentaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.Commentaire[ id=" + id + " ]";
    }
    
}
