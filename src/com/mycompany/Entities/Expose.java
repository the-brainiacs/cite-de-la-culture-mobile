/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

/**
 *
 * @author missaoui
 */
public class Expose {
 
    private Integer id;
    private String locuteurs;
    private String theme;

    public Expose() {
    }

    public Expose(Integer id) {
        this.id = id;
    }

    public Expose(Integer id, String locuteurs, String theme) {
        this.id = id;
        this.locuteurs = locuteurs;
        this.theme = theme;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLocuteurs() {
        return locuteurs;
    }

    public void setLocuteurs(String locuteurs) {
        this.locuteurs = locuteurs;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Expose)) {
            return false;
        }
        Expose other = (Expose) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.Expose[ id=" + id + " ]";
    }
    
}
