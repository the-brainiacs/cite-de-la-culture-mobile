/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Date;

/**
 *
 * @author missaoui
 */
public class Demandeempreint {

    private Integer id;
    private int idlivre;
    private Date dateDemande;
    private User idmembre;

    public Demandeempreint() {
    }

    public Demandeempreint(Integer id) {
        this.id = id;
    }

    public Demandeempreint(Integer id, int idlivre, Date dateDemande) {
        this.id = id;
        this.idlivre = idlivre;
        this.dateDemande = dateDemande;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdlivre() {
        return idlivre;
    }

    public void setIdlivre(int idlivre) {
        this.idlivre = idlivre;
    }

    public Date getDateDemande() {
        return dateDemande;
    }

    public void setDateDemande(Date dateDemande) {
        this.dateDemande = dateDemande;
    }

    public User getIdmembre() {
        return idmembre;
    }

    public void setIdmembre(User idmembre) {
        this.idmembre = idmembre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Demandeempreint)) {
            return false;
        }
        Demandeempreint other = (Demandeempreint) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.Demandeempreint[ id=" + id + " ]";
    }
    
}
