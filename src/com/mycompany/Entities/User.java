/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author missaoui
 */
public class User {

    
    private Integer id;
    private String username;
    private String usernameCanonical;
    private String email;
    private String emailCanonical;
    private boolean enabled;
    private String salt;
    private String password;
    private Date lastLogin;
    private String confirmationToken;
    private Date passwordRequestedAt;
    private ArrayList roles;
    private String nom;
    private String prenom;
    private String img;
    private String bio;
    private String imgCouv;
    private Collection<Evaluation> evaluationCollection;
    private Collection<Demandesponsoring> demandesponsoringCollection;
    private Collection<Evenementevaluation> evenementevaluationCollection;
    private Collection<Inscription> inscriptionCollection;
    private Collection<Reservation> reservationCollection;
    private Collection<ThreadMetadata> threadMetadataCollection;
    private Collection<Favoris> favorisCollection;
    private Collection<Commentaire> commentaireCollection;
    private Collection<Empreint> empreintCollection;
    private Collection<Signalcommentaire> signalcommentaireCollection;
    private Collection<UserEvent> userEventCollection;
    private Collection<Thread> threadCollection;
    private Collection<Message> messageCollection;
    private Collection<MessageMetadata> messageMetadataCollection;
    private Collection<Avis> avisCollection;
    private Collection<Offresponsoring> offresponsoringCollection;
    private Collection<Demandeempreint> demandeempreintCollection;
    private Collection<Evenement> evenementCollection;

    public User() {
    }

    public User(Integer id) {
        this.id = id;
    }

    public User(Integer id, String username, String usernameCanonical, String email, String emailCanonical, boolean enabled, String password, ArrayList roles, String nom, String prenom) {
        this.id = id;
        this.username = username;
        this.usernameCanonical = usernameCanonical;
        this.email = email;
        this.emailCanonical = emailCanonical;
        this.enabled = enabled;
        this.password = password;
        this.roles = roles;
        this.nom = nom;
        this.prenom = prenom;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsernameCanonical() {
        return usernameCanonical;
    }

    public void setUsernameCanonical(String usernameCanonical) {
        this.usernameCanonical = usernameCanonical;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailCanonical() {
        return emailCanonical;
    }

    public void setEmailCanonical(String emailCanonical) {
        this.emailCanonical = emailCanonical;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public Date getPasswordRequestedAt() {
        return passwordRequestedAt;
    }

    public void setPasswordRequestedAt(Date passwordRequestedAt) {
        this.passwordRequestedAt = passwordRequestedAt;
    }

    public ArrayList getRoles() {
        return roles;
    }

    public void setRoles(ArrayList roles) {
        this.roles = roles;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getImgCouv() {
        return imgCouv;
    }

    public void setImgCouv(String imgCouv) {
        this.imgCouv = imgCouv;
    }

    public Collection<Evaluation> getEvaluationCollection() {
        return evaluationCollection;
    }

    public void setEvaluationCollection(Collection<Evaluation> evaluationCollection) {
        this.evaluationCollection = evaluationCollection;
    }

    public Collection<Demandesponsoring> getDemandesponsoringCollection() {
        return demandesponsoringCollection;
    }

    public void setDemandesponsoringCollection(Collection<Demandesponsoring> demandesponsoringCollection) {
        this.demandesponsoringCollection = demandesponsoringCollection;
    }

    public Collection<Evenementevaluation> getEvenementevaluationCollection() {
        return evenementevaluationCollection;
    }

    public void setEvenementevaluationCollection(Collection<Evenementevaluation> evenementevaluationCollection) {
        this.evenementevaluationCollection = evenementevaluationCollection;
    }

    public Collection<Inscription> getInscriptionCollection() {
        return inscriptionCollection;
    }

    public void setInscriptionCollection(Collection<Inscription> inscriptionCollection) {
        this.inscriptionCollection = inscriptionCollection;
    }

    public Collection<Reservation> getReservationCollection() {
        return reservationCollection;
    }

    public void setReservationCollection(Collection<Reservation> reservationCollection) {
        this.reservationCollection = reservationCollection;
    }

    public Collection<ThreadMetadata> getThreadMetadataCollection() {
        return threadMetadataCollection;
    }

    public void setThreadMetadataCollection(Collection<ThreadMetadata> threadMetadataCollection) {
        this.threadMetadataCollection = threadMetadataCollection;
    }

    public Collection<Favoris> getFavorisCollection() {
        return favorisCollection;
    }

    public void setFavorisCollection(Collection<Favoris> favorisCollection) {
        this.favorisCollection = favorisCollection;
    }

    public Collection<Commentaire> getCommentaireCollection() {
        return commentaireCollection;
    }

    public void setCommentaireCollection(Collection<Commentaire> commentaireCollection) {
        this.commentaireCollection = commentaireCollection;
    }

    public Collection<Empreint> getEmpreintCollection() {
        return empreintCollection;
    }

    public void setEmpreintCollection(Collection<Empreint> empreintCollection) {
        this.empreintCollection = empreintCollection;
    }

    public Collection<Signalcommentaire> getSignalcommentaireCollection() {
        return signalcommentaireCollection;
    }

    public void setSignalcommentaireCollection(Collection<Signalcommentaire> signalcommentaireCollection) {
        this.signalcommentaireCollection = signalcommentaireCollection;
    }

    public Collection<UserEvent> getUserEventCollection() {
        return userEventCollection;
    }

    public void setUserEventCollection(Collection<UserEvent> userEventCollection) {
        this.userEventCollection = userEventCollection;
    }

    public Collection<Thread> getThreadCollection() {
        return threadCollection;
    }

    public void setThreadCollection(Collection<Thread> threadCollection) {
        this.threadCollection = threadCollection;
    }

    public Collection<Message> getMessageCollection() {
        return messageCollection;
    }

    public void setMessageCollection(Collection<Message> messageCollection) {
        this.messageCollection = messageCollection;
    }

    public Collection<MessageMetadata> getMessageMetadataCollection() {
        return messageMetadataCollection;
    }

    public void setMessageMetadataCollection(Collection<MessageMetadata> messageMetadataCollection) {
        this.messageMetadataCollection = messageMetadataCollection;
    }

    public Collection<Avis> getAvisCollection() {
        return avisCollection;
    }

    public void setAvisCollection(Collection<Avis> avisCollection) {
        this.avisCollection = avisCollection;
    }

    public Collection<Offresponsoring> getOffresponsoringCollection() {
        return offresponsoringCollection;
    }

    public void setOffresponsoringCollection(Collection<Offresponsoring> offresponsoringCollection) {
        this.offresponsoringCollection = offresponsoringCollection;
    }

    public Collection<Demandeempreint> getDemandeempreintCollection() {
        return demandeempreintCollection;
    }

    public void setDemandeempreintCollection(Collection<Demandeempreint> demandeempreintCollection) {
        this.demandeempreintCollection = demandeempreintCollection;
    }

    public Collection<Evenement> getEvenementCollection() {
        return evenementCollection;
    }

    public void setEvenementCollection(Collection<Evenement> evenementCollection) {
        this.evenementCollection = evenementCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", usernameCanonical=" + usernameCanonical + ", email=" + email + ", emailCanonical=" + emailCanonical + ", enabled=" + enabled + ", salt=" + salt + ", password=" + password + ", lastLogin=" + lastLogin + ", confirmationToken=" + confirmationToken + ", passwordRequestedAt=" + passwordRequestedAt + ", roles=" + roles + ", nom=" + nom + ", prenom=" + prenom + ", img=" + img + ", bio=" + bio + ", imgCouv=" + imgCouv + '}';
    }
    
}
