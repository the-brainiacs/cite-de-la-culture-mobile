/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;


/**
 *
 * @author missaoui
 */
public class Demandesponsoring {

    
    private Integer id;
    private String description;
    private String type;
    private String etat;
    private String package1;
    private Evenement idEvent;
    private User idUser;

    public Demandesponsoring() {
    }

    public Demandesponsoring(Integer id) {
        this.id = id;
    }

    public Demandesponsoring(Integer id, String description, String type, String etat, String package1) {
        this.id = id;
        this.description = description;
        this.type = type;
        this.etat = etat;
        this.package1 = package1;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getPackage1() {
        return package1;
    }

    public void setPackage1(String package1) {
        this.package1 = package1;
    }

    public Evenement getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(Evenement idEvent) {
        this.idEvent = idEvent;
    }

    public User getIdUser() {
        return idUser;
    }

    public void setIdUser(User idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Demandesponsoring)) {
            return false;
        }
        Demandesponsoring other = (Demandesponsoring) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.Demandesponsoring[ id=" + id + " ]";
    }
    
}
