/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Date;

/**
 *
 * @author missaoui
 */
public class PaiementCarte {

    
    private Integer id;
    private String typeCarte;
    private Date dateExpiration;
    private double somme;
    private String numeroCarte;
    private Facture facture;

    public PaiementCarte() {
    }

    public PaiementCarte(Integer id) {
        this.id = id;
    }

    public PaiementCarte(Integer id, String typeCarte, Date dateExpiration, double somme, String numeroCarte) {
        this.id = id;
        this.typeCarte = typeCarte;
        this.dateExpiration = dateExpiration;
        this.somme = somme;
        this.numeroCarte = numeroCarte;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypeCarte() {
        return typeCarte;
    }

    public void setTypeCarte(String typeCarte) {
        this.typeCarte = typeCarte;
    }

    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public double getSomme() {
        return somme;
    }

    public void setSomme(double somme) {
        this.somme = somme;
    }

    public String getNumeroCarte() {
        return numeroCarte;
    }

    public void setNumeroCarte(String numeroCarte) {
        this.numeroCarte = numeroCarte;
    }

    public Facture getFacture() {
        return facture;
    }

    public void setFacture(Facture facture) {
        this.facture = facture;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaiementCarte)) {
            return false;
        }
        PaiementCarte other = (PaiementCarte) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.PaiementCarte[ id=" + id + " ]";
    }
    
}
