/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Collection;
import java.util.Date;

/**
 *
 * @author missaoui
 */
public class Club {

    
    private Integer id;
    private String nom;
    private String genre;
    private Date dateCreation;
    private String groupeAge;
    private double prixInscri;
    private String description;
    private String etat;
    private String president;
    private int nbrParticipant;
    private double budget;
    private int seanceSemaine;
    private int dureeSeance;
    private Salle idSalle;
    private Collection<Inscription> inscriptionCollection;
    private Collection<Workshop> workshopCollection;

    public Club() {
    }

    public Club(Integer id) {
        this.id = id;
    }

    public Club(Integer id, String nom, String genre, Date dateCreation, String groupeAge, double prixInscri, String description, String etat, String president, int nbrParticipant, double budget, int seanceSemaine, int dureeSeance) {
        this.id = id;
        this.nom = nom;
        this.genre = genre;
        this.dateCreation = dateCreation;
        this.groupeAge = groupeAge;
        this.prixInscri = prixInscri;
        this.description = description;
        this.etat = etat;
        this.president = president;
        this.nbrParticipant = nbrParticipant;
        this.budget = budget;
        this.seanceSemaine = seanceSemaine;
        this.dureeSeance = dureeSeance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getGroupeAge() {
        return groupeAge;
    }

    public void setGroupeAge(String groupeAge) {
        this.groupeAge = groupeAge;
    }

    public double getPrixInscri() {
        return prixInscri;
    }

    public void setPrixInscri(double prixInscri) {
        this.prixInscri = prixInscri;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getPresident() {
        return president;
    }

    public void setPresident(String president) {
        this.president = president;
    }

    public int getNbrParticipant() {
        return nbrParticipant;
    }

    public void setNbrParticipant(int nbrParticipant) {
        this.nbrParticipant = nbrParticipant;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public int getSeanceSemaine() {
        return seanceSemaine;
    }

    public void setSeanceSemaine(int seanceSemaine) {
        this.seanceSemaine = seanceSemaine;
    }

    public int getDureeSeance() {
        return dureeSeance;
    }

    public void setDureeSeance(int dureeSeance) {
        this.dureeSeance = dureeSeance;
    }

    public Salle getIdSalle() {
        return idSalle;
    }

    public void setIdSalle(Salle idSalle) {
        this.idSalle = idSalle;
    }

    public Collection<Inscription> getInscriptionCollection() {
        return inscriptionCollection;
    }

    public void setInscriptionCollection(Collection<Inscription> inscriptionCollection) {
        this.inscriptionCollection = inscriptionCollection;
    }

    public Collection<Workshop> getWorkshopCollection() {
        return workshopCollection;
    }

    public void setWorkshopCollection(Collection<Workshop> workshopCollection) {
        this.workshopCollection = workshopCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Club)) {
            return false;
        }
        Club other = (Club) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.Club[ id=" + id + " ]";
    }
    
}
