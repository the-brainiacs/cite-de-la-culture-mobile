/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Date;

/**
 *
 * @author missaoui
 */
public class Empreint {

    
    private Integer id;
    private Date dateretour;
    private int idlivre;
    private User idmembre;

    public Empreint() {
    }

    public Empreint(Integer id) {
        this.id = id;
    }

    public Empreint(Integer id, Date dateretour, int idlivre) {
        this.id = id;
        this.dateretour = dateretour;
        this.idlivre = idlivre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateretour() {
        return dateretour;
    }

    public void setDateretour(Date dateretour) {
        this.dateretour = dateretour;
    }

    public int getIdlivre() {
        return idlivre;
    }

    public void setIdlivre(int idlivre) {
        this.idlivre = idlivre;
    }

    public User getIdmembre() {
        return idmembre;
    }

    public void setIdmembre(User idmembre) {
        this.idmembre = idmembre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empreint)) {
            return false;
        }
        Empreint other = (Empreint) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.Empreint[ id=" + id + " ]";
    }
    
}
