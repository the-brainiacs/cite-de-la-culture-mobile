/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;


/**
 *
 * @author missaoui
 */
public class Festival {

    
    private Integer id;
    private String participants;
    private String typeFestival;
    private String genreFestival;

    public Festival() {
    }

    public Festival(Integer id) {
        this.id = id;
    }

    public Festival(Integer id, String participants, String typeFestival, String genreFestival) {
        this.id = id;
        this.participants = participants;
        this.typeFestival = typeFestival;
        this.genreFestival = genreFestival;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParticipants() {
        return participants;
    }

    public void setParticipants(String participants) {
        this.participants = participants;
    }

    public String getTypeFestival() {
        return typeFestival;
    }

    public void setTypeFestival(String typeFestival) {
        this.typeFestival = typeFestival;
    }

    public String getGenreFestival() {
        return genreFestival;
    }

    public void setGenreFestival(String genreFestival) {
        this.genreFestival = genreFestival;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Festival)) {
            return false;
        }
        Festival other = (Festival) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.Festival[ id=" + id + " ]";
    }
    
}
