/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Collection;

/**
 *
 * @author missaoui
 */
public class Salle {

    
    private Integer id;
    private String nom;
    private int capacite;
    private double coutReservationHeure;
    private String type;
    private Collection<Club> clubCollection;
    private Collection<Workshop> workshopCollection;
    private Collection<Evenement> evenementCollection;

    public Salle() {
    }

    public Salle(Integer id) {
        this.id = id;
    }

    public Salle(Integer id, String nom, int capacite, double coutReservationHeure, String type) {
        this.id = id;
        this.nom = nom;
        this.capacite = capacite;
        this.coutReservationHeure = coutReservationHeure;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getCapacite() {
        return capacite;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    public double getCoutReservationHeure() {
        return coutReservationHeure;
    }

    public void setCoutReservationHeure(double coutReservationHeure) {
        this.coutReservationHeure = coutReservationHeure;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Collection<Club> getClubCollection() {
        return clubCollection;
    }

    public void setClubCollection(Collection<Club> clubCollection) {
        this.clubCollection = clubCollection;
    }

    public Collection<Workshop> getWorkshopCollection() {
        return workshopCollection;
    }

    public void setWorkshopCollection(Collection<Workshop> workshopCollection) {
        this.workshopCollection = workshopCollection;
    }

    public Collection<Evenement> getEvenementCollection() {
        return evenementCollection;
    }

    public void setEvenementCollection(Collection<Evenement> evenementCollection) {
        this.evenementCollection = evenementCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Salle)) {
            return false;
        }
        Salle other = (Salle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nom;
    }
    
}
