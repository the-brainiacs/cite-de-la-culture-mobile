/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

/**
 *
 * @author missaoui
 */
public class Avis {

    private Integer id;
    private String notes;
    private User idMembre;
    private Commentaire commentaireId;

    public Avis() {
    }

    public Avis(Integer id) {
        this.id = id;
    }

    public Avis(Integer id, String notes) {
        this.id = id;
        this.notes = notes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public User getIdMembre() {
        return idMembre;
    }

    public void setIdMembre(User idMembre) {
        this.idMembre = idMembre;
    }

    public Commentaire getCommentaireId() {
        return commentaireId;
    }

    public void setCommentaireId(Commentaire commentaireId) {
        this.commentaireId = commentaireId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Avis)) {
            return false;
        }
        Avis other = (Avis) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.Avis[ id=" + id + " ]";
    }
    
}
