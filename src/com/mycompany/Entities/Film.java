/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Date;

/**
 *
 * @author missaoui
 */
public class Film {

    private Integer id;
    private int duree;
    private String genre;
    private Date anneeSortie;
    private String realisateurs;
    private String acteurs;

    public Film() {
    }

    public Film(Integer id) {
        this.id = id;
    }

    public Film(Integer id, int duree, String genre, Date anneeSortie, String realisateurs, String acteurs) {
        this.id = id;
        this.duree = duree;
        this.genre = genre;
        this.anneeSortie = anneeSortie;
        this.realisateurs = realisateurs;
        this.acteurs = acteurs;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Date getAnneeSortie() {
        return anneeSortie;
    }

    public void setAnneeSortie(Date anneeSortie) {
        this.anneeSortie = anneeSortie;
    }

    public String getRealisateurs() {
        return realisateurs;
    }

    public void setRealisateurs(String realisateurs) {
        this.realisateurs = realisateurs;
    }

    public String getActeurs() {
        return acteurs;
    }

    public void setActeurs(String acteurs) {
        this.acteurs = acteurs;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Film)) {
            return false;
        }
        Film other = (Film) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.citedelaculturerestserver.Film[ id=" + id + " ]";
    }
    
}
