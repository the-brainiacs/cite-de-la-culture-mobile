/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entities;

import java.util.Collection;
import java.util.Date;

/**
 *
 * @author missaoui
 */
public class Evenement {

    
    private Integer id;
    private String nom;
    private Date dateDebut;
    private Date dateFin;
    private int disponibles;
    private double coutReservation;
    private String age;
    private String type;
    private double prix;
    private double prixEtudiants;
    private String description;
    private String etat;
    private String imageId;
    private Collection<Demandesponsoring> demandesponsoringCollection;
    private Collection<Evenementevaluation> evenementevaluationCollection;
    private Collection<Reservation> reservationCollection;
    private Collection<Offresponsoring> offresponsoringCollection;
    private User userId;
    private Salle salleId;

    public Evenement() {
    }

    public Evenement(Integer id) {
        this.id = id;
    }

    public Evenement(Integer id, String nom, Date dateDebut, Date dateFin, int disponibles, double coutReservation, String age, String type, double prix, double prixEtudiants, String description, String etat, String imageId) {
        this.id = id;
        this.nom = nom;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.disponibles = disponibles;
        this.coutReservation = coutReservation;
        this.age = age;
        this.type = type;
        this.prix = prix;
        this.prixEtudiants = prixEtudiants;
        this.description = description;
        this.etat = etat;
        this.imageId = imageId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public int getDisponibles() {
        return disponibles;
    }

    public void setDisponibles(int disponibles) {
        this.disponibles = disponibles;
    }

    public double getCoutReservation() {
        return coutReservation;
    }

    public void setCoutReservation(double coutReservation) {
        this.coutReservation = coutReservation;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public double getPrixEtudiants() {
        return prixEtudiants;
    }

    public void setPrixEtudiants(double prixEtudiants) {
        this.prixEtudiants = prixEtudiants;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public Collection<Demandesponsoring> getDemandesponsoringCollection() {
        return demandesponsoringCollection;
    }

    public void setDemandesponsoringCollection(Collection<Demandesponsoring> demandesponsoringCollection) {
        this.demandesponsoringCollection = demandesponsoringCollection;
    }

    public Collection<Evenementevaluation> getEvenementevaluationCollection() {
        return evenementevaluationCollection;
    }

    public void setEvenementevaluationCollection(Collection<Evenementevaluation> evenementevaluationCollection) {
        this.evenementevaluationCollection = evenementevaluationCollection;
    }

    public Collection<Reservation> getReservationCollection() {
        return reservationCollection;
    }

    public void setReservationCollection(Collection<Reservation> reservationCollection) {
        this.reservationCollection = reservationCollection;
    }

    public Collection<Offresponsoring> getOffresponsoringCollection() {
        return offresponsoringCollection;
    }

    public void setOffresponsoringCollection(Collection<Offresponsoring> offresponsoringCollection) {
        this.offresponsoringCollection = offresponsoringCollection;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Salle getSalleId() {
        return salleId;
    }

    public void setSalleId(Salle salleId) {
        this.salleId = salleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evenement)) {
            return false;
        }
        Evenement other = (Evenement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Evenement{" + "id=" + id + ", nom=" + nom + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", disponibles=" + disponibles + ", coutReservation=" + coutReservation + ", age=" + age + ", type=" + type + ", prix=" + prix + ", prixEtudiants=" + prixEtudiants + ", description=" + description + ", etat=" + etat + ", imageId=" + imageId + ", userId=" + userId + ", salleId=" + salleId + '}';
    }
    
}
