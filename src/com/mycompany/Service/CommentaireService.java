/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Label;
import com.codename1.util.StringUtil;
import com.mycompany.Entities.Article;
import com.mycompany.Entities.Commentaire;
import com.mycompany.Entities.User;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ASUS
 */
public class CommentaireService {

    public List<Commentaire> AfficherCommentaire(int id) {

        List<Commentaire> la = new ArrayList<>();
        String url = "http://localhost/PIDEV/web/app_dev.php/api/commentaire/byArticle/" + id;

        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);

        if (!lb.getText().equals("[]")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Commentaire e = new Commentaire();
                    e.setId(((Double) r.get("id")).intValue());
                    e.setCsignal(((Double) r.get("cSignal")).intValue());
                    e.setContenu((String) r.get("contenu"));
                    e.setEtat((String) r.get("etat"));
                    Article art = new Article();
                    art.setId(id);
                    e.setArticleId(art);
                    Date d1 = new Date();
                    Map<String, Object> dd = (Map<String, Object>) r.get("dateCreation");
                    d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                    e.setDateCreation(d1);

                    Map<String, Object> uu = (Map<String, Object>) r.get("user");
                    User u = new User();
                    u.setId(((Double) uu.get("id")).intValue());
                    u.setUsername((String) uu.get("username"));
                    u.setNom((String) uu.get("nom"));
                    u.setEmail((String) uu.get("email"));
                    u.setPrenom((String) uu.get("prenom"));
                    u.setImg((String) uu.get("Img"));
                    u.setImgCouv((String) uu.get("ImgCouv"));
                    u.setBio((String) uu.get("bio"));
                    u.setRoles((ArrayList) uu.get("roles"));
                    e.setUserId(u);

                    la.add(e);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return la;
    }

    public void ajouterCommentaire(String contenu, int id, Article article) {
        ConnectionRequest con = new ConnectionRequest();
        String url = "http://localhost/PIDEV/web/app_dev.php/api/commentaire/ajouter/" + article.getId() + "?contenu=" + contenu + "&user_id=" + id;
        url = StringUtil.replaceAll(url, " ", "%20");
        con.setUrl(url);
        con.setHttpMethod("GET");

        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }

    public void SupprimerCommentaire(int id) {
        ConnectionRequest con = new ConnectionRequest();
        String url = "http://localhost/PIDEV/web/app_dev.php/api/commentaire/supprimer/" + id;
        con.setUrl(url);
        con.setHttpMethod("GET");

        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }

    public int like(Commentaire c, int id) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/article/like/" + c.getId() + "?user_id=" + id;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);

        int nombre = 0;
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));

                nombre = (((Double) r.get("likes")).intValue());

            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return nombre;
    }

    public void signal(Commentaire c, int id) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/commentaire/signaler/" + c.getId() + "?user_id=1";
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);

        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));

            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    public int Nombrelike(Commentaire c) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/article/nombre/" + c.getId();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);

        int nombre = 0;
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));

                nombre = (((Double) r.get("likes")).intValue());

            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return nombre;
    }

    public int NombreSignal(Commentaire c, int id) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/commentaire/nombresignal/" + c.getId() + "?user_id=1";
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);

        int nombre = 0;
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));

                nombre = (((Double) r.get("likes")).intValue());

            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return nombre;
    }

}
