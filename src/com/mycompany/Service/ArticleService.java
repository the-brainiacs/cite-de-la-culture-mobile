/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Label;
import com.codename1.util.StringUtil;
import com.mycompany.Entities.Article;
import com.mycompany.Entities.User;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ASUS
 */
public class ArticleService {

    public List<Article> AfficherArticles() {

        List<Article> la = new ArrayList<>();

        String url = "http://localhost/PIDEV/web/app_dev.php/api/article/all";
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);

        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Article e = new Article();
                    e.setId(((Double) r.get("id")).intValue());
                    e.setTitre((String) r.get("titre"));
                    e.setContenu((String) r.get("contenu"));

                    e.setImage((String) r.get("image"));
                    Date d1 = new Date();
                    Map<String, Object> dd = (Map<String, Object>) r.get("dateCreation");
                    d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                    e.setDateCreation(d1);

                    la.add(e);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return la;

    }

    public List<Article> rechercheArticles(String pnom) {

        List<Article> la = new ArrayList<>();

        String url = "http://localhost/PIDEV/web/app_dev.php/api/article/recherche?rech=" + pnom;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);

        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Article e = new Article();
                    e.setId(((Double) r.get("id")).intValue());
                    e.setTitre((String) r.get("nom"));
                    e.setContenu((String) r.get("description"));

                    e.setImage((String) r.get("etat"));
                    Date d1 = new Date();
//                    Map<String, Object> dd = (Map<String, Object>) r.get("dateDebut");
                    //                   d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                    //                 e.setDateCreation(d1);

                    la.add(e);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return la;

    }

    public Article getArticle(int id) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/article/byId/" + id;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Article e = new Article();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));

                e.setId(((Double) r.get("id")).intValue());
                e.setTitre((String) r.get("titre"));
                e.setContenu((String) r.get("contenu"));

                e.setImage((String) r.get("image"));
                Date d1 = new Date();
                Map<String, Object> dd = (Map<String, Object>) r.get("dateCreation");
                d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                e.setDateCreation(d1);

            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return e;
    }

}
