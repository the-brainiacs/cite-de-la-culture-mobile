/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Label;
import com.mycompany.Entities.Club;
import com.mycompany.Entities.Workshop;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author kahou
 */
public class WorkshopService {
    
    public List<Workshop> getAllWorkshops(){
        
         String url = "http://localhost/Pidev/web/app_dev.php/api/workshop/afficherAll";
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(ev-> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        }
        );
        NetworkManager.getInstance().addToQueueAndWait(con);
        List<Workshop> lw = new ArrayList<>();

        if (!lb.getText().equals("[]")) 
        {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Workshop w = new Workshop();
                     w.setId(((Double) r.get("id")).intValue());
                    Date d1 = new Date();
                    Map<String, Object> dd = (Map<String, Object>) r.get("dateDebut");
                    d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                    w.setDateDebut(d1);
                     Date d2 = new Date();
                    Map<String, Object> df = (Map<String, Object>) r.get("dateFin");
                    d1.setTime(((Double) df.get("timestamp")).longValue() * 1000);
                    w.setDateDebut(d2);
                    w.setNbParticipant(((Double)r.get("nbParticipant")).intValue());
                    w.setDescription((String)r.get("description"));
                    
                    lw.add(w);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return lw;
    
    
        
    }
    
}
