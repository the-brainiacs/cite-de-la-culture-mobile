/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Label;
import com.codename1.util.StringUtil;
import com.mycompany.Entities.Evenement;
import com.mycompany.Entities.Salle;
import com.mycompany.Entities.User;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author missaoui
 */
public class SalleService {
    public List<Salle> getSalles(int disponibles, Date dd, Date df, String type) {
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d H:mm");
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/get-salle-from-evenement?type="+type+"&disponibles="+disponibles+"&dateDebut="+sdf.format(dd)+"&dateFin="+sdf.format(df);
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        List<Salle> ls = new ArrayList<>();
        if (!lb.getText().equals("[]")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Salle s = new Salle();
                    s.setId(((Double) r.get("id")).intValue());
                    s.setNom((String) r.get("nom"));
                    s.setType((String) r.get("type"));
                    s.setCapacite(((Double) r.get("capacite")).intValue());
                    ls.add(s);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return ls;
    }
}
