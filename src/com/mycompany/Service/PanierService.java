/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Label;
import com.codename1.util.StringUtil;
import com.mycompany.Entities.Evenement;
import com.mycompany.Entities.Reservation;
import com.mycompany.Entities.Salle;
import com.mycompany.Entities.User;
import com.mycompany.MyApplication;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author azus
 */
public class PanierService {
     public List<Reservation> getAllReservations() {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/byId/"+MyApplication.CurrentUser.getId();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        List<Reservation> le = new ArrayList<>();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Reservation re = new Reservation();
                    re.setId(((Double) r.get("id")).intValue());
                    re.setQuantite(((Double) r.get("quantite")).intValue());
                    re.setPrix(((Double) r.get("prix")).intValue());

                    Date d1 = new Date();
                    Map<String, Object> dd = (Map<String, Object>) r.get("dateReservation");
                    d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                    re.setDateReservation(d1);
                   
                    Map<String, Object> e = (Map<String, Object>) r.get("Evenement");
                    Evenement event = new Evenement();
                    event.setId(((Double) e.get("id")).intValue());
                    event.setPrix((Double) e.get("prix"));
                    event.setNom((String) e.get("nom"));
                    event.setDescription((String) e.get("description"));
                    event.setImageId((String) e.get("img"));
                    re.setIdEvent(event);
                    Map<String, Object> uu = (Map<String, Object>) r.get("user");
                    User u = new User();
                    u.setId(((Double) uu.get("id")).intValue());
                    u.setUsername((String) uu.get("username"));
                    u.setNom((String) uu.get("nom"));
                    u.setEmail((String) uu.get("email"));
                    u.setPrenom((String) uu.get("prenom"));
                    u.setImg((String) uu.get("Img"));
                    u.setImgCouv((String) uu.get("ImgCouv"));
                    u.setBio((String) uu.get("bio"));
                    u.setRoles((ArrayList) uu.get("roles"));
                    re.setIdUser(u);
                    le.add(re);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return le;
    }
     
    /* public void supprimer(int R){
   
        String url = "http://localhost/PIDEV/web/app_dev.php/api/Supprimer/" + R;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    

     
     }*/
      public void supprimer(int R){
        ConnectionRequest con = new ConnectionRequest();

       con.setUrl("http://localhost/PIDEV/web/app_dev.php/api/Supprimer/" + R);
       
        NetworkManager.getInstance().addToQueueAndWait(con);
    

     
     }
     
      public void modifier(int Q,int qt){
   
        String url = "http://localhost/PIDEV/web/app_dev.php/api/Modifier/" +Q +"?quantite="+qt;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        System.out.println(lb.getText());
        NetworkManager.getInstance().addToQueueAndWait(con);

    

     
     }
     
    
}
