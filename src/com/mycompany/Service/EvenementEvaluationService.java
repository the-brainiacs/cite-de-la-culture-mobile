/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Label;
import com.codename1.util.StringUtil;
import com.mycompany.Entities.Evenement;
import com.mycompany.Entities.Salle;
import com.mycompany.Entities.Evenementevaluation;
import com.mycompany.Entities.User;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author missaoui
 */
public class EvenementEvaluationService {

    public Evenementevaluation getEvaluation(int evid, int usid) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/getEvaluation?evid=" + evid + "&usid=" + usid;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Evenementevaluation ee = null;
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                ee = new Evenementevaluation();
                ee.setId(((Double) r.get("id")).intValue());
                Evenement e = new Evenement(evid);
                ee.setEvenementId(e);
                User u = new User(usid);
                ee.setUserId(u);
                ee.setNote(((Double) r.get("note")).intValue());
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return ee;
    }
    
    public void ajouterEvaluation(int evid, int usid, int note) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/ajouterEvaluation?evid=" + evid + "&usid=" + usid + "&note=" + note;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    public void modifierEvaluation(int evid, int usid, int note) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/modifierEvaluation?evid=" + evid + "&usid=" + usid + "&note=" + note;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    public double moyenneEvaluations(int id) {
        String url = "http://localhost/PidEV/web/app_dev.php/api/evenement/moyenneEvaluations/"+id;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return Double.parseDouble(lb.getText());
    }
}
