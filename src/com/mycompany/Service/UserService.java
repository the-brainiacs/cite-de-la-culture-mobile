/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Label;
import com.codename1.util.StringUtil;
import com.mycompany.Entities.User;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author missaoui
 */
public class UserService {

    public User login(String username, String password) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/user/login?username=" + username + "&password=" + password;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        User u = null;
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                u = new User();
                u.setId(((Double) result.get("id")).intValue());
                u.setUsername((String) result.get("username"));
                u.setNom((String) result.get("nom"));
                u.setEmail((String) result.get("email"));
                u.setPrenom((String) result.get("prenom"));
                u.setImg((String) result.get("Img"));
                u.setImgCouv((String) result.get("ImgCouv"));
                u.setBio((String) result.get("bio"));
                u.setRoles((ArrayList) result.get("roles"));
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return u;
    }
    
    public User register(String username, String password, String email, String nom, String prenom, String bio, String Img, String ImgCouv) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/user/register?username=" + username + "&password=" + password + "&email=" + email + "&nom=" + nom + "&prenom=" + prenom + "&bio=" + bio + "&Img=" + Img + "&Couv=" + ImgCouv;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        User u = null;
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                u = new User();
                u.setId(((Double) result.get("id")).intValue());
                u.setUsername((String) result.get("username"));
                u.setNom((String) result.get("nom"));
                u.setEmail((String) result.get("email"));
                u.setPrenom((String) result.get("prenom"));
                u.setImg((String) result.get("Img"));
                u.setImgCouv((String) result.get("ImgCouv"));
                u.setBio((String) result.get("bio"));
                u.setRoles((ArrayList) result.get("roles"));
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return u;
    }
    
    public User modifierProfile(int id, String username, String email, String nom, String prenom, String bio, String Img, String ImgCouv) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/user/modifier/"+id+"?username=" + username + "&email=" + email + "&nom=" + nom + "&prenom=" + prenom + "&bio=" + bio + "&Img=" + Img + "&Couv=" + ImgCouv;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        User u = null;
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                u = new User();
                u.setId(((Double) result.get("id")).intValue());
                u.setUsername((String) result.get("username"));
                u.setNom((String) result.get("nom"));
                u.setEmail((String) result.get("email"));
                u.setPrenom((String) result.get("prenom"));
                u.setImg((String) result.get("Img"));
                u.setImgCouv((String) result.get("ImgCouv"));
                u.setBio((String) result.get("bio"));
                u.setRoles((ArrayList) result.get("roles"));
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return u;
    }
    
    public User changerPassword(int id, String oldPassword, String newPassword) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/user/changerpassword/"+id+"?oldpassword=" + oldPassword + "&newpassword=" + newPassword;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        User u = null;
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                u = new User();
                u.setId(((Double) result.get("id")).intValue());
                u.setUsername((String) result.get("username"));
                u.setNom((String) result.get("nom"));
                u.setEmail((String) result.get("email"));
                u.setPrenom((String) result.get("prenom"));
                u.setImg((String) result.get("Img"));
                u.setImgCouv((String) result.get("ImgCouv"));
                u.setBio((String) result.get("bio"));
                u.setRoles((ArrayList) result.get("roles"));
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return u;
    }
    
}
