/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Label;
import com.codename1.util.StringUtil;
import com.mycompany.Entities.Offresponsoring;
import com.mycompany.Entities.User;
import com.mycompany.MyApplication;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author nawre
 */
public class OffreService {
       public static List<Offresponsoring> getAllOffreValide(String nom) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/sponsoring/all_offre";
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        
           if (nom!=null) {
               if (!nom.isEmpty()) {
                   //con.setPost(true);
                   con.addArgument("search", nom);
               }
           }
        
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        List<Offresponsoring> le = new ArrayList<>();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Offresponsoring e = new Offresponsoring();
                    e.setId(((Double) r.get("id")).intValue());
                    e.setDescription((String) r.get("description"));
                    e.setPackage1((String) r.get("package"));
                    e.setType((String) r.get("type"));
                    e.setEtat((String) r.get("etat"));
//                    e.setIduser((int) r.get("iduser"));
                    le.add(e);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return le;
    }
       public boolean ajouterOffre(Offresponsoring o ){
           
            ConnectionRequest con = new ConnectionRequest();
            String Url = "http://localhost/PIDEV/web/app_dev.php/api/sponsoring/add";
            con.setUrl(Url);
            con.setPost(true);
            // post data
           con.addArgument("iduser",o.getIduser().getId().toString());
            con.addArgument("event",o.getIdEvent().getId().toString());
            con.addArgument("type",o.getType());
            con.addArgument("description",o.getDescription());
            con.addArgument("package",o.getPackage1());
            // send request
            NetworkManager.getInstance().addToQueueAndWait(con);
            String str = new String(con.getResponseData());
            // parsing data
            if (con.getResponseCode()!=200) {
                System.out.println(str);
                return false;
            }else{ 
                System.out.println(str);
                return true;
            }
           
           
           
       }
       
       
       public List<Offresponsoring> getMyOffres() {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/sponsoring/mesOffres";
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(url);
        con.setPost(true);
        con.addArgument("iduser",MyApplication.CurrentUser.getId().toString());
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        List<Offresponsoring> le = new ArrayList<>();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Offresponsoring e = new Offresponsoring();
                    e.setId(((Double) r.get("id")).intValue());
                    e.setDescription((String) r.get("description"));
                    e.setPackage1((String) r.get("package"));
                    e.setType((String) r.get("type"));
                    e.setEtat((String) r.get("etat"));
                    le.add(e);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return le;
    }
        public void SupprimerOffre(int id) {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/PIDEV/web/app_dev.php/api/sponsoring/supp_offre/"+id;
        con.setUrl(Url);
        
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
            public boolean ModifierOffre(int id, Offresponsoring o) { 
                 ConnectionRequest con = new ConnectionRequest();
            String Url = "http://localhost/PIDEV/web/app_dev.php/api/sponsoring/modifier_off/"+id;
            con.setUrl(Url);
            con.setPost(true);
            // post data
           con.addArgument("iduser",o.getIduser().getId().toString());
            con.addArgument("event",o.getIdEvent().getId().toString());
            con.addArgument("type",o.getType());
            con.addArgument("description",o.getDescription());
            con.addArgument("package",o.getPackage1());
            // send request
            NetworkManager.getInstance().addToQueueAndWait(con);
            String str = new String(con.getResponseData());
            // parsing data
            if (con.getResponseCode()!=200) {
                System.out.println(str);
                return false;
            }else{ 
                System.out.println(str);
                return true;
            }
          
           
           
           
    }
    
}
