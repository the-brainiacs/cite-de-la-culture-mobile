/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Label;
import com.mycompany.Entities.Club;
import com.mycompany.MyApplication;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author kahou
 */
public class ClubService {

    public List<Club> getAllClubs() {
        String url = "http://localhost/Pidev/web/app_dev.php/api/club/afficherAll";
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(ev-> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        }
        );
        NetworkManager.getInstance().addToQueueAndWait(con);
        List<Club> lc = new ArrayList<>();

        if (!lb.getText().equals("[]")) 
        {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Club c = new Club();
                     c.setId(((Double) r.get("id")).intValue());
                
                    c.setNom((String)r.get("nom"));
                    c.setGenre((String)r.get("genre"));
                    Date d1 = new Date();
                    Map<String, Object> dc = (Map<String, Object>) r.get("dateCreation");
                    d1.setTime(((Double) dc.get("timestamp")).longValue() * 1000);
                    c.setDateCreation(d1);
                    c.setGroupeAge((String)r.get("groupeAge"));
                    c.setPrixInscri((Double)r.get("prixInscri"));
                   c.setDescription((String)r.get(("description")));
                    
                    lc.add(c);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return lc;
    }
    
    public String inscription(int id)
    {
        String url = "http://localhost/Pidev/web/app_dev.php/api/club/inscription/"+id+"/"+MyApplication.CurrentUser.getId();
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(ev-> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        }
        );
        NetworkManager.getInstance().addToQueueAndWait(con);
        Map<String, Object> result = null;
        try {
             result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
        } catch (UnsupportedEncodingException ex) {
        } catch (IOException ex) {
        }
        
        return (String)result.get("OK");
    }
}
