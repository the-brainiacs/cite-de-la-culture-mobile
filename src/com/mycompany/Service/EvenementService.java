/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Label;
import com.codename1.util.StringUtil;
import com.mycompany.Entities.Evenement;
import com.mycompany.Entities.Expose;
import com.mycompany.Entities.Festival;
import com.mycompany.Entities.Film;
import com.mycompany.Entities.Formation;
import com.mycompany.Entities.Musique;
import com.mycompany.Entities.Salle;
import com.mycompany.Entities.Theatre;
import com.mycompany.Entities.User;
import com.mycompany.MyApplication;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author missaoui
 */
public class EvenementService {

    public List<Evenement> getAllEvenementsAcceptes() {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/all";
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        List<Evenement> le = new ArrayList<>();
        if (!lb.getText().equals("[]")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Evenement e = new Evenement();
                    e.setId(((Double) r.get("id")).intValue());
                    e.setNom((String) r.get("nom"));
                    e.setDescription((String) r.get("description"));
                    e.setAge((String) r.get("age"));
                    e.setImageId((String) r.get("Img"));
                    Date d1 = new Date();
                    Map<String, Object> dd = (Map<String, Object>) r.get("dateDebut");
                    d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                    e.setDateDebut(d1);
                    Date d2 = new Date();
                    Map<String, Object> df = (Map<String, Object>) r.get("dateFin");
                    d2.setTime(((Double) df.get("timestamp")).longValue() * 1000);
                    e.setDateFin(d2);
                    e.setDisponibles(((Double) r.get("disponibles")).intValue());
                    e.setCoutReservation((Double) r.get("coutReservation"));
                    e.setPrix((Double) r.get("prix"));
                    e.setPrixEtudiants((Double) r.get("prixEtudiants"));
                    e.setEtat((String) r.get("etat"));
                    e.setType((String) r.get("type"));
                    Map<String, Object> s = (Map<String, Object>) r.get("Salle");
                    Salle salle = new Salle();
                    salle.setId(((Double) s.get("id")).intValue());
                    salle.setCapacite(((Double) s.get("capacite")).intValue());
                    salle.setCoutReservationHeure((Double) s.get("coutReservationHeure"));
                    salle.setNom((String) s.get("nom"));
                    salle.setType((String) s.get("type"));
                    e.setSalleId(salle);
                    Map<String, Object> uu = (Map<String, Object>) r.get("user");
                    User u = new User();
                    u.setId(((Double) uu.get("id")).intValue());
                    u.setUsername((String) uu.get("username"));
                    u.setNom((String) uu.get("nom"));
                    u.setEmail((String) uu.get("email"));
                    u.setPrenom((String) uu.get("prenom"));
                    u.setImg((String) uu.get("Img"));
                    u.setImgCouv((String) uu.get("ImgCouv"));
                    u.setBio((String) uu.get("bio"));
                    u.setRoles((ArrayList) uu.get("roles"));
                    e.setUserId(u);
                    le.add(e);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return le;
    }
    
    public List<Evenement> rechercher(String rech) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/set-arechercher-evenement?rech="+rech;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        List<Evenement> le = new ArrayList<>();
        if (!lb.getText().equals("[]")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Evenement e = new Evenement();
                    e.setId(((Double) r.get("id")).intValue());
                    e.setNom((String) r.get("nom"));
                    e.setDescription((String) r.get("description"));
                    e.setAge((String) r.get("age"));
                    e.setImageId((String) r.get("Img"));
                    Date d1 = new Date();
                    Map<String, Object> dd = (Map<String, Object>) r.get("dateDebut");
                    d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                    e.setDateDebut(d1);
                    Date d2 = new Date();
                    Map<String, Object> df = (Map<String, Object>) r.get("dateFin");
                    d2.setTime(((Double) df.get("timestamp")).longValue() * 1000);
                    e.setDateFin(d2);
                    e.setDisponibles(((Double) r.get("disponibles")).intValue());
                    e.setCoutReservation((Double) r.get("coutReservation"));
                    e.setPrix((Double) r.get("prix"));
                    e.setPrixEtudiants((Double) r.get("prixEtudiants"));
                    e.setEtat((String) r.get("etat"));
                    e.setType((String) r.get("type"));
                    Map<String, Object> s = (Map<String, Object>) r.get("Salle");
                    Salle salle = new Salle();
                    salle.setId(((Double) s.get("id")).intValue());
                    salle.setCapacite(((Double) s.get("capacite")).intValue());
                    salle.setCoutReservationHeure((Double) s.get("coutReservationHeure"));
                    salle.setNom((String) s.get("nom"));
                    salle.setType((String) s.get("type"));
                    e.setSalleId(salle);
                    Map<String, Object> uu = (Map<String, Object>) r.get("user");
                    User u = new User();
                    u.setId(((Double) uu.get("id")).intValue());
                    u.setUsername((String) uu.get("username"));
                    u.setNom((String) uu.get("nom"));
                    u.setEmail((String) uu.get("email"));
                    u.setPrenom((String) uu.get("prenom"));
                    u.setImg((String) uu.get("Img"));
                    u.setImgCouv((String) uu.get("ImgCouv"));
                    u.setBio((String) uu.get("bio"));
                    u.setRoles((ArrayList) uu.get("roles"));
                    e.setUserId(u);
                    le.add(e);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return le;
    }
    
    public List<Evenement> getMesEvenements() {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/byOrganisateur/"+MyApplication.CurrentUser.getId();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        List<Evenement> le = new ArrayList<>();
        if (!lb.getText().equals("[]")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Evenement e = new Evenement();
                    e.setId(((Double) r.get("id")).intValue());
                    e.setNom((String) r.get("nom"));
                    e.setDescription((String) r.get("description"));
                    e.setAge((String) r.get("age"));
                    e.setImageId((String) r.get("Img"));
                    Date d1 = new Date();
                    Map<String, Object> dd = (Map<String, Object>) r.get("dateDebut");
                    d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                    e.setDateDebut(d1);
                    Date d2 = new Date();
                    Map<String, Object> df = (Map<String, Object>) r.get("dateFin");
                    d2.setTime(((Double) df.get("timestamp")).longValue() * 1000);
                    e.setDateFin(d2);
                    e.setDisponibles(((Double) r.get("disponibles")).intValue());
                    e.setCoutReservation((Double) r.get("coutReservation"));
                    e.setPrix((Double) r.get("prix"));
                    e.setPrixEtudiants((Double) r.get("prixEtudiants"));
                    e.setEtat((String) r.get("etat"));
                    e.setType((String) r.get("type"));
                    Map<String, Object> s = (Map<String, Object>) r.get("Salle");
                    Salle salle = new Salle();
                    salle.setId(((Double) s.get("id")).intValue());
                    salle.setCapacite(((Double) s.get("capacite")).intValue());
                    salle.setCoutReservationHeure((Double) s.get("coutReservationHeure"));
                    salle.setNom((String) s.get("nom"));
                    salle.setType((String) s.get("type"));
                    e.setSalleId(salle);
                    Map<String, Object> uu = (Map<String, Object>) r.get("user");
                    User u = new User();
                    u.setId(((Double) uu.get("id")).intValue());
                    u.setUsername((String) uu.get("username"));
                    u.setNom((String) uu.get("nom"));
                    u.setEmail((String) uu.get("email"));
                    u.setPrenom((String) uu.get("prenom"));
                    u.setImg((String) uu.get("Img"));
                    u.setImgCouv((String) uu.get("ImgCouv"));
                    u.setBio((String) uu.get("bio"));
                    u.setRoles((ArrayList) uu.get("roles"));
                    e.setUserId(u);
                    le.add(e);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return le;
    }
    
    public List<Evenement> getRecommandations() {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/recommandations/"+MyApplication.CurrentUser.getId();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        List<Evenement> le = new ArrayList<>();
        if (!lb.getText().equals("[]")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Evenement e = new Evenement();
                    e.setId(((Double) r.get("id")).intValue());
                    e.setNom((String) r.get("nom"));
                    e.setDescription((String) r.get("description"));
                    e.setAge((String) r.get("age"));
                    e.setImageId((String) r.get("Img"));
                    Date d1 = new Date();
                    Map<String, Object> dd = (Map<String, Object>) r.get("dateDebut");
                    d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                    e.setDateDebut(d1);
                    Date d2 = new Date();
                    Map<String, Object> df = (Map<String, Object>) r.get("dateFin");
                    d2.setTime(((Double) df.get("timestamp")).longValue() * 1000);
                    e.setDateFin(d2);
                    e.setDisponibles(((Double) r.get("disponibles")).intValue());
                    e.setCoutReservation((Double) r.get("coutReservation"));
                    e.setPrix((Double) r.get("prix"));
                    e.setPrixEtudiants((Double) r.get("prixEtudiants"));
                    e.setEtat((String) r.get("etat"));
                    e.setType((String) r.get("type"));
                    Map<String, Object> s = (Map<String, Object>) r.get("Salle");
                    Salle salle = new Salle();
                    salle.setId(((Double) s.get("id")).intValue());
                    salle.setCapacite(((Double) s.get("capacite")).intValue());
                    salle.setCoutReservationHeure((Double) s.get("coutReservationHeure"));
                    salle.setNom((String) s.get("nom"));
                    salle.setType((String) s.get("type"));
                    e.setSalleId(salle);
                    Map<String, Object> uu = (Map<String, Object>) r.get("user");
                    User u = new User();
                    u.setId(((Double) uu.get("id")).intValue());
                    u.setUsername((String) uu.get("username"));
                    u.setNom((String) uu.get("nom"));
                    u.setEmail((String) uu.get("email"));
                    u.setPrenom((String) uu.get("prenom"));
                    u.setImg((String) uu.get("Img"));
                    u.setImgCouv((String) uu.get("ImgCouv"));
                    u.setBio((String) uu.get("bio"));
                    u.setRoles((ArrayList) uu.get("roles"));
                    e.setUserId(u);
                    le.add(e);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return le;
    }

    public Evenement getEvenement(int id) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/byId/"+id;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Evenement e = new Evenement();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                e.setId(((Double) r.get("id")).intValue());
                e.setNom((String) r.get("nom"));
                e.setDescription((String) r.get("description"));
                e.setAge((String) r.get("age"));
                e.setImageId((String) r.get("Img"));
                Date d1 = new Date();
                Map<String, Object> dd = (Map<String, Object>) r.get("dateDebut");
                d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                e.setDateDebut(d1);
                Date d2 = new Date();
                Map<String, Object> df = (Map<String, Object>) r.get("dateFin");
                d2.setTime(((Double) df.get("timestamp")).longValue() * 1000);
                e.setDateFin(d2);
                e.setDisponibles(((Double) r.get("disponibles")).intValue());
                e.setCoutReservation((Double) r.get("coutReservation"));
                e.setPrix((Double) r.get("prix"));
                e.setPrixEtudiants((Double) r.get("prixEtudiants"));
                e.setEtat((String) r.get("etat"));
                e.setType((String) r.get("type"));
                Map<String, Object> s = (Map<String, Object>) r.get("Salle");
                Salle salle = new Salle();
                salle.setId(((Double) s.get("id")).intValue());
                salle.setCapacite(((Double) s.get("capacite")).intValue());
                salle.setCoutReservationHeure((Double) s.get("coutReservationHeure"));
                salle.setNom((String) s.get("nom"));
                salle.setType((String) s.get("type"));
                e.setSalleId(salle);
                Map<String, Object> uu = (Map<String, Object>) r.get("user");
                User u = new User();
                u.setId(((Double) uu.get("id")).intValue());
                u.setUsername((String) uu.get("username"));
                u.setNom((String) uu.get("nom"));
                u.setEmail((String) uu.get("email"));
                u.setPrenom((String) uu.get("prenom"));
                u.setImg((String) uu.get("Img"));
                u.setImgCouv((String) uu.get("ImgCouv"));
                u.setBio((String) uu.get("bio"));
                u.setRoles((ArrayList) uu.get("roles"));
                e.setUserId(u);
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return e;
    }
    
    public Film getFilm(int id) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/film/byId/"+id;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Film f = new Film();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                f.setId(((Double) r.get("id")).intValue());
                f.setActeurs((String) r.get("acteurs"));
                f.setGenre((String) r.get("genre"));
                f.setRealisateurs((String) r.get("realisateurs"));
                f.setDuree(((Double) r.get("duree")).intValue());
                Date d1 = new Date();
                Map<String, Object> dd = (Map<String, Object>) r.get("anneeSortie");
                d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                f.setAnneeSortie(d1);
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return f;
    }
    
    public Theatre getTheatre(int id) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/theatre/byId/"+id;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Theatre t = new Theatre();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                t.setId(((Double) r.get("id")).intValue());
                t.setAuteurs((String) r.get("auteurs"));
                t.setGenre((String) r.get("genre"));
                t.setRealisateurs((String) r.get("realisateurs"));
                t.setArtistes((String) r.get("artistes"));
                Date d1 = new Date();
                Map<String, Object> dd = (Map<String, Object>) r.get("anneeSortie");
                d1.setTime(((Double) dd.get("timestamp")).longValue() * 1000);
                t.setAnneeSortie(d1);
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return t;
    }
    
    public Festival getFestival(int id) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/festival/byId/"+id;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Festival f = new Festival();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                f.setId(((Double) r.get("id")).intValue());
                f.setGenreFestival((String) r.get("genreFestival"));
                f.setTypeFestival((String) r.get("typeFestival"));
                f.setParticipants((String) r.get("participants"));
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return f;
    }
    
    public Formation getFormation(int id) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/formation/byId/"+id;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Formation f = new Formation();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                f.setId(((Double) r.get("id")).intValue());
                f.setLocuteurs((String) r.get("locuteurs"));
                f.setTheme((String) r.get("theme"));
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return f;
    }
    
    public Expose getExpose(int id) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/expose/byId/"+id;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Expose e = new Expose();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                e.setId(((Double) r.get("id")).intValue());
                e.setLocuteurs((String) r.get("locuteurs"));
                e.setTheme((String) r.get("theme"));
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return e;
    }
    
    public Musique getMusique(int id) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/musique/byId/"+id;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Musique m = new Musique();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                m.setId(((Double) r.get("id")).intValue());
                m.setArtistes((String) r.get("artistes"));
                m.setGenre((String) r.get("genre"));
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return m;
    }

    public void modifierFilm(Evenement ev, Film f) {
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d H:mm");
        SimpleDateFormat sdf2 = new SimpleDateFormat("y-M-d");
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/modifier/" + ev.getId() + "?nom=" + ev.getNom() + "&description=" + ev.getDescription() + "&prix=" + ev.getPrix() + "&prixEt=" + ev.getPrixEtudiants() + "&age=" + ev.getAge() + "&Img=" + ev.getImageId() + "&acteurs=" + f.getActeurs() + "&realisateurs=" + f.getRealisateurs() + "&genre=" + f.getGenre() + "&anneeSortie=" + sdf2.format(f.getAnneeSortie()) + "&duree=" + f.getDuree();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        System.out.println(lb.getText());
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    public Evenement ajouterFilm(Evenement ev, Film f) {
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d H:mm");
        SimpleDateFormat sdf2 = new SimpleDateFormat("y-M-d");
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/ajouter?nom=" + ev.getNom() + "&dateDebut=" + sdf.format(ev.getDateDebut()) + "&dateFin=" + sdf.format(ev.getDateFin()) + "&disponibles=" + ev.getDisponibles() + "&type=" + ev.getType() + "&salle_id=" + ev.getSalleId().getId() + "&user_id=" + ev.getUserId().getId() + "&description=" + ev.getDescription() + "&prix=" + ev.getPrix() + "&prixEt=" + ev.getPrixEtudiants() + "&age=" + ev.getAge() + "&Img=" + ev.getImageId() + "&acteurs=" + f.getActeurs() + "&realisateurs=" + f.getRealisateurs() + "&genre=" + f.getGenre() + "&anneeSortie=" + sdf2.format(f.getAnneeSortie()) + "&duree=" + f.getDuree();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        System.out.println(lb.getText());
        NetworkManager.getInstance().addToQueueAndWait(con);
        Evenement e = new Evenement();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                e.setId(((Double) r.get("id")).intValue());
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return e;
    }

    public void modifierTheatre(Evenement ev, Theatre t) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("y-M-d");
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/modifier/" + ev.getId() + "?nom=" + ev.getNom() + "&description=" + ev.getDescription() + "&prix=" + ev.getPrix() + "&prixEt=" + ev.getPrixEtudiants() + "&age=" + ev.getAge() + "&Img=" + ev.getImageId() + "&artistes=" + t.getArtistes() + "&auteurs=" + t.getAuteurs() + "&genre=" + t.getGenre() + "&anneeSortie=" + sdf2.format(t.getAnneeSortie()) + "&realisateurs=" + t.getRealisateurs();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    public Evenement ajouterTheatre(Evenement ev, Theatre t) {
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d H:mm");
        SimpleDateFormat sdf2 = new SimpleDateFormat("y-M-d");
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/ajouter?nom=" + ev.getNom() + "&dateDebut=" + sdf.format(ev.getDateDebut()) + "&dateFin=" + sdf.format(ev.getDateFin()) + "&disponibles=" + ev.getDisponibles() + "&type=" + ev.getType() + "&salle_id=" + ev.getSalleId().getId() + "&user_id=" + ev.getUserId().getId() + "&description=" + ev.getDescription() + "&prix=" + ev.getPrix() + "&prixEt=" + ev.getPrixEtudiants() + "&age=" + ev.getAge() + "&Img=" + ev.getImageId() + "&artistes=" + t.getArtistes() + "&auteurs=" + t.getAuteurs() + "&genre=" + t.getGenre() + "&anneeSortie=" + sdf2.format(t.getAnneeSortie()) + "&realisateurs=" + t.getRealisateurs();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Evenement e = new Evenement();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                e.setId(((Double) r.get("id")).intValue());
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return e;
    }

    public void modifierExpose(Evenement ev, Expose e) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/modifier/" + ev.getId() + "?nom=" + ev.getNom() + "&description=" + ev.getDescription() + "&prix=" + ev.getPrix() + "&prixEt=" + ev.getPrixEtudiants() + "&age=" + ev.getAge() + "&Img=" + ev.getImageId() + "&locuteurs=" + e.getLocuteurs() + "&themeExpose=" + e.getTheme();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    public Evenement ajouterExpose(Evenement ev, Expose ex) {
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d H:mm");
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/ajouter?nom=" + ev.getNom() + "&dateDebut=" + sdf.format(ev.getDateDebut()) + "&dateFin=" + sdf.format(ev.getDateFin()) + "&disponibles=" + ev.getDisponibles() + "&type=" + ev.getType() + "&salle_id=" + ev.getSalleId().getId() + "&user_id=" + ev.getUserId().getId() + "&description=" + ev.getDescription() + "&prix=" + ev.getPrix() + "&prixEt=" + ev.getPrixEtudiants() + "&age=" + ev.getAge() + "&Img=" + ev.getImageId() + "&locuteurs=" + ex.getLocuteurs() + "&themeExpose=" + ex.getTheme();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Evenement e = new Evenement();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                e.setId(((Double) r.get("id")).intValue());
            } catch (UnsupportedEncodingException exc) {
                System.out.println(exc.getMessage());
            } catch (IOException exc) {
                System.out.println(exc.getMessage());
            }
        }
        return e;
    }

    public void modifierFestival(Evenement ev, Festival f) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/modifier/" + ev.getId() + "?nom=" + ev.getNom() + "&description=" + ev.getDescription() + "&prix=" + ev.getPrix() + "&prixEt=" + ev.getPrixEtudiants() + "&age=" + ev.getAge() + "&Img=" + ev.getImageId() + "&participants=" + f.getParticipants() + "&genreFestival=" + f.getGenreFestival() + "&typeFestival=" + f.getTypeFestival();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    public Evenement ajouterFestival(Evenement ev, Festival f) {
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d H:mm");
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/ajouter?nom=" + ev.getNom() + "&dateDebut=" + sdf.format(ev.getDateDebut()) + "&dateFin=" + sdf.format(ev.getDateFin()) + "&disponibles=" + ev.getDisponibles() + "&type=" + ev.getType() + "&salle_id=" + ev.getSalleId().getId() + "&user_id=" + ev.getUserId().getId() + "&description=" + ev.getDescription() + "&prix=" + ev.getPrix() + "&prixEt=" + ev.getPrixEtudiants() + "&age=" + ev.getAge() + "&Img=" + ev.getImageId() + "&participants=" + f.getParticipants() + "&genreFestival=" + f.getGenreFestival() + "&typeFestival=" + f.getTypeFestival();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Evenement e = new Evenement();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                e.setId(((Double) r.get("id")).intValue());
            } catch (UnsupportedEncodingException exc) {
                System.out.println(exc.getMessage());
            } catch (IOException exc) {
                System.out.println(exc.getMessage());
            }
        }
        return e;
    }

    public void modifierFormation(Evenement ev, Formation f) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/modifier/" + ev.getId() + "?nom=" + ev.getNom() + "&description=" + ev.getDescription() + "&prix=" + ev.getPrix() + "&prixEt=" + ev.getPrixEtudiants() + "&age=" + ev.getAge() + "&Img=" + ev.getImageId() + "&locuteurs=" + f.getLocuteurs() + "&themeFormation=" + f.getTheme();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    public Evenement ajouterFormation(Evenement ev, Formation f) {
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d H:mm");
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/ajouter?nom=" + ev.getNom() + "&dateDebut=" + sdf.format(ev.getDateDebut()) + "&dateFin=" + sdf.format(ev.getDateFin()) + "&disponibles=" + ev.getDisponibles() + "&type=" + ev.getType() + "&salle_id=" + ev.getSalleId().getId() + "&user_id=" + ev.getUserId().getId() + "&description=" + ev.getDescription() + "&prix=" + ev.getPrix() + "&prixEt=" + ev.getPrixEtudiants() + "&age=" + ev.getAge() + "&Img=" + ev.getImageId() + "&locuteurs=" + f.getLocuteurs() + "&themeFormation=" + f.getTheme();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Evenement e = new Evenement();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                e.setId(((Double) r.get("id")).intValue());
            } catch (UnsupportedEncodingException exc) {
                System.out.println(exc.getMessage());
            } catch (IOException exc) {
                System.out.println(exc.getMessage());
            }
        }
        return e;
    }

    public void modifierMusique(Evenement ev, Musique m) {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/modifier/" + ev.getId() + "?nom=" + ev.getNom() + "&description=" + ev.getDescription() + "&prix=" + ev.getPrix() + "&prixEt=" + ev.getPrixEtudiants() + "&age=" + ev.getAge() + "&Img=" + ev.getImageId() + "&artistes=" + m.getArtistes() + "&genreMusique=" + m.getGenre();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    public Evenement ajouterMusique(Evenement ev, Musique m) {
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d H:mm");
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/ajouter?nom=" + ev.getNom() + "&dateDebut=" + sdf.format(ev.getDateDebut()) + "&dateFin=" + sdf.format(ev.getDateFin()) + "&disponibles=" + ev.getDisponibles() + "&type=" + ev.getType() + "&salle_id=" + ev.getSalleId().getId() + "&user_id=" + ev.getUserId().getId() + "&description=" + ev.getDescription() + "&prix=" + ev.getPrix() + "&prixEt=" + ev.getPrixEtudiants() + "&age=" + ev.getAge() + "&Img=" + ev.getImageId() + "&artistes=" + m.getArtistes() + "&genreMusique=" + m.getGenre();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        Evenement e = new Evenement();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> r = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                e.setId(((Double) r.get("id")).intValue());
            } catch (UnsupportedEncodingException exc) {
                System.out.println(exc.getMessage());
            } catch (IOException exc) {
                System.out.println(exc.getMessage());
            }
        }
        return e;
    }
    
    public void supprimerEvenement(Evenement ev)
    {
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/supprimer/" + ev.getId();
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    public void ajouterReservation(Evenement ev, int id) {
        
        String url = "http://localhost/PIDEV/web/app_dev.php/api/evenement/reservation/" + ev.getId()+ "?user_id=" + id;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        
    }
}
