/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.MyApplication;

/**
 *
 * @author missaoui
 */
public class ProfileForm {

    private Resources theme;
    private Form profile;
    private Label username, bio, nom, email, prenom, n,p,e;
    private Button modifier, password;
    private Container cnt, cnt1, cnt2,cnt3, cnt4;
    private ImageViewer iv, ivc;

    public ProfileForm() {
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font smallPlainSystemFont = Font.createSystemFont(Font.FACE_MONOSPACE, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        Font mediumPlainSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
        profile = new Form("Profile", new FlowLayout(Container.CENTER, Container.TOP));
        profile.getToolbar().addCommandToLeftBar("Index", FontImage.createFixed("\ue815", fnt, 0xffffff, size, size), ev -> {
            IndexForm in = new IndexForm();
            in.getIndex().show();
        });
        EncodedImage enc = EncodedImage.createFromImage(Image.createImage(250, 300, 0xffffffff), false);
        String url = "http://localhost/PiDev/web/getImage.php?file=" + MyApplication.CurrentUser.getImg();
        URLImage urlimage = URLImage.createToStorage(enc, "profile" + MyApplication.CurrentUser.getImg(), url);
        iv = new ImageViewer(urlimage);
        EncodedImage enc2 = EncodedImage.createFromImage(Image.createImage(profile.getWidth(), profile.getWidth()*2/5, 0xffffffff), true);
        String url2 = "http://localhost/PiDev/web/getImage.php?file=" + MyApplication.CurrentUser.getImgCouv();
        URLImage urlimage2 = URLImage.createToStorage(enc2, "Couverture" + MyApplication.CurrentUser.getImgCouv(), url2);
        ivc = new ImageViewer(urlimage2);
        modifier = new Button("Modifier", FontImage.createFixed("\ue859", fnt, 0xffffff, size, size));
        modifier = setButton(modifier, 0xffc107, 0xffffff, 1);
        modifier.addActionListener(e->{
            ModifierProfileForm mpf = new ModifierProfileForm();
            mpf.getModifierProfile().show();
        });
        password = new Button("Changer Password",FontImage.createFixed("\uea42", fnt, 0xffffff, size, size));
        password = setButton(password, 0xE6763A, 0xffffff, 1);
        password.addActionListener(e->{
            ChangerPasswordForm mpf = new ChangerPasswordForm();
            mpf.getChangerPassword().show();
        });
        username = createForFont(mediumPlainSystemFont,MyApplication.CurrentUser.getUsername());
        bio = createForFont(smallPlainSystemFont, MyApplication.CurrentUser.getBio());
        n = new Label("Nom : ");
        p = new Label("Prénom : ");
        e = new Label("Email : ");
        nom = createForFont(smallPlainSystemFont,MyApplication.CurrentUser.getNom());
        prenom = createForFont(smallPlainSystemFont,MyApplication.CurrentUser.getPrenom());
        email = createForFont(smallPlainSystemFont,MyApplication.CurrentUser.getEmail());
        cnt = new Container(BoxLayout.y());
        cnt.getAllStyles().setPadding(10,10,20,20);
        cnt1 = new Container(BoxLayout.x());
        cnt1.getAllStyles().setPadding(10,20,30,30);
        cnt2 = new Container(BoxLayout.y());
        cnt2.getAllStyles().setPadding(40,0,20,0);
        cnt3 = new Container(BoxLayout.y());
        cnt4 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        cnt4.getAllStyles().setPadding(20,20,20,20);
        
        cnt2.add(username);
        cnt2.add(bio);
        cnt1.add(iv);
        cnt1.add(cnt2);
        cnt4.add(modifier);
        cnt4.add(password);
        cnt3.add(n);
        cnt3.add(nom);
        cnt3.add(p);
        cnt3.add(prenom);
        cnt3.add(e);
        cnt3.add(email);
        cnt.add(ivc);
        cnt.add(cnt1);
        cnt.add(cnt3);
        cnt.add(cnt4);
        profile.add(cnt);
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getAllStyles().setFont(fnt);
        return l;
    }
    
    public Form getProfile() {
        return profile;
    }
    
    private Button setButton(Button btn, int color1, int color2, int mar)
    {
        btn.setUIID("btn");
        btn.getStyle().setBgTransparency(255);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(mar,mar,mar*10,mar*10);
        btn.getPressedStyle().setBgTransparency(175);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(mar,mar,mar*10,mar*10);
        btn.getSelectedStyle().setBgTransparency(175);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(mar,mar,mar*10,mar*10);
        return btn;
    }
}
