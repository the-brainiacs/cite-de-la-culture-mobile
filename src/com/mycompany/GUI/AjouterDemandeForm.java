/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextComponent;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Demandesponsoring;
import com.mycompany.Entities.Evenement;
import com.mycompany.MyApplication;
import com.mycompany.Service.DemandeService;

/**
 *
 * @author nawre
 */
public class AjouterDemandeForm {

    private Container cntl, cntl2, cntl3;
    private Resources theme;
    private Form ajoutdemande;
    private Label ltype, ldesc, lpack, l;
    private TextComponent type;
    private TextField desc;
    private Button add;
    private ComboBox cb;

    public AjouterDemandeForm() {

        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        FontImage fm = FontImage.createFixed("\uecc6", fnt, 0xffffff, size, size);
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        ajoutdemande = new Form("Ajouter Demande Sponsoring", new FlowLayout(Container.CENTER, Container.CENTER));
        ajoutdemande.getToolbar().addMaterialCommandToLeftBar("", FontImage.MATERIAL_ARROW_BACK, (evt) -> {
            AfficherSponsoringForm a = new AfficherSponsoringForm();
            a.getF().show();
        });
        l = createForFont1(largeBoldSystemFont, "Salut M.");
        if (MyApplication.CurrentUser != null) {
            l.setText("Salut M. " + MyApplication.CurrentUser.getUsername());
        }
        l.getStyle().setFgColor(0xB95656);
        ltype = new Label("Type");
        ldesc = new Label("Description");
        lpack = new Label("package");
        cb = new ComboBox();
        cb.addItem("Premium");
        cb.addItem("Gold");
        cb.addItem("Silver");
        type = new TextComponent().label(" Type:");
        desc = new TextField("", "description", 20, 0);

        cntl = new Container(BoxLayout.y());

        add = new Button("Ajouter", FontImage.createFixed("\ue86c", fnt, 0xffffff, size, size));
        add = setButton(add, 0x1AA09F, 0xffffff);

        add.addActionListener((evt) -> {
            DemandeService os = new DemandeService();
            Demandesponsoring o = new Demandesponsoring();
            o.setDescription(desc.getText());
            o.setType(type.getText());
            o.setIdUser(MyApplication.CurrentUser);
            o.setPackage1(cb.getSelectedItem().toString());
            o.setIdEvent(new Evenement(1));
            os.ajouterDemande(o);
        });
        cntl.add(l);
        cntl.add(type);
        cntl.add(cb);
        cntl.add(desc);
        cntl.add(add);

        ajoutdemande.add(cntl);

    }

    public Form getAjoutdemande() {
        return ajoutdemande;
    }
    
    
      private Button setButton(Button btn, int color1, int color2) {
        btn.setUIID("btn");
        btn.getStyle().setBgTransparency(200);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(50, 20, 300, 300);
        btn.getPressedStyle().setBgTransparency(155);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(50, 20, 300, 300);
        btn.getSelectedStyle().setBgTransparency(155);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(50, 20, 300, 300);
        return btn;
    }

    private Label createForFont1(Font fnt, String s) {
   Label l = new Label(s);
        l.getUnselectedStyle().setFont(fnt);
        return l;
    }




}
