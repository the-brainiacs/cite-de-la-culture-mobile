/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import static com.codename1.ui.plaf.Style.BACKGROUND_NONE;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Club;
import com.mycompany.Service.ClubService;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kahou
 */
public class AfficherClubForm {

    private Resources theme;
    private Form clubs;
    private Container cnt2, ctn;
    private Label lb;

    public AfficherClubForm() {
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        clubs = new Form("Clubs", new FlowLayout(Container.CENTER, Container.TOP));
        clubs.getToolbar().addCommandToLeftBar("Activités", FontImage.createFixed("\ue815", fnt, 0xffffff, size, size), ev -> {
            AfficherActivitesForm aaf = new AfficherActivitesForm();
            aaf.getActivites().show();
        });
        ClubService cs = new ClubService();
        ArrayList<Club> list = new ArrayList();

        list = (ArrayList<Club>) cs.getAllClubs();
        for (Club c : list) {
            clubs.add(addComponent(c));
        }

    }

    public Form getClubs() {

        return clubs;

    }

    public Container addComponent(Club c) {
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font smallPlainSystemFont = Font.createSystemFont(Font.FACE_MONOSPACE, Font.STYLE_BOLD, Font.SIZE_SMALL);
        Font largePlainSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);

        Container cnt = new Container(BoxLayout.y());

        Container cnt2 = new Container(BoxLayout.x());
        cnt.setPreferredSize(new Dimension(900, 800));
        cnt.getAllStyles().setMargin(20, 20, 30, 30);
        cnt.getAllStyles().setBackgroundType(BACKGROUND_NONE);
        cnt.getAllStyles().setBgTransparency(160);
        cnt.getAllStyles().setBgColor(0x1AA00F);
        cnt.getAllStyles().setPadding(0, 1, 0, 0);
        Label prix = createForFont(smallPlainSystemFont, c.getPrixInscri() + "DT");

        prix.getAllStyles().setBgTransparency(100);
        prix.getAllStyles().setBgColor(0x000000);
        prix.getAllStyles().setAlignment(Label.CENTER);
        prix.getAllStyles().setFgColor(0xffffff);
        Label nom = createForFont(largePlainSystemFont, c.getGenre().toUpperCase() + ": " + c.getNom());
        nom.getAllStyles().setAlignment(Container.CENTER);
        nom.getAllStyles().setFgColor(0xffffff);
        Label desc = createForFont(smallPlainSystemFont, c.getDescription());
        Label age = new Label("Age: " + c.getGroupeAge());
        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM y H:mm");
        Label dc = new Label(sdf.format(c.getDateCreation()));
        Button btn = new Button("S'inscrire");
        btn.addActionListener(e -> {
            ClubService cs = new ClubService();
            String etat = cs.inscription(c.getId());
            if (etat.equals("true")) {
                Dialog.show("Inscription", "Vous êtes maintenant inscrit au club " + c.getNom(), "OK", null);
            } else {
                Dialog.show("Inscription", "Vous êtes déjà inscrit au club " + c.getNom(), "OK", null);
            }
        });
        dc.setIcon(FontImage.createFixed("\uf251", fnt, 0xffffff, size, size));

        dc.getAllStyles().setFgColor(0xffffff);

        cnt.add(nom);

        System.out.println("jaou behi");
        cnt.add(dc);
        cnt.add(prix);
        cnt.add(age);
        cnt.add(desc);
        cnt.add(btn);
        cnt2.add(cnt);

        // Yodkhol l koll club wahdou
//        Button btn = new Button();
//        btn.addActionListener(eee -> {
//            AfficherEvenementForm aef = new AfficherEvenementForm(c.getId(), 1);
//            aef.getEvenement().show();
//        });
//        cnt.setLeadComponent(btn);
//        cnt.revalidate();
//        cnt.getAllStyles().setBorder(Border.createLineBorder(4, 0x000000));
        return cnt2;
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getAllStyles().setFont(fnt);
        return l;
    }

}
