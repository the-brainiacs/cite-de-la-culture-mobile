/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextComponent;
import com.codename1.ui.TextField;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Reservation;
import com.mycompany.Entities.User;
import com.mycompany.MyApplication;
import com.mycompany.Service.PanierService;
import com.mycompany.Service.UserService;
import java.util.List;

/**
 *
 * @author azus
 */
public class AfficherFactureForm {
        private Resources theme;
    private Form facture;
     private Container cntl, cntl2, cntl3,cnt11;
    private Label log;
    private TextComponent username;
    private Button btnl;
    private double t ;
 
    public AfficherFactureForm()
    {
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        
        FontImage fm = FontImage.createFixed("\uecc6", fnt, 0xffffff, size, size);
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        
        Font smallPlainSystemFont = Font.createSystemFont(Font.FACE_MONOSPACE, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        facture = new Form("Facture", new FlowLayout(Container.CENTER, Container.TOP));
        facture.getToolbar().addCommandToLeftBar("Back", FontImage.createFixed("\ue885", fnt, 0xffffff, size, size), ev -> {
            AfficherPanierForm apf = new AfficherPanierForm();
            apf.getPanier().show();
        });
         PanierService es = new PanierService();
        List<Reservation> le = es.getAllReservations();
        t=0 ;
        for (Reservation re : le) {
           t = t + re.getQuantite()* re.getPrix();
        }
        
         cntl = new Container(BoxLayout.y());
        cntl2 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        log = createForFont(largeBoldSystemFont, "La Facture");
        log.getStyle().setFgColor(0x1AA09F);
        cntl3 = new Container(BoxLayout.x());
        cnt11 = new Container(BoxLayout.x());

        cntl3.getStyle().setBorder(Border.createLineBorder(3, 0x1AA09F));
        cntl3.getStyle().setMargin(0, 0,20,20);
        //username = new TextComponent().label("Username :");
        Label q = createForFont(smallPlainSystemFont,"Total  :");
       
        TextField quantite = new TextField();
        quantite.setText(String.valueOf(t)+" dt");
        quantite.setEditable(false);
        //quantite.setPreferredSize(new Dimension(60,80));
        btnl = new Button("Payer",FontImage.createFixed("\uecb5", fnt, 0xffffff, size, size));
        btnl = setButton(btnl, 0x1AA09F, 0xffffff);
        btnl.addActionListener(e -> {
           
        });
        cntl.add(cntl2);
        cntl2.add(log);
        cntl3.add(q);
        cntl3.add(quantite);
        cnt11.add(btnl);
        cntl.add(cntl3);
        cntl.add(cnt11);
        facture.add(cntl);
    }
    
    
     private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getUnselectedStyle().setFont(fnt);
        return l;
    }

    private Button setButton(Button btn, int color1, int color2) {
        btn.setUIID("btn");
        btn.getStyle().setBgTransparency(200);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(50, 20, 300, 300);
        btn.getPressedStyle().setBgTransparency(155);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(50, 20, 300, 300);
        btn.getSelectedStyle().setBgTransparency(155);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(50, 20, 300, 300);
        return btn;
    }

    
    public Form getFacture()
    {
        return facture;
    }
    
}
