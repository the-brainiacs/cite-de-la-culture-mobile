/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Evenement;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.TextComponent;
import static com.codename1.ui.plaf.Style.BACKGROUND_NONE;
import com.mycompany.Service.EvenementService;
import java.util.List;

public class AfficherEvenementsForm {

    private Resources theme;
    private Form evenements;
    private Container cnt2;
    private TextComponent recherche;

    public AfficherEvenementsForm() {
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        evenements = new Form("Evènements", new FlowLayout(Container.CENTER, Container.TOP));
        evenements.getToolbar().addCommandToLeftBar("Index", FontImage.createFixed("\ue815", fnt, 0xffffff, size, size), ev -> {
            IndexForm in = new IndexForm();
            in.getIndex().show();
        });
        evenements.getToolbar().addCommandToOverflowMenu("Ajouter Evènement", FontImage.createFixed("\uf271", fnt, 0x000000, size, size), ev -> {
            AjouterEvenementForm amef = new AjouterEvenementForm();
            amef.getAjouter().show();
        });
        evenements.getToolbar().addCommandToOverflowMenu("Mes Evènements", FontImage.createFixed("\uecf3", fnt, 0x000000, size, size), ev -> {
            AfficherMesEvenementsForm amef = new AfficherMesEvenementsForm();
            amef.getEvenements().show();
        });
        evenements.getToolbar().addCommandToOverflowMenu("Recommandations", FontImage.createFixed("\ueb05", fnt, 0x000000, size, size), ev -> {
            AfficherEvenementsRecommandationsForm aerf = new AfficherEvenementsRecommandationsForm();
            aerf.getEvenements().show();
        });
        cnt2 = new Container(BoxLayout.y());
        recherche = new TextComponent().label("Recherche :").hint(FontImage.createFixed("\ue81b", fnt, 0x000000, size, size));
        recherche.setPreferredW(900);
        recherche.getField().addActionListener(d -> {
            cnt2.removeAll();
            EvenementService ess = new EvenementService();
            List<Evenement> lee = ess.rechercher(recherche.getField().getText());
            if(lee.isEmpty())
                cnt2.add(new Label("Aucun résultat"));
            for (Evenement ee : lee) {
                cnt2.add(addComponent(ee));
            }
            cnt2.revalidate();
        });
        evenements.add(recherche);
        EvenementService es = new EvenementService();
        List<Evenement> le = es.getAllEvenementsAcceptes();
        for (Evenement e : le) {
            cnt2.add(addComponent(e));
        }
        cnt2.revalidate();
        evenements.add(cnt2);
    }

    public Form getEvenements() {
        return evenements;
    }

    private Container addComponent(Evenement e) {
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font smallPlainSystemFont = Font.createSystemFont(Font.FACE_MONOSPACE, Font.STYLE_BOLD, Font.SIZE_SMALL);
        Font largePlainSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);

        EncodedImage enc = EncodedImage.createFromImage(Image.createImage(690, 500, 0xffffffff), true);
        String url = "http://localhost/PiDev/web/getImage.php?file=" + e.getImageId();
        URLImage urlimage = URLImage.createToStorage(enc, "evsimg" + e.getImageId(), url);
        ImageViewer iv = new ImageViewer(urlimage);
        Container cnt = new Container(BoxLayout.y());
        cnt.setUIID("conatiner_ev");
        Container cnt2 = new Container(BoxLayout.x());
        cnt.setPreferredSize(new Dimension(900, 1000));
        cnt.getAllStyles().setMargin(20, 20, 30, 30);
        cnt.getAllStyles().setBackgroundType(BACKGROUND_NONE);
        cnt.getAllStyles().setBgTransparency(160);
        cnt.getAllStyles().setBgColor(0x1AA09F);
        cnt.getAllStyles().setPadding(0, 1, 0, 0);
        Label prix = createForFont(smallPlainSystemFont, e.getPrix() + "DT");
        prix.setPreferredSize(new Dimension(205, iv.getHeight()));
        prix.getAllStyles().setBgTransparency(100);
        prix.getAllStyles().setBgColor(0x000000);
        prix.getAllStyles().setAlignment(Label.CENTER);
        prix.getAllStyles().setFgColor(0xffffff);
        Label nom = createForFont(largePlainSystemFont, e.getType().toUpperCase() + ": " + e.getNom());
        nom.getAllStyles().setAlignment(Container.CENTER);
        nom.getAllStyles().setFgColor(0xffffff);
        Label salle = new Label(e.getSalleId().getNom());
        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM y H:mm");
        Label dd = new Label(sdf.format(e.getDateDebut()));
        Label df = new Label(sdf.format(e.getDateFin()));
        dd.setIcon(FontImage.createFixed("\uf251", fnt, 0xffffff, size, size));
        df.setIcon(FontImage.createFixed("\uf253", fnt, 0xffffff, size, size));
        dd.getAllStyles().setFgColor(0xffffff);
        df.getAllStyles().setFgColor(0xffffff);
        cnt2.add(iv);
        cnt2.add(prix);
        cnt.add(cnt2);
        cnt.add(nom);
        cnt.add(salle);
        cnt.add(dd);
        cnt.add(df);
        Button btn = new Button();
        btn.addActionListener(eee -> {
            AfficherEvenementForm aef = new AfficherEvenementForm(e.getId(), 1);
            aef.getEvenement().show();
        });
        cnt.setLeadComponent(btn);
        cnt.revalidate();
        cnt.getAllStyles().setBorder(Border.createLineBorder(4, 0x000000));
        return cnt;
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getAllStyles().setFont(fnt);
        return l;
    }
}
