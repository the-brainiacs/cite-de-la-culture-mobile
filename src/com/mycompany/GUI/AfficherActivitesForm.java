/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Club;
import com.mycompany.Service.ClubService;
import java.util.ArrayList;
import java.util.List;

public class AfficherActivitesForm {

    private Resources theme;
    private Form activites, clubs;
    private Container ctn2;
    private Label lb;

    public AfficherActivitesForm() {
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        activites = new Form("Activités", new FlowLayout(Container.CENTER, Container.TOP));
        activites.getToolbar().addCommandToLeftBar("Index", FontImage.createFixed("\ue815", fnt, 0xffffff, size, size), ev -> {
            IndexForm in = new IndexForm();
            in.getIndex().show();
        });

        activites.getToolbar().addCommandToOverflowMenu("Clubs", FontImage.createFixed("\uecf3", fnt, 0x000000, size, size), ev -> {
            AfficherClubForm affC = new AfficherClubForm();
            affC.getClubs().show();

        });

        activites.getToolbar().addCommandToOverflowMenu("Workshops", null, ev -> {
            AfficherWorkshopForm affW = new AfficherWorkshopForm();
            affW.getWorkshop().show();
        });

    }

    public Form getActivites() {
        return activites;
    }

}
