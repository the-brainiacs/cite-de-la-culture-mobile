/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.BrowserComponent;
import com.codename1.ui.Button;
import static com.codename1.ui.CN.log;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextComponent;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Evenement;
import com.mycompany.Entities.Reservation;
import com.mycompany.Entities.User;
import com.mycompany.MyApplication;
import com.mycompany.Service.EvenementService;
import com.mycompany.Service.PanierService;
import com.mycompany.Service.UserService;
import java.io.IOException;
import java.util.List;
import javafx.scene.paint.Color;





public class AfficherPanierForm {
    private Resources theme;
    private Form panier;
    private Button payer,supprimer,plus,moins,voir;
    private Button btnl;
    private Container cnt,cnt2,cnt4,cnt44,cnt3,cnt11, cntl2, cntl3,cnt5,cnt33;
    private TextComponent total;
    private Label log;
    private int qq ;
  
     
    public AfficherPanierForm()
    {
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        panier = new Form("Panier", new FlowLayout(Container.CENTER, Container.TOP));
        panier.getToolbar().addCommandToLeftBar("Index", FontImage.createFixed("\ue815", fnt, 0xffffff, size, size), ev -> {
            IndexForm in = new IndexForm();
            in.getIndex().show();
        });
        cntl2 = new Container(new FlowLayout(Container.CENTER, Container.TOP));
        log = createForFont(largeBoldSystemFont, "Mes Participations");
        log.getAllStyles().setFgColor(Color.INDIGO.hashCode());
        //log.getStyle().setFgColor(0x1AA09F);
        cntl2.add(log);
        panier.add(cntl2);
         cnt = new Container(BoxLayout.y());
         cnt4 = new Container(BoxLayout.y());
        PanierService es = new PanierService();
        List<Reservation> le = es.getAllReservations();
        for (Reservation re : le) {
            cnt.add(addComponent(re));
        }
           
       
          cnt11 = new Container(BoxLayout.y());
        
        
     
        
         voir = new Button("Continuer la réservation", FontImage.createFixed("\uec64", fnt, 0xffffff, size, size));
        voir = setButton(voir, 0x1AA09F, 0xffffff, 1);
        //voir.getAllStyles().setFgColor(Color.ORANGE.hashCode());

        voir.addActionListener(ee -> {
         
        AfficherEvenementsForm aef = new AfficherEvenementsForm();
            aef.getEvenements().show();
        });
         
        payer = new Button("Checkout", FontImage.createFixed("\ue856", fnt, 0xffffff, size, size));
        payer = setButton(payer, 0x9C1BDD, 0xffffff, 1);
        
        payer.addActionListener(ee -> {
         
       AfficherFactureForm aeff = new AfficherFactureForm();
                aeff.getFacture().show();
        });
        
     
        cnt.add(voir);
        cnt.add(cnt11);
        //cnt.add(cnt11);
        cnt.add(payer);
        panier.add(cnt);
   
    }

   
    public Form getPanier()
    {
        return panier;
    }
    
    public Container addComponent(Reservation e) {
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font smallPlainSystemFont = Font.createSystemFont(Font.FACE_MONOSPACE, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        Font largePlainSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        Container cnt = new Container(BoxLayout.y());
        Container cnt2 = new Container(BoxLayout.x());
        cnt.setPreferredSize(new Dimension(1000, 800));
        cnt.getStyle().setMargin(20, 20, 30, 30);
        cnt.getStyle().setBorder(Border.createLineBorder(4, 0x00000000));
        EncodedImage enc = EncodedImage.createFromImage(Image.createImage(250, 200, 0xffffffff), true);
        String url = "http://localhost/PiDev/web/getImage.php?file=" + e.getIdEvent().getImageId();
        System.out.println(url);
        URLImage urlimage = URLImage.createToStorage(enc, "Couv" + e.getIdEvent().getNom(), url);
        ImageViewer iv = new ImageViewer(urlimage);
        
       
       
        Label event = new Label(e.getIdEvent().getNom());
        event.getAllStyles().setFgColor(Color.INDIGO.hashCode());
       // Label des = new Label(e.getIdEvent().getDescription());
        Label des = createForFont(smallPlainSystemFont, e.getIdEvent().getDescription());
        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM y H:m");
        Label dd = new Label(sdf.format(e.getDateReservation()));
        Label prix = createForFont(smallPlainSystemFont, e.getPrix()+"DT");
        //Label qu = createForFont(smallPlainSystemFont, e.getQuantite()+"");
        Label q = createForFont(smallPlainSystemFont,"Quantité :");
        //Label q = new Label("Quantité :");
        //qq = e.getQuantite();
        TextField quantite = new TextField();
        quantite.setText(String.valueOf(e.getQuantite()));
        quantite.setEditable(false);
        quantite.setPreferredSize(new Dimension(100,100));
        //quantite.setText().valueOf(e.getQuantite());
    
        dd.setIcon(FontImage.createFixed("\uf251", fnt, 0x000000, size, size));
        //cnt4 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        //cnt4.getStyle().setPadding(20, 20, 20, 20);
        Container cnt4 = new Container((new FlowLayout(Container.LEFT)));
        Container cnt44 = new Container(BoxLayout.x());
        cnt44.getStyle().setPadding(50, 50, 50, 50);
        cnt3 = new Container(new FlowLayout(Container.RIGHT , Container.TOP));
        cnt3.getStyle().setPadding(20, 20, 20, 20);
        cnt33 = new Container(new FlowLayout(Container.LEFT, Container.TOP));
         //cnt3.getStyle().setPadding(20, 20, 20, 20);
        cnt5 = new Container(new FlowLayout(Container.RIGHT, Container.TOP));
        cnt5.getStyle().setPadding(20, 40, 40, 20);
       
        
        supprimer = new Button("", FontImage.createFixed("\uedba", fnt, 0xff0000, size, size));
        supprimer = setButton(supprimer, 0xffffff, 0xffffff, 1);
        supprimer.addActionListener(ee -> {
            boolean bool = Dialog.show("Suppression", " Vous êtes sur de supprimer l'évènement  " + e.getIdEvent().getNom() + " ?", "Oui", "Annuler");
            if (bool) {
                Dialog.show("Suppression Réussie", "L'évènement  " + e.getIdEvent().getNom() + " a été supprimé", "OK", null);
              PanierService ps = new PanierService();
              
              ps.supprimer(e.getId());
                AfficherPanierForm apf = new AfficherPanierForm();
               apf.getPanier().show();
            }

        });
        plus = new Button("", FontImage.createFixed("\ue8dd", fnt, 0x000000, size, size));
        plus = setButton(plus, 0xffffff, 0xffffff, 1);
        plus.addActionListener(ee -> {
           
          
        quantite.setText(String.valueOf(e.getQuantite()+1));

       PanierService ps = new PanierService();
        ps.modifier(e.getId(),e.getQuantite()+1);
       
       Dialog.show("Modification Réussie", "La quantité de l'évènement  " + e.getIdEvent().getNom() + " a été modifiée avec succes", "OK", null);

        AfficherPanierForm apf = new AfficherPanierForm();
        apf.getPanier().show();

        });
        
        moins = new Button("", FontImage.createFixed("\ue8e0", fnt, 0x000000, size, size));
        moins = setButton(moins, 0xffffff, 0xffffff, 1);
        moins.addActionListener(ee -> {
           qq=e.getQuantite() ;
            if(qq!=1)
            {
                quantite.setText(String.valueOf(e.getQuantite()-1));

       
       PanierService ps = new PanierService();
        ps.modifier(e.getId(),e.getQuantite()-1);
        Dialog.show("Modification Réussie", "La quantité de l'évènement  " + e.getIdEvent().getNom() + " a été modifiée avec succes", "OK", null);

       
        AfficherPanierForm apf = new AfficherPanierForm();
        apf.getPanier().show(); 
            } else 
            {Dialog.show("Modification", "vous ne pouvez pas modifier la quantité", "OK", null);

       
        AfficherPanierForm apf = new AfficherPanierForm();
        apf.getPanier().show(); 
            }
                
                    
       
           

        });
        cnt2.add(iv);
        cnt2.add(event);
        //cnt5.add(prix);
       // cnt2.add(cnt5);
        
        cnt.add(cnt2);
       
        
        cnt.add(des);
       // cnt.add(dd);
        cnt.add(prix);
        //cnt.add(qu);
        
        
        //cnt4.add(prix);
        cnt4.add(q);
        cnt4.add(moins);
        cnt4.add(quantite);
        cnt4.add(plus);
        
        //cnt4.add(qu);
         cnt33.add(dd);
        cnt3.add(supprimer);
        
        
        
        
        cnt.add(cnt4);
        cnt.add(cnt33);
        cnt.add(cnt3);
        
        
        
        Button btn = new Button();
        btn.addActionListener(eee->{
            AfficherPanierForm apf = new AfficherPanierForm();
            apf.getPanier().show();
        });
      //  cnt.setLeadComponent(btn);

        cnt.revalidate();
        return cnt;
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getStyle().setFont(fnt);
        return l;
    }
    
     private Button setButton(Button btn, int color1, int color2, int mar) {
        btn.setUIID("btn");
        btn.getStyle().setBgTransparency(220);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(mar, mar, mar * 10, mar * 10);
        btn.getPressedStyle().setBgTransparency(175);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(mar, mar, mar * 10, mar * 10);
        btn.getSelectedStyle().setBgTransparency(175);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(mar, mar, mar * 10, mar * 10);
        return btn;
    }
}