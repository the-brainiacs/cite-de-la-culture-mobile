/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.ext.filechooser.FileChooser;
import com.codename1.io.FileSystemStorage;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.PickerComponent;
import com.codename1.ui.TextComponent;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.plaf.Border;
import com.codename1.util.regex.RE;
import com.mycompany.Entities.Evenement;
import com.mycompany.Entities.Expose;
import com.mycompany.Entities.Festival;
import com.mycompany.Entities.Film;
import com.mycompany.Entities.Formation;
import com.mycompany.Entities.Musique;
import com.mycompany.Entities.Salle;
import com.mycompany.Entities.Theatre;
import com.mycompany.MyApplication;
import com.mycompany.Service.EvenementService;
import com.mycompany.Service.SalleService;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 *
 * @author missaoui
 */
public class AjouterEvenementForm {

    private Form ajouter;
    private Container cnt, cnt2, cnt3, cnt5, cnt6, cnt7;
    private Label reg, i, sal;
    private TextComponent disponibles, nom, desc, prix, prixEt, acteurs, realisateurs, duree, artistes, auteurs, participants, locuteurs;
    private PickerComponent anneeSortie, dd, df, type, age, genre, genreM, genreF, typeF, themeF, themeE;
    private ComboBox<Salle> salle;
    private ImageViewer iv;
    private Button btn, imgChooser;
    private String file = "";

    public AjouterEvenementForm() {
        EvenementService es = new EvenementService();
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM y H:mm");
        SimpleDateFormat sdf2 = new SimpleDateFormat("E, d MMM y");
        ajouter = new Form("Ajouter ");
        ajouter.getToolbar().addCommandToLeftBar("Back", FontImage.createFixed("\uecc6", fnt, 0xffffff, size, size), ev -> {
            AfficherEvenementsForm aef = new AfficherEvenementsForm();
            aef.getEvenements().show();
        });
        i = new Label("");
        iv = new ImageViewer();
        iv.setPreferredH(500);
        reg = createForFont(largeBoldSystemFont, "Ajouter Evenement");
        reg.getAllStyles().setFgColor(0x00ff00);
        reg.getAllStyles().setAlignment(Label.CENTER);
        cnt = new Container(BoxLayout.y());
        cnt3 = new Container(BoxLayout.x());
        cnt3.getAllStyles().setMargin(10, 10, 20, 10);
        cnt5 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        cnt6 = new Container(BoxLayout.y());
        cnt7 = new Container(BoxLayout.y());
        cnt7.getAllStyles().setBorder(Border.createLineBorder(3, 0x00ff00));
        cnt7.getAllStyles().setMargin(0, 0, 20, 20);
        nom = new TextComponent().label("Nom :");
        dd = PickerComponent.createDateTime(new Date()).label("Date Début :");
        dd.getPicker().setType(Display.PICKER_TYPE_DATE_AND_TIME);
        dd.getPicker().setDate(new Date());
        df = PickerComponent.createDateTime(new Date()).label("Date Fin :");
        df.getPicker().setType(Display.PICKER_TYPE_DATE_AND_TIME);
        df.getPicker().setDate(new Date());
        disponibles = new TextComponent().label("Disponibles :");
        type = PickerComponent.createStrings("film", "theatre", "festival", "formation", "expose", "musique").label("Type :");
        type.getPicker().setType(Display.PICKER_TYPE_STRINGS);
        type.getPicker().setStrings("film", "theatre", "festival", "formation", "expose", "musique");
        cnt2 = new Container(BoxLayout.x());
        cnt2.getAllStyles().setPaddingLeft(20);
        sal = new Label("Salle :");
        salle = new ComboBox<>();
        age = PickerComponent.createStrings("tout_public", "interdit_12_ans", "pour_enfants").label("Age :");
        age.getPicker().setType(Display.PICKER_TYPE_STRINGS);
        age.getPicker().setStrings("tout_public", "interdit_12_ans", "pour_enfants");
        prix = new TextComponent().label("Prix :");
        prix.getField().setConstraint(TextField.NUMERIC);
        prixEt = new TextComponent().label("Prix Etudiants :");
        prixEt.getField().setConstraint(TextField.NUMERIC);
        desc = new TextComponent().label("Description :");
        desc.getField().setGrowByContent(true);
        desc.getField().setGrowLimit(-1);
        imgChooser = new Button("Image", FontImage.createFixed("\ue827", fnt, 0xffffff, size, size));
        imgChooser = setButton(imgChooser, 0xB95656, 0xfffffff, 0);
        imgChooser.addActionListener(eee -> {
            if (FileChooser.isAvailable()) {
                FileChooser.showOpenDialog(".jpg, .bmp, .png, .gif, .jpeg", e2 -> {
                    if (e2 != null) {
                        file = (String) e2.getSource();
                    }
                    if (file == null) {
                        i.setText("Aucune image");
                        cnt3.revalidate();
                    } else {
                        i.setText("");
                        String extension = null;
                        if (file.lastIndexOf(".") > 0) {
                            extension = file.substring(file.lastIndexOf(".") + 1);
                        }
                        if ("jpg".equals(extension) || "gif".equals(extension) || "jpeg".equals(extension) || "png".equals(extension) || "bmp".equals(extension)) {
                            FileSystemStorage fs = FileSystemStorage.getInstance();
                            try {
                                InputStream fis = fs.openInputStream(file);
                                iv.setImage(Image.createImage(fis));
                                cnt3.revalidate();
                            } catch (Exception ex) {
                                System.out.println(ex.getMessage());
                            }
                        }
                    }
                    cnt3.revalidate();
                });
            }
        });

        btn = new Button("Ajouter", FontImage.createFixed("\uf271", fnt, 0xffffff, size, size));
        btn = setButton(btn, 0x00ff00, 0xffffff, 30);
        btn.addActionListener(ee -> {
            boolean bool = false;
            Evenement e = new Evenement();
            if (type.getPicker().getSelectedString().equals("film") && (verif() & verifFilm())) {
                Film film = new Film(null, Integer.parseInt(duree.getText()), genre.getPicker().getSelectedString(), anneeSortie.getPicker().getDate(), realisateurs.getText(), acteurs.getText());
                e.setNom(nom.getText());
                e.setDateDebut(dd.getPicker().getDate());
                e.setDateFin(df.getPicker().getDate());
                e.setDisponibles(Integer.parseInt(disponibles.getText()));
                e.setType(type.getPicker().getSelectedString());
                e.setDescription(desc.getText());
                e.setAge(age.getPicker().getSelectedString());
                e.setPrix(Double.parseDouble(prix.getText()));
                e.setPrixEtudiants(Double.parseDouble(prixEt.getText()));
                e.setImageId(file);
                e.setSalleId(salle.getSelectedItem());
                e.setUserId(MyApplication.CurrentUser);
                e = es.ajouterFilm(e, film);
                bool = true;
            }
            if (type.getPicker().getSelectedString().equals("theatre") && (verif() & verifTheatre())) {
                Theatre theatre = new Theatre(null, genre.getPicker().getSelectedString(), anneeSortie.getPicker().getDate(), realisateurs.getText(), auteurs.getText(), artistes.getText());
                e.setNom(nom.getText());
                e.setDateDebut(dd.getPicker().getDate());
                e.setDateFin(df.getPicker().getDate());
                e.setDisponibles(Integer.parseInt(disponibles.getText()));
                e.setType(type.getPicker().getSelectedString());
                e.setDescription(desc.getText());
                e.setAge(age.getPicker().getSelectedString());
                e.setPrix(Double.parseDouble(prix.getText()));
                e.setPrixEtudiants(Double.parseDouble(prixEt.getText()));
                e.setImageId(file);
                e.setSalleId(salle.getSelectedItem());
                e.setUserId(MyApplication.CurrentUser);
                e = es.ajouterTheatre(e, theatre);
                bool = true;
            }
            if (type.getPicker().getSelectedString().equals("festival") && (verif() & verifFestival())) {
                Festival festival = new Festival(null, participants.getText(), typeF.getPicker().getSelectedString(), genreF.getPicker().getSelectedString());
                e.setNom(nom.getText());
                e.setDateDebut(dd.getPicker().getDate());
                e.setDateFin(df.getPicker().getDate());
                e.setDisponibles(Integer.parseInt(disponibles.getText()));
                e.setType(type.getPicker().getSelectedString());
                e.setDescription(desc.getText());
                e.setAge(age.getPicker().getSelectedString());
                e.setPrix(Double.parseDouble(prix.getText()));
                e.setPrixEtudiants(Double.parseDouble(prixEt.getText()));
                e.setImageId(file);
                e.setSalleId(salle.getSelectedItem());
                e.setUserId(MyApplication.CurrentUser);
                e = es.ajouterFestival(e, festival);
                bool = true;
            }
            if (type.getPicker().getSelectedString().equals("formation") && (verif() & verifFormation())) {
                Formation formation = new Formation(null, locuteurs.getText(), themeF.getPicker().getSelectedString());
                e.setNom(nom.getText());
                e.setDateDebut(dd.getPicker().getDate());
                e.setDateFin(df.getPicker().getDate());
                e.setDisponibles(Integer.parseInt(disponibles.getText()));
                e.setType(type.getPicker().getSelectedString());
                e.setDescription(desc.getText());
                e.setAge(age.getPicker().getSelectedString());
                e.setPrix(Double.parseDouble(prix.getText()));
                e.setPrixEtudiants(Double.parseDouble(prixEt.getText()));
                e.setImageId(file);
                e.setSalleId(salle.getSelectedItem());
                e.setUserId(MyApplication.CurrentUser);
                e = es.ajouterFormation(e, formation);
                bool = true;
            }
            if (type.getPicker().getSelectedString().equals("expose") && (verif() & verifExpose())) {
                Expose expose = new Expose(null, locuteurs.getText(), themeE.getPicker().getSelectedString());
                e.setNom(nom.getText());
                e.setDateDebut(dd.getPicker().getDate());
                e.setDateFin(df.getPicker().getDate());
                e.setDisponibles(Integer.parseInt(disponibles.getText()));
                e.setType(type.getPicker().getSelectedString());
                e.setDescription(desc.getText());
                e.setAge(age.getPicker().getSelectedString());
                e.setPrix(Double.parseDouble(prix.getText()));
                e.setPrixEtudiants(Double.parseDouble(prixEt.getText()));
                e.setImageId(file);
                e.setSalleId(salle.getSelectedItem());
                e.setUserId(MyApplication.CurrentUser);
                e = es.ajouterExpose(e, expose);
                bool = true;
            }
            if (type.getPicker().getSelectedString().equals("musique") && (verif() & verifMusique())) {
                Musique musique = new Musique(null, artistes.getText(), genreM.getPicker().getSelectedString());
                e.setNom(nom.getText());
                e.setDateDebut(dd.getPicker().getDate());
                e.setDateFin(df.getPicker().getDate());
                e.setDisponibles(Integer.parseInt(disponibles.getText()));
                e.setType(type.getPicker().getSelectedString());
                e.setDescription(desc.getText());
                e.setAge(age.getPicker().getSelectedString());
                e.setPrix(Double.parseDouble(prix.getText()));
                e.setPrixEtudiants(Double.parseDouble(prixEt.getText()));
                e.setImageId(file);
                e.setSalleId(salle.getSelectedItem());
                e.setUserId(MyApplication.CurrentUser);
                e = es.ajouterMusique(e, musique);
                bool = true;
            }
            if (bool) {
                Dialog.show("Ajout Réussi","L'évènement "+e.getNom()+" a été ajouté sans échec","OK",null);
                AfficherEvenementForm mpf = new AfficherEvenementForm(e.getId(), 2);
                mpf.getEvenement().show();
            }
        });
        dd.getPicker().addActionListener(e -> {
            RE pattern = new RE("^[0-9]*$");
            if (!disponibles.getText().equals("") && pattern.match(disponibles.getField().getText())) {
                ((DefaultListModel) salle.getModel()).removeAll();
                SalleService ss = new SalleService();
                List<Salle> salles = ss.getSalles(Integer.parseInt(disponibles.getText()), dd.getPicker().getDate(), df.getPicker().getDate(), type.getPicker().getSelectedString());
                for (Salle s : salles) {
                    salle.addItem(s);
                }
                cnt7.revalidate();
            }
        });
        df.getPicker().addActionListener(e -> {
            RE pattern = new RE("^[0-9]*$");
            if (!disponibles.getText().equals("") && pattern.match(disponibles.getField().getText())) {
                ((DefaultListModel) salle.getModel()).removeAll();
                SalleService ss = new SalleService();
                List<Salle> salles = ss.getSalles(Integer.parseInt(disponibles.getText()), dd.getPicker().getDate(), df.getPicker().getDate(), type.getPicker().getSelectedString());
                for (Salle s : salles) {
                    salle.addItem(s);
                }
                cnt7.revalidate();
            }
        });
        disponibles.getField().addActionListener(e -> {
            RE pattern = new RE("^[0-9]*$");
            if (!disponibles.getText().equals("") && pattern.match(disponibles.getField().getText())) {
                ((DefaultListModel) salle.getModel()).removeAll();
                SalleService ss = new SalleService();
                List<Salle> salles = ss.getSalles(Integer.parseInt(disponibles.getText()), dd.getPicker().getDate(), df.getPicker().getDate(), type.getPicker().getSelectedString());
                for (Salle s : salles) {
                    salle.addItem(s);
                }
                cnt7.revalidate();
            }
        });
        type.getPicker().addActionListener(e -> {
            RE pattern = new RE("^[0-9]*$");
            if (!disponibles.getText().equals("") && pattern.match(disponibles.getField().getText())) {
                ((DefaultListModel) salle.getModel()).removeAll();
                SalleService ss = new SalleService();
                List<Salle> salles = ss.getSalles(Integer.parseInt(disponibles.getText()), dd.getPicker().getDate(), df.getPicker().getDate(), type.getPicker().getSelectedString());
                for (Salle s : salles) {
                    salle.addItem(s);
                }
                cnt7.revalidate();
            }
            cnt6.removeAll();
            if (type.getPicker().getSelectedString().equals("film")) {
                acteurs = new TextComponent().label("Acteurs :");
                realisateurs = new TextComponent().label("Réalisateurs :");
                duree = new TextComponent().label("Durée :");
                anneeSortie = PickerComponent.createDate(new Date()).label("Date de Sortie :");
                genre = PickerComponent.createStrings("action", "drame", "comedie", "romance", "historique", "western", "aventure", "thriller", "opera", "science_fiction", "horreur", "fantasie", "biographie").label("Genre :");
                genre.getPicker().setType(Display.PICKER_TYPE_STRINGS);
                genre.getPicker().setStrings("action", "drame", "comedie", "romance", "historique", "western", "aventure", "thriller", "opera", "science_fiction", "horreur", "fantasie", "biographie");
                cnt6.add(acteurs);
                cnt6.add(realisateurs);
                cnt6.add(genre);
                cnt6.add(duree);
                cnt6.add(anneeSortie);
            }
            if (type.getPicker().getSelectedString().equals("theatre")) {
                artistes = new TextComponent().label("Artistes :");
                realisateurs = new TextComponent().label("Réalisateurs :");
                auteurs = new TextComponent().label("Auteurs :");
                anneeSortie = PickerComponent.createDate(new Date()).label("Date de Sortie :");
                genre = PickerComponent.createStrings("action", "drame", "comedie", "romance", "historique", "western", "aventure", "thriller", "opera", "science_fiction", "horreur", "fantasie", "biographie").label("Genre :");
                genre.getPicker().setType(Display.PICKER_TYPE_STRINGS);
                genre.getPicker().setStrings("action", "drame", "comedie", "romance", "historique", "western", "aventure", "thriller", "opera", "science_fiction", "horreur", "fantasie", "biographie");
                cnt6.add(artistes);
                cnt6.add(realisateurs);
                cnt6.add(genre);
                cnt6.add(auteurs);
                cnt6.add(anneeSortie);
            }
            if (type.getPicker().getSelectedString().equals("festival")) {
                participants = new TextComponent().label("Participants :");
                genreF = PickerComponent.createStrings("sportif", "culturel", "humour", "voyage", "bande_dessinee", "cirque", "cinema", "danse", "musique", "photographie", "theatre", "litterature", "jeux").label("Genre :");
                genreF.getPicker().setType(Display.PICKER_TYPE_STRINGS);
                genreF.getPicker().setStrings("sportif", "culturel", "humour", "voyage", "bande_dessinee", "cirque", "cinema", "danse", "musique", "photographie", "theatre", "litterature", "jeux");
                typeF = PickerComponent.createStrings("international", "national", "regional", "local").label("Type :");
                typeF.getPicker().setType(Display.PICKER_TYPE_STRINGS);
                typeF.getPicker().setStrings("international", "national", "regional", "local");
                cnt6.add(participants);
                cnt6.add(genreF);
                cnt6.add(typeF);
            }
            if (type.getPicker().getSelectedString().equals("formation")) {
                locuteurs = new TextComponent().label("Locuteurs :");
                themeF = PickerComponent.createStrings("informatique", "bureautique", "qualite_organisation", "comptabilite", "langues", "transport", "marketing", "social", "communication", "securite", "management").label("Theme :");
                themeF.getPicker().setType(Display.PICKER_TYPE_STRINGS);
                themeF.getPicker().setStrings("informatique", "bureautique", "qualite_organisation", "comptabilite", "langues", "transport", "marketing", "social", "communication", "securite", "management");
                cnt6.add(locuteurs);
                cnt6.add(themeF);
            }
            if (type.getPicker().getSelectedString().equals("expose")) {
                locuteurs = new TextComponent().label("Locuteurs :");
                themeE = PickerComponent.createStrings("histoire", "geographie", "sciences", "musique", "civil", "culture", "economie").label("Theme :");
                themeE.getPicker().setType(Display.PICKER_TYPE_STRINGS);
                themeE.getPicker().setStrings("histoire", "geographie", "sciences", "musique", "civil", "culture", "economie");
                cnt6.add(locuteurs);
                cnt6.add(themeE);
            }
            if (type.getPicker().getSelectedString().equals("musique")) {
                artistes = new TextComponent().label("Artistes :");
                genreM = PickerComponent.createStrings("blues", "disco", "funk", "pop", "country", "folk", "jazz", "rai", "rap", "rock", "soul", "reggae", "edm", "classique", "orientale").label("Genre :");
                genreM.getPicker().setType(Display.PICKER_TYPE_STRINGS);
                genreM.getPicker().setStrings("blues", "disco", "funk", "pop", "country", "folk", "jazz", "rai", "rap", "rock", "soul", "reggae", "edm", "classique", "orientale");
                cnt6.add(artistes);
                cnt6.add(genreM);
            }
            cnt6.revalidate();
        });
        acteurs = new TextComponent().label("Acteurs :");
        realisateurs = new TextComponent().label("Réalisateurs :");
        duree = new TextComponent().label("Durée :");
        anneeSortie = PickerComponent.createDate(new Date()).label("Date de Sortie :");
        genre = PickerComponent.createStrings("action", "drame", "comedie", "romance", "historique", "western", "aventure", "thriller", "opera", "science_fiction", "horreur", "fantasie", "biographie").label("Genre :");
        genre.getPicker().setType(Display.PICKER_TYPE_STRINGS);
        genre.getPicker().setStrings("action", "drame", "comedie", "romance", "historique", "western", "aventure", "thriller", "opera", "science_fiction", "horreur", "fantasie", "biographie");
        cnt6.add(acteurs);
        cnt6.add(realisateurs);
        cnt6.add(genre);
        cnt6.add(duree);
        cnt6.add(anneeSortie);
        cnt.add(reg);
        cnt7.add(nom);
        cnt7.add(dd);
        cnt7.add(df);
        cnt7.add(disponibles);
        cnt7.add(type);
        cnt2.add(sal);
        cnt2.add(salle);
        cnt7.add(cnt2);
        cnt7.add(age);
        cnt7.add(prix);
        cnt7.add(prixEt);
        cnt7.add(desc);
        cnt5.add(imgChooser);
        cnt5.add(i);
        cnt3.add(cnt5);
        cnt3.add(iv);
        cnt7.add(cnt3);
        cnt7.add(cnt6);
        cnt7.add(btn);
        cnt.add(cnt7);
        ajouter.add(cnt);
        ajouter.setEditOnShow(nom.getField());
    }

    public Form getAjouter() {
        return ajouter;
    }

    private Button setButton(Button btn, int color1, int color2, int mar) {
        btn.setUIID("btn");
        btn.getStyle().setBgTransparency(255);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(mar, mar, mar * 10, mar * 10);
        btn.getPressedStyle().setBgTransparency(175);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(mar, mar, mar * 10, mar * 10);
        btn.getSelectedStyle().setBgTransparency(175);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(mar, mar, mar * 10, mar * 10);
        return btn;
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getAllStyles().setFont(fnt);
        return l;
    }

    public boolean verif() {
        boolean bool = true;
        desc.errorMessage("");
        nom.errorMessage("");
        prix.errorMessage("");
        prixEt.errorMessage("");
        disponibles.errorMessage("");
        dd.errorMessage("");
        df.errorMessage("");
        sal.setText("Salle :");
        if (salle.getSelectedItem()==null)
        {
            sal.setText("*Salle :");
            bool = false;
        }
        if (nom.getText().equals("")) {
            bool = false;
            nom.errorMessage("*Nom est requis");
        }
        if (desc.getText().equals("")) {
            bool = false;
            desc.errorMessage("*Description est requis");
        }
        if (prix.getText().equals("")) {
            bool = false;
            prix.errorMessage("*Prix est requis");
        }
        if (prixEt.getText().equals("")) {
            bool = false;
            prixEt.errorMessage("*Prix Etudiants est requis");
        }
        if (disponibles.getText().equals("")) {
            bool = false;
            disponibles.errorMessage("*Disponibles est requis");
        }
        RE pattern = new RE("^[0-9]*.[0-9]*$");
        RE pattern2 = new RE("^[0-9]*$");
        if (!pattern.match(prix.getText())) {
            bool = false;
            prix.errorMessage("*Prix doit être numérique");
        }
        if (!pattern.match(prixEt.getText())) {
            bool = false;
            prixEt.errorMessage("*Prix Etudiants doit être numérique");
        }
        if (!pattern2.match(disponibles.getText())) {
            bool = false;
            disponibles.errorMessage("*Disponibles doit être numérique");
        }
        if(pattern.match(prixEt.getText())&&pattern.match(prix.getText())&&Integer.parseInt(prix.getText())<Integer.parseInt(prixEt.getText()))
        {
            bool = false;
            prixEt.errorMessage("*Prix Etudiants doit être inférieur au prix");
        }
        if (file.equals("")) {
            i.setText("*L'image est requis");
            bool = false;
        }
        if ((int) (dd.getPicker().getDate().getTime() - df.getPicker().getDate().getTime()) >= 0) {
            bool = false;
            df.errorMessage("*La date début doit être avant la date fin");
        }
        if ((int) (dd.getPicker().getDate().getTime() - new Date().getTime()) < 0) {
            bool = false;
            dd.errorMessage("*La date début doit être après la date actuelle");
        }
        ajouter.revalidate();
        return bool;
    }

    public boolean verifFilm() {
        boolean bool = true;
        acteurs.errorMessage("");
        realisateurs.errorMessage("");
        duree.errorMessage("");
        if (acteurs.getText().equals("")) {
            bool = false;
            acteurs.errorMessage("*Acteurs est requis");
        }
        if (realisateurs.getText().equals("")) {
            bool = false;
            realisateurs.errorMessage("*Réalisateurs est requis");
        }
        if (duree.getText().equals("")) {
            bool = false;
            duree.errorMessage("*Durée est requis");
        }
        RE pattern = new RE("^[0-9]*$");
        if (!pattern.match(duree.getText())) {
            bool = false;
            duree.errorMessage("*Durée doit être numérique");
        }
        return bool;
    }

    public boolean verifTheatre() {
        boolean bool = true;
        artistes.errorMessage("");
        realisateurs.errorMessage("");
        auteurs.errorMessage("");
        if (artistes.getText().equals("")) {
            bool = false;
            artistes.errorMessage("*Artistes est requis");
        }
        if (realisateurs.getText().equals("")) {
            bool = false;
            realisateurs.errorMessage("*Réalisateurs est requis");
        }
        if (auteurs.getText().equals("")) {
            bool = false;
            auteurs.errorMessage("*Auteurs est requis");
        }
        return bool;
    }

    public boolean verifFestival() {
        boolean bool = true;
        participants.errorMessage("");
        if (participants.getText().equals("")) {
            bool = false;
            participants.errorMessage("*Participants est requis");
        }
        return bool;
    }

    public boolean verifFormation() {
        boolean bool = true;
        locuteurs.errorMessage("");
        if (locuteurs.getText().equals("")) {
            bool = false;
            locuteurs.errorMessage("*Locuteurs est requis");
        }
        return bool;
    }

    public boolean verifExpose() {
        boolean bool = true;
        locuteurs.errorMessage("");
        if (locuteurs.getText().equals("")) {
            bool = false;
            locuteurs.errorMessage("*Locuteurs est requis");
        }
        return bool;
    }

    public boolean verifMusique() {
        boolean bool = true;
        artistes.errorMessage("");
        if (artistes.getText().equals("")) {
            bool = false;
            artistes.errorMessage("*Artistes est requis");
        }
        return bool;
    }
}
