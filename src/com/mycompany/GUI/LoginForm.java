/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.Stroke;
import com.codename1.ui.TextComponent;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.RoundBorder;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.User;
import com.mycompany.MyApplication;
import com.mycompany.Service.UserService;

/**
 *
 * @author missaoui
 */
public class LoginForm {

    private Resources theme;
    private Form login;
    private Container cntl, cntl2, cntl3;
    private Label log;
    private TextComponent username, password;
    private Button btnl;

    public LoginForm() {
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        FontImage fm = FontImage.createFixed("\uecc6", fnt, 0xffffff, size, size);
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        login = new Form("Login", new FlowLayout(Container.CENTER, Container.CENTER));
        login.getToolbar().addCommandToLeftBar("Back", fm, e -> {
            IndexForm ind = new IndexForm();
            ind.getIndex().show();
        });
        cntl = new Container(BoxLayout.y());
        cntl2 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        log = createForFont(largeBoldSystemFont, "Se Connecter");
        log.getAllStyles().setFgColor(0x1AA09F);
        cntl3 = new Container(BoxLayout.y());
        cntl3.getAllStyles().setBorder(Border.createLineBorder(3, 0x1AA09F));
        cntl3.getAllStyles().setMargin(0, 0,20,20);
        username = new TextComponent().label("Username :");
        password = new TextComponent().label("Password :");
        password.getField().setConstraint(TextField.PASSWORD);
        btnl = new Button("Login",FontImage.createFixed("\ue86c", fnt, 0xffffff, size, size));
        btnl = setButton(btnl, 0x1AA09F, 0xffffff);
        btnl.addActionListener(e -> {
            if (verif()) {
                UserService us = new UserService();
                User u = us.login(username.getText(), password.getText());
                if (u != null) {
                    MyApplication.CurrentUser = u;
                    IndexForm ind = new IndexForm();
                    ind.getIndex().show();
                } else {
                    Dialog.show("Login Echec", "Votre password ou username est incorrect. Veuillez le vérifier.", "ok", null);
                }
            }
        });
        cntl.add(cntl2);
        cntl2.add(log);
        cntl3.add(username);
        cntl3.add(password);
        cntl3.add(btnl);
        cntl.add(cntl3);
        login.add(cntl);
        login.setEditOnShow(username.getField());
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getAllStyles().setFont(fnt);
        return l;
    }

    private Button setButton(Button btn, int color1, int color2) {
        btn.setUIID("btn");
        btn.getStyle().setBgTransparency(200);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(50, 20, 300, 300);
        btn.getPressedStyle().setBgTransparency(155);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(50, 20, 300, 300);
        btn.getSelectedStyle().setBgTransparency(155);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(50, 20, 300, 300);
        return btn;
    }

    public Form getLogin() {
        return login;
    }

    public boolean verif() {
        boolean bool = true;
        username.errorMessage("");
        password.errorMessage("");
        if (username.getText().equals("")) {
            bool = false;
            username.errorMessage("*Username est requis");
        }
        if (password.getText().equals("")) {
            bool = false;
            password.errorMessage("*Password est requis");
        }
        login.revalidate();
        return bool;
    }
}
