/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.table.Table;
import com.codename1.ui.table.TableModel;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Article;
import com.mycompany.Entities.Commentaire;
import com.mycompany.Entities.User;
import com.mycompany.MyApplication;
import com.mycompany.Service.ArticleService;
import com.mycompany.Service.CommentaireService;
import java.util.List;
import javafx.scene.paint.Color;

/**
 *
 * @author ASUS
 */
public class AfficherArticleForm {

    private Resources theme;
    private Form article;
    private Container cnt, cnt2, cnt3, cnt4, cnt5, cntcommentaire, cnt6;
    private ImageViewer iv;
    private Label nom, dd, df, salle, age, info;
    private Table tab;
    private TableModel model;
    private TextArea desc, contenu, prixEt;
    private Button modifier, supprimer;

    public AfficherArticleForm(int id) {

        ArticleService as = new ArticleService();
        Article e = as.getArticle(id);

        TextArea contenu = new TextArea();
        contenu.getAllStyles().setFgColor(0x000000);
        contenu.setHint("saisir un commentaire");

        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font smallPlainSystemFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        Font smallBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_SMALL);
        Font largePlainSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);

        article = new Form("Article", new FlowLayout(Container.CENTER, Container.TOP));
        article.getToolbar().addCommandToLeftBar("Index", FontImage.createFixed("\ue815", fnt, 0xffffff, size, size), ev -> {
            IndexForm in = new IndexForm();
            in.getIndex().show();
        });
        article.getToolbar().addCommandToRightBar("Back", FontImage.createFixed("\uecc6", fnt, 0xffffff, size, size), ev -> {
            AfficherArticlesForm aef = new AfficherArticlesForm();
            aef.getArticles().show();
        });

        cnt = new Container(BoxLayout.y());
        cnt.setUIID("cnt");
        cnt.getAllStyles().setBgTransparency(60);
        cnt.getAllStyles().setBgColor(0xffffff);
        cnt2 = new Container(BoxLayout.x());
        cnt3 = new Container(new FlowLayout(Container.CENTER, Container.TOP));
        cnt3.getAllStyles().setPadding(0, 5, 0, 0);
        cnt3.getAllStyles().setMargin(5, 0, 0, 0);
        cnt3.setUIID("cnt");
        cnt3.getAllStyles().setBgTransparency(120);
        cnt3.getAllStyles().setBgColor(0xf8f8ff);
        cnt3.setPreferredSize(new Dimension(article.getWidth() * 3 / 8, article.getWidth() * 3 / 5));
        EncodedImage enc = EncodedImage.createFromImage(Image.createImage(1200, 600, 0xffffffff), true);
        String url = "http://localhost/PiDev/web/upload/" + e.getImage();
        URLImage urlimage2 = URLImage.createToStorage(enc, "Couverture" + e.getImage(), url);
        iv = new ImageViewer(urlimage2);
        iv.getAllStyles().setMargin(0, 0, 0, 0);
        iv.getAllStyles().setPadding(0, 0, 0, 0);
        nom = createForFont(largePlainSystemFont, e.getTitre().toUpperCase());
        nom.getAllStyles().setAlignment(Label.CENTER);
        nom.getAllStyles().set3DText(true, true);
        desc = new TextArea(e.getContenu());
        desc.setEditable(false);
        desc.setFocusable(false);
        desc.getAllStyles().setFont(smallPlainSystemFont);
        desc.getAllStyles().setAlignment(TextArea.CENTER);
        desc.getAllStyles().setFgColor(Color.RED.hashCode());
        //   salle = new Label(e.getSalleId().getNom());
        //  salle.getAllStyles().setFgColor(Color.INDIGO.hashCode());
        //age = new Label("Age: " + e.getAge().replace('_', ' ').toUpperCase());
        //age.getAllStyles().setFgColor(Color.ORANGE.hashCode());
        //prix = new TextArea("Prix " + e.getPrix() + "DT");
        //prix.getAllStyles().setFont(smallBoldSystemFont);
        //prix.getAllStyles().setAlignment(TextArea.CENTER);
        //prix.getAllStyles().setFgColor(0xffffff);
        //prixEt = new TextArea("Prix Etudiants " + e.getPrixEtudiants() + "DT");
        //prixEt.getAllStyles().setFont(smallPlainSystemFont);
        //prixEt.getAllStyles().setAlignment(TextArea.CENTER);
        //prixEt.getAllStyles().setFgColor(0xffffff);
        //SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM y H:mm");
        SimpleDateFormat sdf2 = new SimpleDateFormat("E, d MMM y");
        dd = new Label(sdf2.format(e.getDateCreation()));

        dd.setIcon(FontImage.createFixed("\uf251", fnt, 0x000000, size, size));

        dd.getAllStyles().setFgColor(0x000000);
        //df.getAllStyles().setFgColor(0x000000);
        cnt4 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        cnt4.getStyle().setPadding(20, 20, 20, 20);

        cnt2.add(iv);

        //cnt2.add(cnt3);
        cnt.add(cnt2);
        cnt.add(nom);

        cnt.add(dd);

        cnt.add(desc);
        cnt5 = new Container(BoxLayout.y());
        info = createForFont(largePlainSystemFont, "Commentaires");

        CommentaireService cs = new CommentaireService();
        List<Commentaire> le = cs.AfficherCommentaire(id);
        cnt5.add(info);
        for (Commentaire a : le) {
            cnt5.add(addComponent(a));
        }

        Button ajouter = new Button("commenter");

        ajouter.addActionListener(cc -> {
            User currentuser = MyApplication.CurrentUser;
            CommentaireService c = new CommentaireService();
            c.ajouterCommentaire(contenu.getText(), currentuser.getId(), e);
            AfficherArticleForm aef = new AfficherArticleForm(e.getId());
            aef.getArticle().show();

        });

        cnt5.add(contenu);
        cnt5.add(ajouter);

        cnt.add(cnt5);
        article.add(cnt);

    }

    public Container addComponent(Commentaire e) {
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        cntcommentaire = new Container(BoxLayout.y());
        cnt6 = new Container(new FlowLayout(Container.RIGHT, Container.TOP));

        Font smallPlainSystemFont = Font.createSystemFont(Font.FACE_MONOSPACE, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        Font smallBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_SMALL);
        //Font MediumPlainSystemFont = Font.createSystemFont(Font.FACE_MONOSPACE, Font.STYLE_PLAIN, Font.SIZE_MEDIUM);
        Label nom = createForFont(smallBoldSystemFont, e.getUserId().getPrenom() + " " + e.getUserId().getNom());

        Label cont = createForFont(smallPlainSystemFont, e.getContenu());
        CommentaireService c = new CommentaireService();

        User currentuser = MyApplication.CurrentUser;
        int sig = c.NombreSignal(e, currentuser.getId());

        if (currentuser.getId() == e.getUserId().getId()) {
            Button action = new Button("Supprimer");
            Button like = new Button();
            action.getAllStyles().setBgColor(0x6d071a);
            Label date = new Label(String.valueOf(c.Nombrelike(e)));

            like.addActionListener(li -> {

                int nombre = c.like(e, currentuser.getId());

                String n = String.valueOf(nombre);
                date.setText(n);

            });
            like.setIcon(FontImage.createFixed("\ue8ef", fnt, 0xffffff, size, size));
            Label trait = new Label("__________________________________________________________________");
            trait.getAllStyles().setFgColor(0x000000);
            cntcommentaire.add(nom);
            cntcommentaire.add(cont);
            cnt6.add(date);
            cnt6.add(like);
            cnt6.add(action);

            cntcommentaire.add(cnt6);
            cntcommentaire.add(trait);
            action.addActionListener(l -> {

                boolean bool = Dialog.show("Suppression", " Vous êtes sur de vouloir supprimer ce commentaire ?", "OK", "Annuler");
                if (bool) {
                    int id = e.getArticleId().getId();
                    Dialog.show("Suppression Réussie", "Le commentaire  " + e.getContenu() + " a été supprimé sans échec", "OK", null);
                    c.SupprimerCommentaire(e.getId());
                    AfficherArticleForm aef = new AfficherArticleForm(id);
                    aef.getArticle().show();
                }

            }
            );
        } else {

            Button action = new Button("Signaler");
            action.addActionListener(ac -> {

                boolean bool = Dialog.show("Signaler", " Vous êtes sur de vouloir Signaler ce commentaire ?", "OK", "Annuler");
                if (bool) {
                    int id = currentuser.getId();
                    Dialog.show("Suppression Réussie", "Le commentaire  " + e.getContenu() + " a été signaler sans échec", "OK", null);
                    c.signal(e, id);
                    AfficherArticleForm aef = new AfficherArticleForm(e.getArticleId().getId());
                    aef.getArticle().show();
                }

            });
            Button like = new Button();
            like.setIcon(FontImage.createFixed("\ue8ef", fnt, 0xffffff, size, size));

            Label date = new Label(String.valueOf(c.Nombrelike(e)));
            like.addActionListener(li -> {

                int nombre = c.like(e, currentuser.getId());

                String n = String.valueOf(nombre);
                date.setText(n);

            });
            Label trait = new Label("__________________________________________________________________");
            trait.getAllStyles().setFgColor(0x000000);
            cntcommentaire.add(nom);
            cntcommentaire.add(cont);
            cnt6.add(date);
            cnt6.add(like);
            if (sig == 0) {
                cnt6.add(action);
            }
            cntcommentaire.add(cnt6);
            cntcommentaire.add(trait);
            action.addActionListener(l -> {

            }
            );

        }

        return cntcommentaire;

    }

    public Form getArticle() {
        return article;
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getAllStyles().setFont(fnt);
        return l;
    }

}
