/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextComponent;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Article;
import com.mycompany.Entities.Evenement;
import com.mycompany.Service.ArticleService;
import java.util.List;

public class AfficherArticlesForm {

    private Resources theme;
    private Form articles;
    private Container cnt;
    private TextComponent recherche;

    public AfficherArticlesForm() {
        recherche = new TextComponent().hint("rechercher");

        recherche.getField().addDataChangedListener((type, index) -> {

            recherche(recherche.getField().getText());

        });

        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        articles = new Form("Articles", new FlowLayout(Container.CENTER, Container.TOP));
        articles.getToolbar().addCommandToLeftBar("Index", FontImage.createFixed("\ue815", fnt, 0xffffff, size, size), ev -> {
            IndexForm in = new IndexForm();
            in.getIndex().show();
        });

        cnt = new Container(BoxLayout.y());
        articles.add(recherche);
        articles.add(cnt);

        ArticleService es = new ArticleService();
        List<Article> le = es.AfficherArticles();
        for (Article e : le) {
            cnt.add(addComponent(e));
        }
    }

    public void recherche(String nom) {

        cnt.removeAll();
        ArticleService as = new ArticleService();
        List<Article> le = as.rechercheArticles(nom);
        for (Article e : le) {

            cnt.add(addComponent(e));
        }

    }

    ;
    
    public Form getArticles() {
        return articles;
    }

    public Container addComponent(Article e) {
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font smallPlainSystemFont = Font.createSystemFont(Font.FACE_MONOSPACE, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        Font largePlainSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
        Container cnt1 = new Container(BoxLayout.y());
        Container cnt2 = new Container(BoxLayout.x());
        cnt1.setPreferredSize(new Dimension(900, 1000));
        cnt1.getStyle().setMargin(20, 20, 30, 30);
        cnt1.getStyle().setBorder(Border.createLineBorder(4, 0x00000000));
        EncodedImage enc = EncodedImage.createFromImage(Image.createImage(900, 600, 0xffffffff), true);
        String url = "http://localhost/PiDev/web/upload/" + e.getImage();
        URLImage urlimage = URLImage.createToStorage(enc, "Couverture" + e.getImage(), url);
        ImageViewer iv = new ImageViewer(urlimage);
        //Label prix = createForFont(smallPlainSystemFont, e.getContenu());
        SpanLabel contenu = new SpanLabel(e.getContenu());
        Label nom = createForFont(largePlainSystemFont, e.getTitre());
        nom.getStyle().setAlignment(Container.CENTER);
        //Label salle = new Label(e.getSalleId().getNom());
        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM y H:m");
        //Label dd = new Label(sdf.format(e.getDateDebut()));
        //Label df = new Label(sdf.format(e.getDateFin()));
        //dd.setIcon(FontImage.createFixed("\uf251", fnt, 0x000000, size, size));
        //df.setIcon(FontImage.createFixed("\uf253", fnt, 0x000000, size, size));
        cnt2.add(iv);

        cnt1.add(cnt2);
        cnt1.add(nom);
        cnt1.add(contenu);
        //cnt.add(prix);
        //cnt.add(salle);
        //cnt.add(dd);
        //cnt.add(df);
        Button btn = new Button();
        btn.addActionListener(eee -> {

            AfficherArticleForm aef = new AfficherArticleForm(e.getId());

            aef.getArticle().show();
        });
        cnt1.setLeadComponent(btn);
        cnt1.revalidate();
        cnt1.getAllStyles().setBorder(Border.createLineBorder(4, 0x000000));

        return cnt1;
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getStyle().setFont(fnt);
        return l;
    }
}
