/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import static com.codename1.ui.plaf.Style.BACKGROUND_NONE;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Club;
import com.mycompany.Entities.Workshop;
import com.mycompany.Service.ClubService;
import com.mycompany.Service.WorkshopService;
import java.util.ArrayList;

/**
 *
 * @author kahou
 */
public class AfficherWorkshopForm {

    private Resources theme;
    private Form workshops;
    private Container cnt2, ctn;
    private Label lb;

    public AfficherWorkshopForm() {
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        workshops = new Form("Workshops", new FlowLayout(Container.CENTER, Container.TOP));
        workshops.getToolbar().addCommandToLeftBar("Activités", FontImage.createFixed("\ue815", fnt, 0xffffff, size, size), ev -> {
            AfficherActivitesForm aaf = new AfficherActivitesForm();
            aaf.getActivites().show();
        });
        WorkshopService ws = new WorkshopService();
        ArrayList<Workshop> list = new ArrayList();

        list = (ArrayList<Workshop>) ws.getAllWorkshops();
        for (Workshop w : list) {
            workshops.add(addComponent(w));
        }

    }

    public Form getWorkshop() {

        return workshops;

    }

    public Container addComponent(Workshop w) {
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font smallPlainSystemFont = Font.createSystemFont(Font.FACE_MONOSPACE, Font.STYLE_BOLD, Font.SIZE_SMALL);
        Font largePlainSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);

        Container cnt = new Container(BoxLayout.y());

        Container cnt2 = new Container(BoxLayout.x());
        cnt.setPreferredSize(new Dimension(900, 500));
        cnt.getAllStyles().setMargin(20, 20, 30, 30);
        cnt.getAllStyles().setBackgroundType(BACKGROUND_NONE);
        cnt.getAllStyles().setBgTransparency(160);
        cnt.getAllStyles().setBgColor(0x1AA00F);
        cnt.getAllStyles().setPadding(0, 1, 0, 0);
        Label nb = createForFont(smallPlainSystemFont, w.getNbParticipant() + " participants");

        nb.getAllStyles().setBgTransparency(100);
        nb.getAllStyles().setBgColor(0x000000);
        nb.getAllStyles().setAlignment(Label.CENTER);
        nb.getAllStyles().setFgColor(0xffffff);

        Label desc = createForFont(smallPlainSystemFont, w.getDescription());
        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM y H:mm");
        Label dc = new Label(sdf.format(w.getDateDebut()));
        Label df = new Label(sdf.format(w.getDateFin()));

        dc.setIcon(FontImage.createFixed("\uf251", fnt, 0xffffff, size, size));

        dc.getAllStyles().setFgColor(0xffffff);

        df.setIcon(FontImage.createFixed("\uf252", fnt, 0xffffff, size, size));

        df.getAllStyles().setFgColor(0xffffff);
        cnt.add(nb);
        cnt.add(dc);
        cnt.add(df);

        cnt.add(desc);
        cnt2.add(cnt);

        // Yodkhol l koll club wahdou
//        Button btn = new Button();
//        btn.addActionListener(eee -> {
//            AfficherEvenementForm aef = new AfficherEvenementForm(c.getId(), 1);
//            aef.getEvenement().show();
//        });
//        cnt.setLeadComponent(btn);
//        cnt.revalidate();
//        cnt.getAllStyles().setBorder(Border.createLineBorder(4, 0x000000));
        return cnt2;

    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getAllStyles().setFont(fnt);
        return l;
    }

}
