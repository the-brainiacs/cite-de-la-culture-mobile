/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.googlemaps.MapContainer;
import com.codename1.io.Log;
import com.codename1.location.Location;
import com.codename1.location.LocationManager;
import com.codename1.maps.Coord;
import com.codename1.ui.Button;
import static com.codename1.ui.CN.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.MyApplication;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author missaoui
 */
public class IndexForm {

    private Resources theme;
    private Form index;
    private Button btn, btn2;
    private Label l;
    private Container cnt, cnt2;
    private ImageViewer iv;
    private SpanLabel sl;

    public IndexForm() {
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        index = new Form("Cité De La Culture", new FlowLayout(Container.CENTER, Container.CENTER));
        cnt = new Container(BoxLayout.y());
        cnt2 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        iv = new ImageViewer(theme.getImage("Cite-de-la-Culture.jpg"));
        iv.setPreferredH(600);
        iv.getAllStyles().setMarginBottom(100);
        iv.getAllStyles().setMarginTop(100);
        l = createForFont(largeBoldSystemFont, "Bienvenue");
        if (MyApplication.CurrentUser != null) {
            l.setText("Bienvenue " + MyApplication.CurrentUser.getUsername());
        }
        l.getStyle().setFgColor(0xB95656);
        sl = new SpanLabel("Cité de la culture est un complexe situé en plein centre de la ville de Tunis, à l'emplacement de l'ancienne Foire internationale de Tunis sur l'avenue Mohammed V.");
        sl.getTextComponent().setAlignment(CENTER);
        btn = new Button("Se connecter", FontImage.createFixed("\ue86c", fnt, 0xffffff, size, size));
        btn = setButton(btn, 0x1AA09F, 0xffffff);
        btn.addActionListener(e -> {
            LoginForm lf = new LoginForm();
            lf.getLogin().show();
        });
        btn2 = new Button("Register", FontImage.createFixed("\uf234", fnt, 0xffffff, size, size));
        btn2 = setButton(btn2, 0x9C1BDD, 0xfffffff);
        btn2.addActionListener(e -> {
            RegisterForm rf = new RegisterForm();
            rf.getRegister().show();
        });
        cnt2.add(l);
        cnt.add(cnt2);
        cnt.add(sl);
        cnt.add(iv);
        if (MyApplication.CurrentUser != null) {
            index.getToolbar().addCommandToOverflowMenu("Profile", FontImage.createFixed("\ue822", fnt, 0x000000, size, size), ev -> {
                ProfileForm pf = new ProfileForm();
                pf.getProfile().show();
            });
            index.getToolbar().addCommandToOverflowMenu("Map", FontImage.createFixed("\uf279", fnt, 0x000000, size, size), ev -> {
                MapForm pf = new MapForm();
                pf.getMapForm().show();
            });
            index.getToolbar().addCommandToOverflowMenu("Logout", FontImage.createFixed("\ueb0a", fnt, 0x000000, size, size), ev -> {
                MyApplication.CurrentUser = null;
                IndexForm in = new IndexForm();
                in.getIndex().show();
            });
            index.getToolbar().addCommandToLeftSideMenu("Articles", FontImage.createFixed("\uecee", fnt, 0xffffff, size, size), ev -> {
                AfficherArticlesForm aaf = new AfficherArticlesForm();
                aaf.getArticles().show();
            });
            index.getToolbar().addCommandToLeftSideMenu("Evènements", FontImage.createFixed("\ue86b", fnt, 0xffffff, size, size), ev -> {
                AfficherEvenementsForm aef = new AfficherEvenementsForm();
                aef.getEvenements().show();
            });
            index.getToolbar().addCommandToLeftSideMenu("Bibliothèque", FontImage.createFixed("\uec1c", fnt, 0xffffff, size, size), ev -> {
                AfficherBibliothèqueForm abf = new AfficherBibliothèqueForm();
                abf.getBibliotheque().show();
            });
            index.getToolbar().addCommandToLeftSideMenu("Activités", FontImage.createFixed("\ue81a", fnt, 0xffffff, size, size), ev -> {
                AfficherActivitesForm aaaf = new AfficherActivitesForm();
                aaaf.getActivites().show();
            });
            index.getToolbar().addCommandToLeftSideMenu("Panier", FontImage.createFixed("\uecb5", fnt, 0xffffff, size, size), ev -> {
                AfficherPanierForm apf = new AfficherPanierForm();
                apf.getPanier().show();
            });
            index.getToolbar().addCommandToLeftSideMenu("Sponsoring", FontImage.createFixed("\uebed", fnt, 0xffffff, size, size), ev -> {
                AfficherSponsoringForm asf = new AfficherSponsoringForm();
                asf.getF().show();
            });
        } else {
            cnt.add(btn);
            cnt.add(btn2);
        }
        index.add(cnt);
    }

    public Form getIndex() {
        return index;
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getAllStyles().setFont(fnt);
        return l;
    }

    private Button setButton(Button btn, int color1, int color2) {
        btn.setUIID("btn");
        btn.getStyle().setBgTransparency(200);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(20, 20, 200, 200);
        btn.getPressedStyle().setBgTransparency(155);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(20, 20, 200, 200);
        btn.getSelectedStyle().setBgTransparency(155);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(20, 20, 200, 200);
        return btn;
    }
}
