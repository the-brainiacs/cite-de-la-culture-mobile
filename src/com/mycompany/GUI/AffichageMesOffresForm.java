/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Offresponsoring;
import com.mycompany.Service.OffreService;
import java.util.List;

/**
 *
 * @author nawre
 */
public class AffichageMesOffresForm {

    private Resources theme;
    private Form mesOffres;
    private ImageViewer iv;
    private Container cnt;

    public AffichageMesOffresForm() {
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        mesOffres = new Form("Mes Offres", new FlowLayout(Container.CENTER, Container.TOP));

        mesOffres.getToolbar().addCommandToLeftSideMenu("Index", FontImage.createFixed("\uecee", fnt, 0xffffff, size, size), ev -> {
            IndexForm in = new IndexForm();
            in.getIndex().show();
        });
        mesOffres.getToolbar().addCommandToLeftSideMenu("Offres Sponsoring", FontImage.createFixed("\ue86b", fnt, 0xffffff, size, size), ev -> {
            AfficherSponsoringForm in = new AfficherSponsoringForm();
            in.getF().show();
        });
        mesOffres.getToolbar().addCommandToLeftSideMenu("Demandes Sponsoring", FontImage.createFixed("\uec1c", fnt, 0xffffff, size, size), ev -> {
            AffichageDemandeForm abf = new AffichageDemandeForm();
            abf.getF().show();
        });

        mesOffres.getToolbar().addMaterialCommandToRightBar("", FontImage.MATERIAL_ADD, (evt) -> {
            AjouterOffreForm a = new AjouterOffreForm();
            a.getAjoutoffre().show();
        });
        cnt = new Container(BoxLayout.y());

        OffreService es = new OffreService();
        List<Offresponsoring> le = es.getMyOffres();
        for (Offresponsoring e : le) {
            mesOffres.add(addComponent(e));
        }
    }

    public Form getMesOffres() {
        return mesOffres;
    }

    public Container addComponent(Offresponsoring e) {
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font smallPlainSystemFont = Font.createSystemFont(Font.FACE_MONOSPACE, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        Font largePlainSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        Container cnt = new Container(BoxLayout.y());
        Container cnt2 = new Container(BoxLayout.x());
        cnt.setPreferredSize(new Dimension(900, 1000));
        cnt.getStyle().setMargin(20, 20, 30, 30);
        cnt.getStyle().setBorder(Border.createLineBorder(4, 0x00000000));
        EncodedImage enc = EncodedImage.createFromImage(Image.createImage(650, 500, 0xffffffff), true);
        iv = new ImageViewer(theme.getImage("Cite-de-la-Culture.jpg"));
        Label type = createForFont(largePlainSystemFont, e.getType());
        type.getStyle().setAlignment(Container.CENTER);
        SpanLabel description = new SpanLabel(e.getDescription());
        Label package1 = new Label(e.getPackage1());

        cnt2.add(iv);
        cnt.add(cnt2);
        cnt.add(type);
        cnt.add(package1);
        cnt.add(description);
        //  cnt.add(description);
        return cnt;
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getStyle().setFont(fnt);
        return l;
    }

}
