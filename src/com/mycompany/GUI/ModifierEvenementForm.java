/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.ext.filechooser.FileChooser;
import com.codename1.io.FileSystemStorage;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.PickerComponent;
import com.codename1.ui.TextComponent;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.codename1.util.regex.RE;
import com.mycompany.Entities.Evenement;
import com.mycompany.Entities.Expose;
import com.mycompany.Entities.Festival;
import com.mycompany.Entities.Film;
import com.mycompany.Entities.Formation;
import com.mycompany.Entities.Musique;
import com.mycompany.Entities.Theatre;
import com.mycompany.Service.EvenementService;
import java.io.InputStream;
import java.util.Date;
import javafx.scene.paint.Color;

/**
 *
 * @author missaoui
 */
public class ModifierEvenementForm {

    private Form modifier;
    private Container cnt, cnt3, cnt5, cnt6, cnt7;
    private Label reg, i;
    private TextComponent dd, df, disponibles, type, salle, nom, desc, prix, prixEt, acteurs, realisateurs, duree, artistes, auteurs, participants, locuteurs;
    private PickerComponent anneeSortie, age, genre, genreM, genreF, typeF, themeF, themeE;
    private ImageViewer iv;
    private Button btn, imgChooser;
    private String file = "";

    public ModifierEvenementForm(int id) {
        EvenementService es = new EvenementService();
        Evenement e = es.getEvenement(id);
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM y H:mm");
        SimpleDateFormat sdf2 = new SimpleDateFormat("E, d MMM y");
        modifier = new Form("Modifier " + e.getNom(), new FlowLayout(Container.CENTER, Container.TOP));
        modifier.getToolbar().addCommandToLeftBar("Back", FontImage.createFixed("\uecc6", fnt, 0xffffff, size, size), ev -> {
            AfficherEvenementForm aef = new AfficherEvenementForm(id, 2);
            aef.getEvenement().show();
        });
        i = new Label("");
        EncodedImage enc = EncodedImage.createFromImage(Image.createImage(750, 400, 0xffffffff), false);
        String url = "http://localhost/PiDev/web/getImage.php?file=" + e.getImageId();
        URLImage urlimage = URLImage.createToStorage(enc, "Img" + e.getImageId(), url);
        iv = new ImageViewer(urlimage);
        iv.setPreferredH(500);
        reg = createForFont(largeBoldSystemFont, "Modifier Evenement");
        reg.getAllStyles().setFgColor(0xffc107);
        reg.getAllStyles().setAlignment(Label.CENTER);
        cnt = new Container(BoxLayout.y());
        cnt3 = new Container(BoxLayout.x());
        cnt3.getAllStyles().setMargin(10, 10, 20, 10);
        cnt5 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        cnt6 = new Container(BoxLayout.y());
        cnt7 = new Container(BoxLayout.y());
        cnt7.getAllStyles().setBorder(Border.createLineBorder(3, 0xffc107));
        cnt7.getAllStyles().setMargin(0, 0, 20, 20);
        nom = new TextComponent().label("Nom :");
        nom.getField().setText("" + e.getNom());
        dd = new TextComponent().label("Date Début :");
        dd.getField().setText("" + sdf.format(e.getDateDebut()));
        dd.getField().setEnabled(false);
        dd.getField().getAllStyles().setFgColor(Color.CADETBLUE.hashCode());
        df = new TextComponent().label("Date Fin :");
        df.getField().setText("" + sdf.format(e.getDateFin()));
        df.getField().setEnabled(false);
        df.getField().getAllStyles().setFgColor(Color.CADETBLUE.hashCode());
        disponibles = new TextComponent().label("Disponibles :");
        disponibles.getField().setText("" + e.getDisponibles());
        disponibles.getField().setEnabled(false);
        disponibles.getField().getAllStyles().setFgColor(Color.CADETBLUE.hashCode());
        type = new TextComponent().label("Type: ");
        type.getField().setText("" + e.getType());
        type.getField().setEnabled(false);
        type.getField().getAllStyles().setFgColor(Color.CADETBLUE.hashCode());
        salle = new TextComponent().label("Salle: ");
        salle.getField().setText("" + e.getSalleId().getNom());
        salle.getField().setEnabled(false);
        salle.getField().getAllStyles().setFgColor(Color.CADETBLUE.hashCode());
        age = PickerComponent.createStrings("tout_public", "interdit_12_ans", "pour_enfants").label("Age :");
        age.getPicker().setType(Display.PICKER_TYPE_STRINGS);
        age.getPicker().setStrings("tout_public", "interdit_12_ans", "pour_enfants");
        age.getPicker().setSelectedString(e.getAge());
        prix = new TextComponent().label("Prix :");
        prix.getField().setConstraint(TextField.NUMERIC);
        prix.getField().setText("" + e.getPrix());
        prixEt = new TextComponent().label("Prix Etudiants :");
        prixEt.getField().setConstraint(TextField.NUMERIC);
        prixEt.getField().setText("" + e.getPrixEtudiants());
        desc = new TextComponent().label("Description :");
        desc.getField().setText("" + e.getDescription());
        desc.getField().setGrowByContent(true);
        desc.getField().setGrowLimit(-1);
        imgChooser = new Button("Image", FontImage.createFixed("\ue827", fnt, 0xffffff, size, size));
        imgChooser = setButton(imgChooser, 0xB95656, 0xfffffff, 0);
        imgChooser.addActionListener(eee -> {
            if (FileChooser.isAvailable()) {
                FileChooser.showOpenDialog(".jpg, .bmp, .png, .gif, .jpeg", e2 -> {
                    if (e2 != null) {
                        file = (String) e2.getSource();
                    }
                    if (file == null) {
                        i.setText("Aucune image");
                        cnt3.revalidate();
                    } else {
                        i.setText("");
                        String extension = null;
                        if (file.lastIndexOf(".") > 0) {
                            extension = file.substring(file.lastIndexOf(".") + 1);
                        }
                        if ("jpg".equals(extension) || "gif".equals(extension) || "jpeg".equals(extension) || "png".equals(extension) || "bmp".equals(extension)) {
                            FileSystemStorage fs = FileSystemStorage.getInstance();
                            try {
                                InputStream fis = fs.openInputStream(file);
                                iv.setImage(Image.createImage(fis));
                                cnt3.revalidate();
                            } catch (Exception ex) {
                                System.out.println(ex.getMessage());
                            }
                        }
                    }
                    cnt3.revalidate();
                });
            }
        });
        if (e.getType().equals("film")) {
            Film film = es.getFilm(id);
            acteurs = new TextComponent().label("Acteurs :");
            acteurs.getField().setText("" + film.getActeurs());
            realisateurs = new TextComponent().label("Réalisateurs :");
            realisateurs.getField().setText("" + film.getRealisateurs());
            duree = new TextComponent().label("Durée :");
            duree.getField().setText("" + film.getDuree());
            anneeSortie = PickerComponent.createDate(new Date()).label("Date De Sortie :");
            anneeSortie.getPicker().setDate(film.getAnneeSortie());
            genre = PickerComponent.createStrings("action", "drame", "comedie", "romance", "historique", "western", "aventure", "thriller", "opera", "science_fiction", "horreur", "fantasie", "biographie").label("Genre :");
            genre.getPicker().setType(Display.PICKER_TYPE_STRINGS);
            genre.getPicker().setStrings("action", "drame", "comedie", "romance", "historique", "western", "aventure", "thriller", "opera", "science_fiction", "horreur", "fantasie", "biographie");
            genre.getPicker().setSelectedString(film.getGenre());
            cnt6.add(acteurs);
            cnt6.add(realisateurs);
            cnt6.add(genre);
            cnt6.add(duree);
            cnt6.add(anneeSortie);
        }
        if (e.getType().equals("theatre")) {
            Theatre theatre = es.getTheatre(id);
            artistes = new TextComponent().label("Artistes :");
            artistes.getField().setText("" + theatre.getArtistes());
            realisateurs = new TextComponent().label("Réalisateurs :");
            realisateurs.getField().setText("" + theatre.getRealisateurs());
            auteurs = new TextComponent().label("Auteurs :");
            auteurs.getField().setText("" + theatre.getAuteurs());
            anneeSortie = PickerComponent.createDate(new Date()).label("Date De Sortie :");
            anneeSortie.getPicker().setDate(theatre.getAnneeSortie());
            genre = PickerComponent.createStrings("action", "drame", "comedie", "romance", "historique", "western", "aventure", "thriller", "opera", "science_fiction", "horreur", "fantasie", "biographie").label("Genre :");
            genre.getPicker().setType(Display.PICKER_TYPE_STRINGS);
            genre.getPicker().setStrings("action", "drame", "comedie", "romance", "historique", "western", "aventure", "thriller", "opera", "science_fiction", "horreur", "fantasie", "biographie");
            genre.getPicker().setSelectedString(theatre.getGenre());
            cnt6.add(artistes);
            cnt6.add(realisateurs);
            cnt6.add(genre);
            cnt6.add(auteurs);
            cnt6.add(anneeSortie);
        }
        if (e.getType().equals("festival")) {
            Festival festival = es.getFestival(id);
            participants = new TextComponent().label("Participants :");
            participants.getField().setText("" + festival.getParticipants());
            genreF = PickerComponent.createStrings("sportif", "culturel", "humour", "voyage", "bande_dessinee", "cirque", "cinema", "danse", "musique", "photographie", "theatre", "litterature", "jeux").label("Genre :");
            genreF.getPicker().setType(Display.PICKER_TYPE_STRINGS);
            genreF.getPicker().setStrings("sportif", "culturel", "humour", "voyage", "bande_dessinee", "cirque", "cinema", "danse", "musique", "photographie", "theatre", "litterature", "jeux");
            genreF.getPicker().setSelectedString(festival.getGenreFestival());
            typeF = PickerComponent.createStrings("international", "national", "regional", "local").label("Type :");
            typeF.getPicker().setType(Display.PICKER_TYPE_STRINGS);
            typeF.getPicker().setStrings("international", "national", "regional", "local");
            typeF.getPicker().setSelectedString(festival.getTypeFestival());
            cnt6.add(participants);
            cnt6.add(genreF);
            cnt6.add(typeF);
        }
        if (e.getType().equals("formation")) {
            Formation formation = es.getFormation(id);
            locuteurs = new TextComponent().label("Locuteurs :");
            locuteurs.getField().setText("" + formation.getLocuteurs());
            themeF = PickerComponent.createStrings("informatique", "bureautique", "qualite_organisation", "comptabilite", "langues", "transport", "marketing", "social", "communication", "securite", "management").label("Theme :");
            themeF.getPicker().setType(Display.PICKER_TYPE_STRINGS);
            themeF.getPicker().setStrings("informatique", "bureautique", "qualite_organisation", "comptabilite", "langues", "transport", "marketing", "social", "communication", "securite", "management");
            themeF.getPicker().setSelectedString(formation.getTheme());
            cnt6.add(locuteurs);
            cnt6.add(themeF);
        }
        if (e.getType().equals("expose")) {
            Expose expose = es.getExpose(id);
            locuteurs = new TextComponent().label("Locuteurs :");
            locuteurs.getField().setText("" + expose.getLocuteurs());
            themeE = PickerComponent.createStrings("histoire", "geographie", "sciences", "musique", "civil", "culture", "economie").label("Theme :");
            themeE.getPicker().setType(Display.PICKER_TYPE_STRINGS);
            themeE.getPicker().setStrings("histoire", "geographie", "sciences", "musique", "civil", "culture", "economie");
            themeE.getPicker().setSelectedString(expose.getTheme());
            cnt6.add(locuteurs);
            cnt6.add(themeE);
        }
        if (e.getType().equals("musique")) {
            Musique musique = es.getMusique(id);
            artistes = new TextComponent().label("Artistes :");
            artistes.getField().setText("" + musique.getArtistes());
            genreM = PickerComponent.createStrings("blues", "disco", "funk", "pop", "country", "folk", "jazz", "rai", "rap", "rock", "soul", "reggae", "edm", "classique", "orientale").label("Genre :");
            genreM.getPicker().setType(Display.PICKER_TYPE_STRINGS);
            genreM.getPicker().setStrings("blues", "disco", "funk", "pop", "country", "folk", "jazz", "rai", "rap", "rock", "soul", "reggae", "edm", "classique", "orientale");
            genreM.getPicker().setSelectedString(musique.getGenre());
            cnt6.add(artistes);
            cnt6.add(genreM);
        }
        btn = new Button("Modifier", FontImage.createFixed("\ue859", fnt, 0xffffff, size, size));
        btn = setButton(btn, 0xffc107, 0xffffff, 30);
        btn.addActionListener(ee -> {
            boolean bool = false;
            if (e.getType().equals("film") && (verif() & verifFilm())) {
                Film film = new Film(null, Integer.parseInt(duree.getText()), genre.getPicker().getSelectedString(), anneeSortie.getPicker().getDate(), realisateurs.getText(), acteurs.getText());
                e.setNom(nom.getText());
                e.setDescription(desc.getText());
                e.setAge(age.getPicker().getSelectedString());
                e.setPrix(Double.parseDouble(prix.getText()));
                e.setPrixEtudiants(Double.parseDouble(prixEt.getText()));
                e.setImageId(file);
                es.modifierFilm(e, film);
                bool = true;
            }
            if (e.getType().equals("theatre") && (verif() & verifTheatre())) {
                Theatre theatre = new Theatre(null, genre.getPicker().getSelectedString(), anneeSortie.getPicker().getDate(), realisateurs.getText(), auteurs.getText(), artistes.getText());
                e.setNom(nom.getText());
                e.setDescription(desc.getText());
                e.setAge(age.getPicker().getSelectedString());
                e.setPrix(Double.parseDouble(prix.getText()));
                e.setPrixEtudiants(Double.parseDouble(prixEt.getText()));
                e.setImageId(file);
                es.modifierTheatre(e, theatre);
                bool = true;
            }
            if (e.getType().equals("festival") && (verif() & verifFestival())) {
                Festival festival = new Festival(null, participants.getText(), typeF.getPicker().getSelectedString(), genreF.getPicker().getSelectedString());
                e.setNom(nom.getText());
                e.setDescription(desc.getText());
                e.setAge(age.getPicker().getSelectedString());
                e.setPrix(Double.parseDouble(prix.getText()));
                e.setPrixEtudiants(Double.parseDouble(prixEt.getText()));
                e.setImageId(file);
                es.modifierFestival(e, festival);
                bool = true;
            }
            if (e.getType().equals("formation") && (verif() & verifFormation())) {
                Formation formation = new Formation(null, locuteurs.getText(), themeF.getPicker().getSelectedString());
                e.setNom(nom.getText());
                e.setDescription(desc.getText());
                e.setAge(age.getPicker().getSelectedString());
                e.setPrix(Double.parseDouble(prix.getText()));
                e.setPrixEtudiants(Double.parseDouble(prixEt.getText()));
                e.setImageId(file);
                es.modifierFormation(e, formation);
                bool = true;
            }
            if (e.getType().equals("expose") && (verif() & verifExpose())) {
                Expose expose = new Expose(null, locuteurs.getText(), themeE.getPicker().getSelectedString());
                e.setNom(nom.getText());
                e.setDescription(desc.getText());
                e.setAge(age.getPicker().getSelectedString());
                e.setPrix(Double.parseDouble(prix.getText()));
                e.setPrixEtudiants(Double.parseDouble(prixEt.getText()));
                e.setImageId(file);
                es.modifierExpose(e, expose);
                bool = true;
            }
            if (e.getType().equals("musique") && (verif() & verifMusique())) {
                Musique musique = new Musique(null, artistes.getText(), genreM.getPicker().getSelectedString());
                e.setNom(nom.getText());
                e.setDescription(desc.getText());
                e.setAge(age.getPicker().getSelectedString());
                e.setPrix(Double.parseDouble(prix.getText()));
                e.setPrixEtudiants(Double.parseDouble(prixEt.getText()));
                e.setImageId(file);
                es.modifierMusique(e, musique);
                bool = true;
            }
            if (bool) {
                Dialog.show("Modification Réussie","L'évènement "+e.getNom()+" a été modifié sans échec","OK",null);
                AfficherEvenementForm mpf = new AfficherEvenementForm(id, 2);
                mpf.getEvenement().show();
            }
        });
        cnt.add(reg);
        cnt7.add(nom);
        cnt7.add(dd);
        cnt7.add(df);
        cnt7.add(disponibles);
        cnt7.add(type);
        cnt7.add(salle);
        cnt7.add(age);
        cnt7.add(prix);
        cnt7.add(prixEt);
        cnt7.add(desc);
        cnt5.add(imgChooser);
        cnt5.add(i);
        cnt3.add(cnt5);
        cnt3.add(iv);
        cnt7.add(cnt3);
        cnt7.add(cnt6);
        cnt7.add(btn);
        cnt.add(cnt7);
        modifier.add(cnt);
        modifier.setEditOnShow(nom.getField());
    }

    public Form getModifier() {
        return modifier;
    }

    private Button setButton(Button btn, int color1, int color2, int mar) {
        btn.setUIID("btn");
        btn.getStyle().setBgTransparency(255);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(mar, mar, mar * 10, mar * 10);
        btn.getPressedStyle().setBgTransparency(175);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(mar, mar, mar * 10, mar * 10);
        btn.getSelectedStyle().setBgTransparency(175);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(mar, mar, mar * 10, mar * 10);
        return btn;
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getAllStyles().setFont(fnt);
        return l;
    }

    public boolean verif() {
        boolean bool = true;
        desc.errorMessage("");
        nom.errorMessage("");
        prix.errorMessage("");
        prixEt.errorMessage("");
        if (nom.getText().equals("")) {
            bool = false;
            nom.errorMessage("*Nom est requis");
        }
        if (desc.getText().equals("")) {
            bool = false;
            desc.errorMessage("*Description est requis");
        }
        if (prix.getText().equals("")) {
            bool = false;
            prix.errorMessage("*Prix est requis");
        }
        if (prixEt.getText().equals("")) {
            bool = false;
            prixEt.errorMessage("*Prix Etudiants est requis");
        }
        RE pattern = new RE("^[0-9]*.[0-9]*$");
        if (!pattern.match(prix.getText())) {
            bool = false;
            prix.errorMessage("*Prix doit être numérique");
        }
        if (!pattern.match(prixEt.getText())) {
            bool = false;
            prixEt.errorMessage("*Prix Etudiants doit être numérique");
        }
        if(pattern.match(prixEt.getText())&&pattern.match(prix.getText())&&Double.parseDouble(prix.getText())<Double.parseDouble(prixEt.getText()))
        {
            bool = false;
            prixEt.errorMessage("*Prix Etudiants doit être inférieur au prix");
        }
        modifier.revalidate();
        return bool;
    }

    public boolean verifFilm() {
        boolean bool = true;
        acteurs.errorMessage("");
        realisateurs.errorMessage("");
        duree.errorMessage("");
        if (acteurs.getText().equals("")) {
            bool = false;
            acteurs.errorMessage("*Acteurs est requis");
        }
        if (realisateurs.getText().equals("")) {
            bool = false;
            realisateurs.errorMessage("*Réalisateurs est requis");
        }
        if (duree.getText().equals("")) {
            bool = false;
            duree.errorMessage("*Durée est requis");
        }
        RE pattern = new RE("^[0-9]*$");
        if (!pattern.match(duree.getText())) {
            bool = false;
            duree.errorMessage("*Durée doit être numérique");
        }
        return bool;
    }

    public boolean verifTheatre() {
        boolean bool = true;
        artistes.errorMessage("");
        realisateurs.errorMessage("");
        auteurs.errorMessage("");
        if (artistes.getText().equals("")) {
            bool = false;
            artistes.errorMessage("*Artistes est requis");
        }
        if (realisateurs.getText().equals("")) {
            bool = false;
            realisateurs.errorMessage("*Réalisateurs est requis");
        }
        if (auteurs.getText().equals("")) {
            bool = false;
            auteurs.errorMessage("*Auteurs est requis");
        }
        return bool;
    }

    public boolean verifFestival() {
        boolean bool = true;
        participants.errorMessage("");
        if (participants.getText().equals("")) {
            bool = false;
            participants.errorMessage("*Participants est requis");
        }
        return bool;
    }

    public boolean verifFormation() {
        boolean bool = true;
        locuteurs.errorMessage("");
        if (locuteurs.getText().equals("")) {
            bool = false;
            locuteurs.errorMessage("*Locuteurs est requis");
        }
        return bool;
    }

    public boolean verifExpose() {
        boolean bool = true;
        locuteurs.errorMessage("");
        if (locuteurs.getText().equals("")) {
            bool = false;
            locuteurs.errorMessage("*Locuteurs est requis");
        }
        return bool;
    }

    public boolean verifMusique() {
        boolean bool = true;
        artistes.errorMessage("");
        if (artistes.getText().equals("")) {
            bool = false;
            artistes.errorMessage("*Artistes est requis");
        }
        return bool;
    }
}
