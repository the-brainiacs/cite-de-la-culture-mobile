/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.components.InteractionDialog;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Slider;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.table.DefaultTableModel;
import com.codename1.ui.table.Table;
import com.codename1.ui.table.TableModel;
import com.mycompany.Entities.Evenement;
import com.mycompany.Entities.Evenementevaluation;
import com.mycompany.Entities.Expose;
import com.mycompany.Entities.Festival;
import com.mycompany.Entities.Film;
import com.mycompany.Entities.Formation;
import com.mycompany.Entities.Musique;
import com.mycompany.Entities.Theatre;
import com.mycompany.Entities.User;
import com.mycompany.MyApplication;
import com.mycompany.Service.EvenementEvaluationService;
import com.mycompany.Service.EvenementService;
import javafx.scene.paint.Color;

/**
 *
 * @author missaoui
 */
public class AfficherEvenementForm {

    private Form evenement;
    private Container cnt, cnt2, cnt3, cnt4, cnt5;
    private ImageViewer iv;
    private Label nom, dd, df, salle, age, info, etat, disponibles, moy;
    private Table tab;
    private TableModel model;
    private TextArea desc, prix, prixEt;
    private Button modifier, supprimer, rate, ajouter;

    public AfficherEvenementForm(int id, int choix) {
        EvenementEvaluationService ees = new EvenementEvaluationService();
        EvenementService es = new EvenementService();
        Evenement e = es.getEvenement(id);
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        Font smallPlainSystemFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        Font smallBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_SMALL);
        Font largePlainSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        evenement = new Form(e.getNom(), new FlowLayout(Container.CENTER, Container.TOP));
        evenement.getToolbar().addCommandToLeftBar("Back", FontImage.createFixed("\uecc6", fnt, 0xffffff, size, size), ev -> {
            if (choix == 1) {
                AfficherEvenementsForm aef = new AfficherEvenementsForm();
                aef.getEvenements().show();
            }
            if (choix == 2) {
                AfficherMesEvenementsForm aef = new AfficherMesEvenementsForm();
                aef.getEvenements().show();
            }
            if (choix == 3) {
                AfficherEvenementsRecommandationsForm aef = new AfficherEvenementsRecommandationsForm();
                aef.getEvenements().show();
            }
        });
        cnt = new Container(BoxLayout.y());
        cnt.setUIID("cnt");
        cnt.getAllStyles().setBgTransparency(60);
        cnt.getAllStyles().setBgColor(0x1AA09F);
        cnt2 = new Container(BoxLayout.x());
        cnt3 = new Container(new FlowLayout(Container.CENTER, Container.TOP));
        cnt3.getAllStyles().setPadding(0, 5, 0, 0);
        cnt3.getAllStyles().setMargin(0, 0, 0, 0);
        cnt3.setUIID("cnt");
        cnt3.getAllStyles().setBgTransparency(120);
        cnt3.getAllStyles().setBgColor(0x1AA09F);
        cnt3.setPreferredSize(new Dimension(evenement.getWidth() * 3 / 8, evenement.getWidth() * 3 / 5));
        EncodedImage enc = EncodedImage.createFromImage(Image.createImage(evenement.getWidth() * 5 / 8, evenement.getWidth() * 3 / 5, 0xffffffff), true);
        String url = "http://localhost/PiDev/web/getImage.php?file=" + e.getImageId();
        URLImage urlimage2 = URLImage.createToStorage(enc, "evimg" + e.getImageId(), url);
        moy = new Label("Note Moyenne : " + ees.moyenneEvaluations(id));
        moy.setUIID("moy");
        moy.getAllStyles().setBgTransparency(120);
        moy.getAllStyles().setBgColor(0x1AA09F);
        moy.getAllStyles().setAlignment(Label.CENTER);
        moy.getAllStyles().setFgColor(Color.GOLDENROD.hashCode());
        moy.getAllStyles().setMargin(4, 0, 0, 0);
        iv = new ImageViewer(urlimage2);
        iv.getAllStyles().setMargin(0, 0, 0, 0);
        iv.getAllStyles().setPadding(0, 0, 0, 0);
        nom = createForFont(largePlainSystemFont, e.getType().toUpperCase() + ": " + e.getNom());
        nom.getAllStyles().setAlignment(Label.CENTER);
        nom.getAllStyles().set3DText(true, true);
        desc = new TextArea(e.getDescription());
        desc.setEditable(false);
        desc.setFocusable(false);
        desc.getAllStyles().setFont(smallPlainSystemFont);
        desc.getAllStyles().setAlignment(TextArea.CENTER);
        desc.getAllStyles().setFgColor(Color.RED.hashCode());
        rate = new Button("rate", FontImage.createFixed("\ue81f", fnt, 0xffffff, size, size));
        rate = setButton(rate, 0xB95656, 0xfffffff, 0);
        rate.addActionListener(ee -> {
            afficherEvaluation(e);
        });
        disponibles = new Label(e.getDisponibles() + " Places Disponibles");
        salle = new Label(e.getSalleId().getNom());
        salle.getAllStyles().setFgColor(Color.INDIGO.hashCode());
        age = new Label("Age: " + e.getAge().replace('_', ' ').toUpperCase());
        age.getAllStyles().setFgColor(Color.ORANGE.hashCode());
        prix = new TextArea("Prix " + e.getPrix() + "DT");
        prix.getAllStyles().setFont(smallBoldSystemFont);
        prix.getAllStyles().setAlignment(TextArea.CENTER);
        prix.getAllStyles().setFgColor(0xffffff);
        prix.setEditable(false);
        prix.setFocusable(false);
        prixEt = new TextArea("Prix Etudiants " + e.getPrixEtudiants() + "DT");
        prixEt.getAllStyles().setFont(smallPlainSystemFont);
        prixEt.getAllStyles().setAlignment(TextArea.CENTER);
        prixEt.getAllStyles().setFgColor(0xffffff);
        prixEt.setEditable(false);
        prixEt.setFocusable(false);
        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM y H:mm");
        SimpleDateFormat sdf2 = new SimpleDateFormat("E, d MMM y");
        dd = new Label("Début: " + sdf.format(e.getDateDebut()));
        df = new Label("Fin: " + sdf.format(e.getDateFin()));
        dd.setIcon(FontImage.createFixed("\uf251", fnt, 0x000000, size, size));
        df.setIcon(FontImage.createFixed("\uf253", fnt, 0x000000, size, size));
        dd.getAllStyles().setFgColor(0x000000);
        df.getAllStyles().setFgColor(0x000000);
        cnt4 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        cnt4.getAllStyles().setPadding(20, 20, 20, 20);
        modifier = new Button("Modifier", FontImage.createFixed("\ue859", fnt, 0xffffff, size, size));
        modifier = setButton(modifier, 0xffc107, 0xffffff, 1);
        modifier.addActionListener(ee -> {
            ModifierEvenementForm mef = new ModifierEvenementForm(id);
            mef.getModifier().show();
        });
        supprimer = new Button("Supprimer", FontImage.createFixed("\uf272", fnt, 0xffffff, size, size));
        supprimer = setButton(supprimer, 0xff0000, 0xffffff, 1);
        supprimer.addActionListener(ee -> {
            boolean bool = Dialog.show("Suppression", " Vous êtes sur de supprimer l'évènement  " + e.getNom() + " ?", "OK", "Annuler");
            if (bool) {
                Dialog.show("Suppression Réussie", "L'évènement  " + e.getNom() + " a été supprimé sans échec", "OK", null);
                es.supprimerEvenement(e);
                AfficherMesEvenementsForm aef = new AfficherMesEvenementsForm();
                aef.getEvenements().show();
            }
        });
        ajouter = new Button("Ajouter au panier", FontImage.createFixed("\ue86c", fnt, 0xffffff, size, size));
        ajouter = setButton(ajouter, 0x1AA09F, 0xffffff, 1);
        ajouter.addActionListener(ee -> {
            
            User currentUser = MyApplication.CurrentUser;
             boolean bool = Dialog.show("Ajout au panier", " Vous êtes sur de vouloir ajouter au panier ?", "OUI", "Annuler");
            if (bool) {
                Dialog.show("Ajout Réussie", "L'évènement a été ajouté à votre panier", "OK", null);
              es.ajouterReservation(e, currentUser.getId());
                AfficherPanierForm apf = new AfficherPanierForm();
                apf.getPanier().show();
            }


        });
        if (e.getUserId().getId() == MyApplication.CurrentUser.getId()) {
            etat = new Label("Etat : " + e.getEtat().replace('_', ' '));
            etat.getAllStyles().setAlignment(Label.CENTER);
            cnt.add(etat);
        }
        cnt.add(moy);
        cnt2.add(iv);
        cnt3.add(prix);
        cnt3.add(prixEt);
        cnt2.add(cnt3);
        cnt.add(cnt2);
        cnt.add(nom);
        if (e.getUserId().getId() != MyApplication.CurrentUser.getId()) {
            cnt.add(rate);
        }
        cnt.add(disponibles);
        cnt.add(salle);
        cnt.add(age);
        cnt.add(dd);
        cnt.add(df);
        cnt.add(desc);
        cnt5 = new Container(BoxLayout.y());
        info = createForFont(largePlainSystemFont, "Information Additionnal");
        cnt5.add(info);
        if (e.getType().equals("film")) {
            Film f = es.getFilm(id);
            model = new DefaultTableModel(new String[]{"", ""}, new Object[][]{
                {"Acteurs:", f.getActeurs()},
                {"Réalisateurs:", f.getRealisateurs()},
                {"Genre:", f.getGenre()},
                {"Durée:", f.getDuree()},
                {"Date de Sortie:", sdf2.format(f.getAnneeSortie())},}) {
                public boolean isCellEditable(int row, int col) {
                    return false;
                }
            };
        }
        if (e.getType().equals("theatre")) {
            Theatre t = es.getTheatre(id);
            model = new DefaultTableModel(new String[]{"", ""}, new Object[][]{
                {"Artistes:", t.getArtistes()},
                {"Réalisateurs:", t.getRealisateurs()},
                {"Genre:", t.getGenre()},
                {"Auteurs:", t.getAuteurs()},
                {"Date de Sortie:", sdf2.format(t.getAnneeSortie())},}) {
                public boolean isCellEditable(int row, int col) {
                    return false;
                }
            };

        }
        if (e.getType().equals("festival")) {
            Festival f = es.getFestival(id);
            model = new DefaultTableModel(new String[]{"", ""}, new Object[][]{
                {"Participants:", f.getParticipants()},
                {"Genre:", f.getGenreFestival()},
                {"Type:", f.getTypeFestival()},}) {
                public boolean isCellEditable(int row, int col) {
                    return false;
                }
            };
        }
        if (e.getType().equals("formation")) {
            Formation f = es.getFormation(id);
            model = new DefaultTableModel(new String[]{"", ""}, new Object[][]{
                {"Locuteurs:", f.getLocuteurs()},
                {"Thème:", f.getTheme()},}) {
                public boolean isCellEditable(int row, int col) {
                    return false;
                }
            };
        }
        if (e.getType().equals("expose")) {
            Expose ee = es.getExpose(id);
            model = new DefaultTableModel(new String[]{"", ""}, new Object[][]{
                {"Locuteurs:", ee.getLocuteurs()},
                {"Thème:", ee.getTheme()},}) {
                public boolean isCellEditable(int row, int col) {
                    return false;
                }
            };
        }
        if (e.getType().equals("musique")) {
            Musique m = es.getMusique(id);
            model = new DefaultTableModel(new String[]{"", ""}, new Object[][]{
                {"Artistes:", m.getArtistes()},
                {"Genre:", m.getGenre()},}) {
                public boolean isCellEditable(int row, int col) {
                    return false;
                }
            };
        }
        tab = new Table(model) {
            @Override
            protected Component createCell(Object value, int row, int column, boolean editable) { // (1)
                Component cell;
                cell = super.createCell(value, row, column, editable);
                if (column == 0) {
                    cell.getAllStyles().setFgColor(Color.INDIGO.hashCode());
                }
                cell.getAllStyles().setBgTransparency(0);
                return cell;
            }
        };
        tab.setDrawBorder(false);
        cnt5.add(tab);
        cnt.add(cnt5);
        cnt4.add(ajouter);
        if (e.getUserId().getId() == MyApplication.CurrentUser.getId()) {
            cnt4.add(modifier);
            if (!e.getEtat().equals("valide")) {
                cnt4.add(supprimer);
            }
        }
        cnt.add(cnt4);
        evenement.add(cnt);
    }

    public Form getEvenement() {
        return evenement;
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getAllStyles().setFont(fnt);
        return l;
    }

    private Button setButton(Button btn, int color1, int color2, int mar) {
        btn.setUIID("btnn");
        btn.getStyle().setBgTransparency(220);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(mar, mar, mar * 10, mar * 10);
        btn.getPressedStyle().setBgTransparency(175);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(mar, mar, mar * 10, mar * 10);
        btn.getSelectedStyle().setBgTransparency(175);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(mar, mar, mar * 10, mar * 10);
        return btn;
    }

    void afficherEvaluation(Evenement e) {
        InteractionDialog id = new InteractionDialog("SVP Evaluez " + e.getNom());
        Form f = Display.getInstance().getCurrent();
        id.setLayout(new BorderLayout());
        Slider rate = createStarRankSlider();
        EvenementEvaluationService ees = new EvenementEvaluationService();
        Evenementevaluation ee = ees.getEvaluation(e.getId(), MyApplication.CurrentUser.getId());
        if (ee != null) {
            rate.setProgress(ee.getNote());
        }
        Button ok = new Button("OK");
        ok = setButton(ok, 0xB95656, 0xfffffff, 9);
        Button no = new Button("Non");
        no = setButton(no, 0xB95656, 0xfffffff, 9);
        id.add(BorderLayout.CENTER, FlowLayout.encloseCenterMiddle(rate)).
                add(BorderLayout.SOUTH, GridLayout.encloseIn(2, no, ok));
        int height = id.getPreferredH();
        id.show(f.getHeight() - height - f.getTitleArea().getHeight(), 0, 0, 0);
        no.addActionListener(ex -> id.dispose());
        ok.addActionListener(ex -> {
            id.dispose();
            if (ee != null) {
                ees.modifierEvaluation(e.getId(), MyApplication.CurrentUser.getId(), rate.getProgress());
                moy.setText("Note Moyenne : " + ees.moyenneEvaluations(e.getId()));
            } else {
                ees.ajouterEvaluation(e.getId(), MyApplication.CurrentUser.getId(), rate.getProgress());
                moy.setText("Note Moyenne : " + ees.moyenneEvaluations(e.getId()));
            }
        });
    }

    private void initStarRankStyle(Style s, Image star) {
        s.setBackgroundType(Style.BACKGROUND_IMAGE_TILE_BOTH);
        s.setBorder(Border.createEmpty());
        s.setBgImage(star);
        s.setBgTransparency(0);
    }

    private Slider createStarRankSlider() {
        Slider starRank = new Slider();
        starRank.setEditable(true);
        starRank.setMinValue(0);
        starRank.setMaxValue(5);
        Font fnt = Font.createTrueTypeFont("native:MainLight", "native:MainLight").
                derive(Display.getInstance().convertToPixels(5, true), Font.STYLE_PLAIN);
        Style s = new Style(0xffff33, 0, fnt, (byte) 0);
        Image fullStar = FontImage.createMaterial(FontImage.MATERIAL_STAR, s).toImage();
        s.setOpacity(100);
        s.setFgColor(0);
        Image emptyStar = FontImage.createMaterial(FontImage.MATERIAL_STAR, s).toImage();
        initStarRankStyle(starRank.getSliderEmptySelectedStyle(), emptyStar);
        initStarRankStyle(starRank.getSliderEmptyUnselectedStyle(), emptyStar);
        initStarRankStyle(starRank.getSliderFullSelectedStyle(), fullStar);
        initStarRankStyle(starRank.getSliderFullUnselectedStyle(), fullStar);
        starRank.setPreferredSize(new Dimension(fullStar.getWidth() * 5, fullStar.getHeight()));
        return starRank;
    }
}
