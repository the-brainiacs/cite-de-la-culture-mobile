/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ext.filechooser.FileChooser;
import com.codename1.io.File;
import com.codename1.io.FileSystemStorage;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextComponent;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.util.regex.RE;
import com.mycompany.Entities.User;
import com.mycompany.MyApplication;
import com.mycompany.Service.UserService;
import java.io.InputStream;

/**
 *
 * @author missaoui
 */
public class ModifierProfileForm {

    private Form modifierProfile;
    private Resources theme;
    private Container cnt, cnt2, cnt3, cnt4, cnt5, cnt6, cnt7;
    private Label reg, i, ic;
    private TextComponent username, bio, nom, prenom, email;
    private ImageViewer iv, ivc;
    private Button btn, imgChooser, imgCouvChooser;
    private String file="", file2="";

    public ModifierProfileForm() {
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        FontImage fm = FontImage.createFixed("\uecc6", fnt, 0xffffff, size, size);
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        modifierProfile = new Form("Modifier Profile", new FlowLayout(Container.CENTER, Container.CENTER));
        modifierProfile.getToolbar().addCommandToLeftBar("Back", fm, e -> {
            ProfileForm ind = new ProfileForm();
            ind.getProfile().show();
        });
        cnt = new Container(BoxLayout.y());
        cnt2 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        cnt3 = new Container(BoxLayout.x());
        cnt3.getAllStyles().setMargin(10,10,20,10);
        cnt4 = new Container(BoxLayout.x());
        cnt5 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        cnt6 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        cnt6.getAllStyles().setMargin(10,10,20,10);
        cnt7 = new Container(BoxLayout.y());
        cnt7.getAllStyles().setBorder(Border.createLineBorder(3, 0xffc107));
        cnt7.getAllStyles().setMargin(0, 0,20,20);
        if (MyApplication.CurrentUser.getImg() == null) {
            i = new Label("Ajouter une image");
            iv = new ImageViewer();
            iv.setPreferredH(500);
        } else {
            i = new Label("");
            EncodedImage enc = EncodedImage.createFromImage(Image.createImage(350, 400, 0xffffffff), false);
            String url = "http://localhost/PiDev/web/getImage.php?file=" + MyApplication.CurrentUser.getImg();
            URLImage urlimage = URLImage.createToStorage(enc, "profile" + MyApplication.CurrentUser.getImg(), url);
            iv = new ImageViewer(urlimage);
            iv.setPreferredH(500);
        }
        i.getStyle().setFgColor(0xB95656);
        if (MyApplication.CurrentUser.getImgCouv() == null) {
            ic = new Label("Ajouter une image");
            ivc = new ImageViewer();
            ivc.setPreferredH(500);
        } else {
            ic = new Label("");
            EncodedImage enc2 = EncodedImage.createFromImage(Image.createImage(750, 400, 0xffffffff), false);
            String url2 = "http://localhost/PiDev/web/getImage.php?file=" + MyApplication.CurrentUser.getImgCouv();
            URLImage urlimage2 = URLImage.createToStorage(enc2, "Couverture" + MyApplication.CurrentUser.getImgCouv(), url2);
            ivc = new ImageViewer(urlimage2);
            ivc.setPreferredH(500);
        }
        ic.getAllStyles().setFgColor(0xB95656);
        reg = createForFont(largeBoldSystemFont, "Modifier Profile");
        reg.getAllStyles().setFgColor(0xffc107);
        username = new TextComponent().label("Username :");
        username.getField().setText(MyApplication.CurrentUser.getUsername());
        nom = new TextComponent().label("Nom :");
        nom.getField().setText(MyApplication.CurrentUser.getNom());
        prenom = new TextComponent().label("Prénom :");
        prenom.getField().setText(MyApplication.CurrentUser.getPrenom());
        bio = new TextComponent().label("Bio :");
        bio.getField().setText(MyApplication.CurrentUser.getBio());
        email = new TextComponent().label("Email :");
        email.getField().setText(MyApplication.CurrentUser.getEmail());
        email.getField().setConstraint(TextField.EMAILADDR);
        imgChooser = new Button("Profile", FontImage.createFixed("\ue827", fnt, 0xffffff, size, size));
        imgChooser = setButton(imgChooser, 0xB95656, 0xfffffff, 0);
        imgChooser.addActionListener(e -> {
            if (FileChooser.isAvailable()) {
                FileChooser.showOpenDialog(".jpg, .bmp, .png, .gif, .jpeg", e2 -> {
                    if (e2 != null) {
                        file = (String) e2.getSource();
                    }
                    if (file == null) {
                        i.setText("Aucune image");
                        cnt3.revalidate();
                    } else {
                        i.setText("");
                        String extension = null;
                        if (file.lastIndexOf(".") > 0) {
                            extension = file.substring(file.lastIndexOf(".") + 1);
                        }
                        if ("jpg".equals(extension) || "gif".equals(extension) || "jpeg".equals(extension) || "png".equals(extension) || "bmp".equals(extension)) {
                            FileSystemStorage fs = FileSystemStorage.getInstance();
                            try {
                                InputStream fis = fs.openInputStream(file);
                                iv.setImage(Image.createImage(fis));
                                cnt3.revalidate();
                            } catch (Exception ex) {
                                System.out.println(ex.getMessage());
                            }
                        }
                    }
                    cnt3.revalidate();
                });
            }
        });
        imgCouvChooser = new Button("Couverture", FontImage.createFixed("\ue826", fnt, 0xffffff, size, size));
        imgCouvChooser = setButton(imgCouvChooser, 0xB95656, 0xfffffff, 0);
        imgCouvChooser.addActionListener(e -> {
            if (FileChooser.isAvailable()) {
                FileChooser.showOpenDialog(".jpg, .bmp, .png, .gif, .jpeg", e2 -> {
                    if (e2 != null) {
                        file2 = (String) e2.getSource();
                    }
                    if (file2 == null) {
                        ic.setText("Aucune image");
                        cnt4.revalidate();
                    } else {
                        ic.setText("");
                        String extension2 = null;
                        if (file2.lastIndexOf(".") > 0) {
                            extension2 = file2.substring(file2.lastIndexOf(".") + 1);
                        }
                        if ("jpg".equals(extension2) || "gif".equals(extension2) || "jpeg".equals(extension2) || "png".equals(extension2) || "bmp".equals(extension2)) {
                            FileSystemStorage fs = FileSystemStorage.getInstance();
                            try {
                                InputStream fis = fs.openInputStream(file2);
                                ivc.setImage(Image.createImage(fis));
                                cnt4.revalidate();
                            } catch (Exception ex) {
                                System.out.println(ex.getMessage());
                            }
                        }
                    }
                    cnt4.revalidate();
                });
            }
        });
        btn = new Button("Modifier", FontImage.createFixed("\ue859", fnt, 0xffffff, size, size));
        btn = setButton(btn, 0xffc107, 0xfffffff, 30);
        btn.addActionListener(e -> {
            if(verif()){
            UserService us = new UserService();
            User u = us.modifierProfile(MyApplication.CurrentUser.getId(), username.getText(), email.getText(), nom.getText(), prenom.getText(), bio.getText(), file, file2);
            if (u != null) {
                Dialog.show("Modification Réussie","Votre profile a été modifié sans échec","OK",null);
                MyApplication.CurrentUser = u;
                ProfileForm ind = new ProfileForm();
                ind.getProfile().show();
            } else {
                Dialog.show("Modification Echec", "Username "+username.getText()+" déjà existe", "OK", null);
            }
            }
        });
        cnt.add(cnt2);
        cnt2.add(reg);
        cnt7.add(username);
        cnt7.add(email);
        cnt7.add(nom);
        cnt7.add(prenom);
        cnt7.add(bio);
        cnt5.add(imgChooser);
        cnt5.add(i);
        cnt3.add(cnt5);
        cnt3.add(iv);
        cnt6.add(imgCouvChooser);
        cnt6.add(ic);
        cnt4.add(cnt6);
        cnt4.add(ivc);
        cnt7.add(cnt3);
        cnt7.add(cnt4);
        cnt7.add(btn);
        cnt.add(cnt7);
        modifierProfile.add(cnt);
        modifierProfile.setEditOnShow(username.getField());
    }

    public Form getModifierProfile() {
        return modifierProfile;
    }

    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getAllStyles().setFont(fnt);
        return l;
    }

    private Button setButton(Button btn, int color1, int color2, int mar) {
        btn.setUIID("btn");
        btn.getStyle().setBgTransparency(255);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(mar, mar, mar * 10, mar * 10);
        btn.getPressedStyle().setBgTransparency(175);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(mar, mar, mar * 10, mar * 10);
        btn.getSelectedStyle().setBgTransparency(175);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(mar, mar, mar * 10, mar * 10);
        return btn;
    }
    
    public boolean verif() {
        boolean bool = true;
        username.errorMessage("");
        nom.errorMessage("");
        prenom.errorMessage("");
        bio.errorMessage("");
        email.errorMessage("");
        if (username.getText().equals("")) {
            bool = false;
            username.errorMessage("*Username est requis");
        }
        if (username.getText().contains(" ")) {
            bool = false;
            username.errorMessage("*Username ne doit pas avoir des espaces");
        }
        if (nom.getText().equals("")) {
            bool = false;
            nom.errorMessage("*Nom est requis");
        }
        if (prenom.getText().equals("")) {
            bool = false;
            prenom.errorMessage("*Prenom est requis");
        }
        if (bio.getText().equals("")) {
            bool = false;
            bio.errorMessage("*Bio est requis");
        }
        if (email.getText().equals("")) {
            bool = false;
            email.errorMessage("*Email est requis");
        }
        RE pattern = new RE("^[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-z)]+\\.[(a-zA-z)]{2,3}$");
        if (!pattern.match(email.getText())) {
            bool = false;
            email.errorMessage("*Email doit etre sous la forme abc@cde.fg");
        }
        modifierProfile.revalidate();
        return bool;
    }
}
