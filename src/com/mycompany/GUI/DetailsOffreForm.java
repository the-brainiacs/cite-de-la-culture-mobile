/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.mycompany.Entities.Offresponsoring;
import com.mycompany.MyApplication;
import static com.mycompany.MyApplication.theme;
import com.mycompany.Service.OffreService;

/**
 *
 * @author nawre
 */
public class DetailsOffreForm {

    private Button btn, btn2;
    Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
    int size = Display.getInstance().convertToPixels(4);
    Form hi = new Form("Consulter Details", new BoxLayout(BoxLayout.Y_AXIS));

    public DetailsOffreForm(Offresponsoring act) {

        hi.setScrollableY(true);

        Container pubInfo = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container con = new Container(new BoxLayout(BoxLayout.X_AXIS));
        pubInfo.setScrollableY(true);
        hi.getToolbar().addCommandToLeftBar("Retour", MyApplication.theme.getImage("back-command.png"), (event) -> {
            AfficherSponsoringForm mp = new AfficherSponsoringForm();
            mp.getF().show();
        });
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);

        Label l = new Label();
        l = createForFont1(largeBoldSystemFont, "Salut M.");
        if (MyApplication.CurrentUser != null) {
            l.setText("Salut M. " + MyApplication.CurrentUser.getUsername());
        }
        l.getStyle().setFgColor(0xB95656);
        //  l.getStyle().setAlignment(1000);

        SpanLabel contenu = new SpanLabel();
        contenu.setText("Description : " + act.getDescription());

        Label titre = new Label("Type : " + act.getType());
        Label pack = new Label("Package : " + act.getPackage1());

        btn = new Button("Modifier", FontImage.createFixed("", fnt, 0xffffff, size, size));
        btn = setButton(btn, 0x1AA09F, 0xffffff);
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ModifierOffreForm mp = new ModifierOffreForm(act);
                mp.getF().show();
            }

        });
        btn2 = new Button("Supprimer", FontImage.createFixed("", fnt, 0xffffff, size, size));
        btn2 = setButton(btn2, 0xFF3502, 0xfffffff);
        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                boolean bool = Dialog.show("Supprimer", "Voulez-vous vraiment supprimer l'offre ? ", "Ok", "Cancel");
                if (bool) {
                    OffreService os = new OffreService();
                    System.out.println(act.getId());
                    os.SupprimerOffre(act.getId());
                    AfficherSponsoringForm mp = new AfficherSponsoringForm();
                    mp.getF().show();
                    Dialog.show("Suppression", "Suppression affectuée avec succès", "Ok", null);

                } else {
                    DetailsOffreForm mp = new DetailsOffreForm(act);
                    mp.getF().show();
                }
            }

        });

        ImageViewer image = new ImageViewer();
        EncodedImage placeholder = EncodedImage.createFromImage(
                theme.getImage("Cite-de-la-Culture.jpg"), true);
        URLImage uRLImage = URLImage.createToStorage(placeholder,
                "imageFromServer",
                "http://localhost/images/logo.jpg");

        image.setImage(uRLImage);
        // contenu.add(BorderLayout.NORTH, image.scaled(Display.getInstance().getDisplayWidth(), 800));
        //b.setSize(new Dimension(Display.getInstance().getDisplayWidth(), 200));
        //hi.add(BorderLayout.SOUTH,b);
        pubInfo.add(l);
        pubInfo.add(image);
        pubInfo.add(titre);
        pubInfo.add(pack);
        pubInfo.add(contenu);

        hi.add(pubInfo);
        //System.out.println(act.getIduser().getId());
        hi.add(btn);
        hi.add(btn2);

        hi.show();

    }

    public Form getF() {
        return hi;
    }

    public void setF(Form f) {
        this.hi = f;
    }

    private Label createForFont1(Font fnt, String s) {
        Label l = new Label(s);
        l.getUnselectedStyle().setFont(fnt);
        return l;
    }

    private Button setButton(Button btn, int color1, int color2) {
        btn.setUIID("btn");
        btn.getStyle().setBgTransparency(200);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(20, 20, 200, 200);
        btn.getPressedStyle().setBgTransparency(155);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(20, 20, 200, 200);
        btn.getSelectedStyle().setBgTransparency(155);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(20, 20, 200, 200);
        return btn;
    }

}
