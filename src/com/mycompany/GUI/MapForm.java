/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.googlemaps.MapContainer;
import com.codename1.location.Location;
import com.codename1.location.LocationManager;
import com.codename1.maps.Coord;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.sun.javafx.webkit.WebConsoleListener;
import java.io.IOException;

/**
 *
 * @author missaoui
 */
public class MapForm {

    private Form mapform;
    private MapContainer map;
    private Resources theme;
    private String JS_API_KEY;

    public MapForm() {
        WebConsoleListener.setDefaultListener((webView, message, lineNumber, sourceId) -> {
            System.out.println(message + "[at " + lineNumber + "]");
        });
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        mapform = new Form("Map", new FlowLayout(Container.CENTER, Container.TOP));
        mapform.getToolbar().addCommandToLeftBar("Index", FontImage.createFixed("\ue815", fnt, 0xffffff, size, size), ev -> {
            IndexForm in = new IndexForm();
            in.getIndex().show();
        });
        JS_API_KEY = "AIzaSyDIGofwaHO5FTBqNAkS36dY3LTpo4NX3hg";
        map = new MapContainer(JS_API_KEY);
        map.setMapType(MapContainer.MAP_TYPE_TERRAIN);
        map.zoom(new Coord(36.810619, 10.186240), 14);
        map.setCameraPosition(new Coord(36.810619, 10.186240));
        Location location = LocationManager.getLocationManager().getCurrentLocationSync(30000);
        if (location == null) {
            try {
                location = LocationManager.getLocationManager().getCurrentLocation();
            } catch (IOException err) {
                Dialog.show("Location Error", "Unable to find your current location, please be sure that your GPS is turned on", "OK", null);
                return;
            }
        }
        map.addPath(
                new Coord(location.getLatitude(), location.getLongitude()),
                map.getCameraPosition()
        );
        Style s = new Style();
        s.setFgColor(0xff0000);
        s.setBgTransparency(0);
        Style s2 = new Style();
        s2.setFgColor(0x0000ff);
        s2.setBgTransparency(0);
        FontImage markerImg = FontImage.createMaterial(FontImage.MATERIAL_PLACE, s, Display.getInstance().convertToPixels(1));
        FontImage markerImg2 = FontImage.createMaterial(FontImage.MATERIAL_PLACE, s2, Display.getInstance().convertToPixels(1));
        map.addMarker(
                EncodedImage.createFromImage(markerImg, false),
                new Coord(36.810619, 10.186240),
                "Cité De La culture",
                "",
                e3 -> {
                }
        );
        map.addMarker(
                EncodedImage.createFromImage(markerImg2, false),
                new Coord(location.getLatitude(), location.getLongitude()),
                "Votre position",
                "",
                e3 -> {
                }
        );
        Container root = LayeredLayout.encloseIn(
                BorderLayout.center(map)
        );
        mapform.add(root);
    }

    public Form getMapForm() {
        return mapform;
    }

}
