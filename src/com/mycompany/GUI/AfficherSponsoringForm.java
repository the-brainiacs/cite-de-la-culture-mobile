/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextComponent;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Offresponsoring;
import static com.mycompany.MyApplication.theme;
import com.mycompany.Service.OffreService;
import java.util.List;

/**
 *
 * @author nawre
 */
public class AfficherSponsoringForm {


    Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
    int size = Display.getInstance().convertToPixels(4);
    Form hi = new Form("Offre Sponsoring", new BoxLayout(BoxLayout.Y_AXIS));
    Container cnt= new Container(BoxLayout.y());
    TextComponent rech = new TextComponent().hint("rechercher");
    List<Offresponsoring> s;

    public AfficherSponsoringForm() {

      
        OffreService dt = new OffreService();
        s = dt.getAllOffreValide(null);

        for (int i = 0; i < s.size(); i++) {
            
            create(s.get(i));
        }
        
        hi.refreshTheme();
        hi.add(rech);
        hi.add(cnt);
        
        rech.getField().addDataChangedListener((type, index) -> {
            recherher(rech.getField().getText());
        });
        // MenuManager.createMenu(hi, theme);
        

        hi.getToolbar().addCommandToLeftSideMenu("Index", FontImage.createFixed("\uecee", fnt, 0xffffff, size, size), ev -> {
            IndexForm in = new IndexForm();
            in.getIndex().show();
        });
        hi.getToolbar().addCommandToLeftSideMenu("Mes Offres", FontImage.createFixed("\ue86b", fnt, 0xffffff, size, size), ev -> {
            AffichageMesOffresForm in = new AffichageMesOffresForm();
            in.getMesOffres().show();
        });
        hi.getToolbar().addCommandToLeftSideMenu("Demande Sponsoring", FontImage.createFixed("\uec1c", fnt, 0xffffff, size, size), ev -> {
            AffichageDemandeForm abf = new AffichageDemandeForm();
            abf.getF().show();
        });
        hi.getToolbar().addCommandToSideMenu("SMS", FontImage.createFixed("", fnt, 0xffffff ,size,size), new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                SmsSponsorForm showf = new SmsSponsorForm(theme);
            }
        });
        hi.getToolbar().addMaterialCommandToRightBar("", FontImage.MATERIAL_ADD, (evt) -> {
            AjouterOffreForm a = new AjouterOffreForm();
            a.getAjoutoffre().show();
        });

        hi.show();

    }
    
    public void recherher(String nom){
        OffreService dt = new OffreService();
        s = dt.getAllOffreValide(nom);

        for (int i = 0; i < s.size(); i++) {
            
            create(s.get(i));
        }
        
        hi.refreshTheme();
    }
    
    public void create(Offresponsoring act){
        
       

            Container contact = new Container(new BorderLayout());
         
            Label titre = new Label(act.getType());
           // contact.add(con2);
            contact.add(BorderLayout.NORTH, titre);
            ImageViewer image = new ImageViewer();
            EncodedImage placeholder = EncodedImage.createFromImage(
                    theme.getImage("Cite-de-la-Culture.jpg"), true);
            URLImage uRLImage = URLImage.createToStorage(placeholder,
                    "imageFromServer",
                    "http://localhost/images/logo.jpg");

            image.setImage(uRLImage);
            SpanLabel contenu = new SpanLabel();
            contenu.setText(act.getPackage1());
            Button btnOk = new Button("Consulter ...");

            btnOk.addPointerPressedListener((evt) -> {
                DetailsOffreForm a = new DetailsOffreForm(act);
                a.getF().show();

            });
            //titre.setFocusable(true);

            //  contenu.setTextBlockAlign(Component.BOTTOM);
            contact.add(BorderLayout.SOUTH, contenu);

            //   pubContainer.add(BorderLayout.CENTER, BorderLayout.east(new Label(act.getCategories().getName().toString())));
            //pubContainer.setLeadComponent(titre);
           
            cnt.add(image);
            cnt.add(contact);
            cnt.add(btnOk);
        
    }

    public Form getF() {
        return hi;
    }

    public void setF(Form f) {
        this.hi = f;
    }

}
