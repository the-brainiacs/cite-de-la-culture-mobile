/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Evenement;
import com.mycompany.Entities.Offresponsoring;
import com.mycompany.MyApplication;
import com.mycompany.Service.OffreService;


public class ModifierOffreForm {
    private Resources theme;
    private Form modifier;
    private Container cntl, cntl2, cntl3;
    private Label ltype, ldesc, lpack,l,espace;
    private TextField desc , type;
    private Button add;
    private ComboBox cb;
    public ModifierOffreForm(Offresponsoring o)
    {
         theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        FontImage fm = FontImage.createFixed("\uecc6", fnt, 0xffffff, size, size);
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        modifier = new Form("Modifer Offre Sponsoring", new FlowLayout(Container.CENTER, Container.CENTER));
        modifier.getToolbar().addMaterialCommandToLeftBar("", FontImage.MATERIAL_ARROW_BACK, (evt) -> {
            AfficherSponsoringForm a= new AfficherSponsoringForm();
            a.getF().showBack();
        });
         l = createForFont1(largeBoldSystemFont, "Salut M.");
        if(MyApplication.CurrentUser != null)
            l.setText("Salut M. "+MyApplication.CurrentUser.getUsername());
        l.getStyle().setFgColor(0xB95656);

        cb = new ComboBox();
        cb.addItem("Premium");
        cb.addItem("Gold");
        cb.addItem("Silver");
        cb.setSelectedItem(o.getPackage1());
        type = new TextField(o.getType());
        desc = new TextField(o.getDescription());
        
        cntl = new Container(BoxLayout.y());
        
        
        add = new Button("Modifier",FontImage.createFixed("\ue86c", fnt, 0xffffff, size, size));
        add = setButton(add, 0x1AA09F, 0xffffff);
        
        add.addActionListener((evt) -> {
              boolean bool = Dialog.show("Modifier", "Voulez vous vraiment modifier Offre :" + o.getType(), "OUI", "NON");
            if (bool) {
                OffreService os = new  OffreService();
                o.setDescription(desc.getText());
                o.setType(type.getText());
                o.setIduser(MyApplication.CurrentUser);
                o.setPackage1(cb.getSelectedItem().toString());
                o.setIdEvent(new Evenement(1));
                os.ModifierOffre(o.getId(),o);
                AfficherSponsoringForm ao = new AfficherSponsoringForm();
                ao.getF().showBack();
            }
            else {
                ModifierOffreForm mo = new ModifierOffreForm(o);
                mo.getF().show();
            }
        });
        cntl.add(l);
        cntl.add(type);
        cntl.add(desc);
//      cntl.add(espace);
        cntl.add(cb);

        cntl.add(add);
        
        modifier.add(cntl);
        


    }
    
    public Form getF()
    {
        return modifier;
    }
      private Label createForFont1(Font fnt, String s) {
   Label l = new Label(s);
        l.getUnselectedStyle().setFont(fnt);
        return l;
    }

      
      private Button setButton(Button btn, int color1, int color2) {
        btn.setUIID("btn");
        btn.getStyle().setBgTransparency(200);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(50, 20, 300, 300);
        btn.getPressedStyle().setBgTransparency(155);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(50, 20, 300, 300);
        btn.getSelectedStyle().setBgTransparency(155);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(50, 20, 300, 300);
        return btn;
    }


}