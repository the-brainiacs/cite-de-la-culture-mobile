/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.Demandesponsoring;
import static com.mycompany.MyApplication.theme;
import com.mycompany.Service.DemandeService;
import java.util.List;

/**
 *
 * @author nawre
 */
public class AffichageDemandeForm {

    Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
    int size = Display.getInstance().convertToPixels(4);
    Form hi = new Form("Demande Sponsoring", new BoxLayout(BoxLayout.Y_AXIS));

    public AffichageDemandeForm() {

        DemandeService dt = new DemandeService();
        List<Demandesponsoring> s = dt.getAllDemandeValide();

        for (int i = 0; i < s.size(); i++) {
            final Demandesponsoring act = s.get(i);

            Container contact = new Container(new BorderLayout());

            Label titre = new Label(act.getType());

            contact.add(BorderLayout.NORTH, titre);
            ImageViewer image = new ImageViewer();
            EncodedImage placeholder = EncodedImage.createFromImage(
                    theme.getImage("Cite-de-la-Culture.jpg"), true);
            URLImage uRLImage = URLImage.createToStorage(placeholder,
                    "imageFromServer",
                    "http://localhost/images/logo.jpg");

            image.setImage(uRLImage);
            SpanLabel contenu = new SpanLabel();
            contenu.setText(act.getPackage1());
            Button btnOk = new Button("Consulter ...");

            btnOk.addPointerPressedListener((evt) -> {
                DetailsDemandeForm a = new DetailsDemandeForm(act);
                a.getF().show();

            });
            //titre.setFocusable(true);

            //  contenu.setTextBlockAlign(Component.BOTTOM);
            contact.add(BorderLayout.SOUTH, contenu);

            //   pubContainer.add(BorderLayout.CENTER, BorderLayout.east(new Label(act.getCategories().getName().toString())));
            //pubContainer.setLeadComponent(titre);
            hi.add(image);
            hi.add(contact);
            hi.add(btnOk);

        }
        hi.getToolbar().addCommandToLeftSideMenu("Index", FontImage.createFixed("\uecee", fnt, 0xffffff, size, size), ev -> {
            IndexForm in = new IndexForm();
            in.getIndex().show();
        });
        hi.getToolbar().addCommandToLeftSideMenu("Offres Sponsoring", FontImage.createFixed("\ue86b", fnt, 0xffffff, size, size), ev -> {
            AfficherSponsoringForm in = new AfficherSponsoringForm();
            in.getF().show();
        });
        hi.getToolbar().addCommandToLeftSideMenu("Mes Demandes", FontImage.createFixed("\uec1c", fnt, 0xffffff, size, size), ev -> {
            AffichageDemandeForm abf = new AffichageDemandeForm();
            abf.getF().show();
        });
        

        hi.getToolbar().addMaterialCommandToRightBar("", FontImage.MATERIAL_ADD, (evt) -> {
            AjouterDemandeForm a = new AjouterDemandeForm();
            a.getAjoutdemande().show();
        });
        hi.show();
    }

   public Form getF() {
        return hi;
    }

    public void setF(Form f) {
        this.hi = f;
    }

}
