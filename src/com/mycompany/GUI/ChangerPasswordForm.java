/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;

import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextComponent;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entities.User;
import com.mycompany.MyApplication;
import com.mycompany.Service.UserService;

/**
 *
 * @author missaoui
 */
public class ChangerPasswordForm {
    private Form changerPassword;
    private Resources theme;
    private TextComponent oldPassword, newPassword, newPassword2;
    private Container cnt,cnt2, cnt3;
    private Label cp;
    private Button btn;
    
    public ChangerPasswordForm()
    {
        theme = UIManager.initFirstTheme("/theme");
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(4);
        FontImage fm = FontImage.createFixed("\uecc6", fnt, 0xffffff, size, size);
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        changerPassword = new Form("Changer Password", new FlowLayout(Container.CENTER, Container.CENTER));
        changerPassword.getToolbar().addCommandToLeftBar("Back", fm, e -> {
            ProfileForm ind = new ProfileForm();
            ind.getProfile().show();
        });
        cnt = new Container(BoxLayout.y());
        cnt2 = new Container(new FlowLayout(Container.CENTER, Container.CENTER));
        cnt3 = new Container(BoxLayout.y());
        cnt3.getAllStyles().setBorder(Border.createLineBorder(3, 0xE6763A));
        cnt3.getAllStyles().setMargin(0, 0,20,20);
        cp = createForFont(largeBoldSystemFont, "Changer Password");
        cp.getAllStyles().setFgColor(0xE6763A);
        oldPassword = new TextComponent().label("Password Actuel :");
        oldPassword.getField().setConstraint(TextField.PASSWORD);
        newPassword = new TextComponent().label("Nouveau Password :");
        newPassword.getField().setConstraint(TextField.PASSWORD);
        newPassword2 = new TextComponent().label("Répéter Nouveau Password :");
        newPassword2.getField().setConstraint(TextField.PASSWORD);
        btn = new Button("Changer",FontImage.createFixed("\uea42", fnt, 0xffffff, size, size));
        btn = setButton(btn, 0xE6763A, 0xffffff);
        btn.addActionListener(e -> {
            if(verif()){
            UserService us = new UserService();
            User u = us.changerPassword(MyApplication.CurrentUser.getId(), oldPassword.getText(), newPassword.getText());
            if(u!=null) {
                Dialog.show("Modification Réussie","Le mot de passe a été changé sans échec","OK",null);
                MyApplication.CurrentUser = u;
                ProfileForm pf = new ProfileForm();
                pf.getProfile().show();
            } else {
                Dialog.show("Modification Echec", "Votre password actuel est incorrect. Veuillez le vérifier.", "OK", null);
            }
        }
        });
        cnt.add(cnt2);
        cnt2.add(cp);
        cnt3.add(oldPassword);
        cnt3.add(newPassword);
        cnt3.add(newPassword2);
        cnt3.add(btn);
        cnt.add(cnt3);
        changerPassword.add(cnt);
        changerPassword.setEditOnShow(oldPassword.getField());
    }
    
    public Form getChangerPassword()
    {
        return changerPassword;
    }
    
    private Button setButton(Button btn, int color1, int color2)
    {
        btn.setUIID("btn");
        btn.getStyle().setBgTransparency(255);
        btn.getStyle().setBgColor(color1);
        btn.getStyle().setFgColor(color2);
        btn.getStyle().setAlignment(TextField.CENTER);
        btn.getStyle().setMargin(50,20,300,300);
        btn.getPressedStyle().setBgTransparency(175);
        btn.getPressedStyle().setBgColor(color1);
        btn.getPressedStyle().setFgColor(color2);
        btn.getPressedStyle().setAlignment(TextField.CENTER);
        btn.getPressedStyle().setMargin(50,20,300,300);
        btn.getSelectedStyle().setBgTransparency(175);
        btn.getSelectedStyle().setBgColor(color1);
        btn.getSelectedStyle().setFgColor(color2);
        btn.getSelectedStyle().setAlignment(TextField.CENTER);
        btn.getSelectedStyle().setMargin(50,20,300,300);
        return btn;
    }
    
    private Label createForFont(Font fnt, String s) {
        Label l = new Label(s);
        l.getAllStyles().setFont(fnt);
        return l;
    }
    
    public boolean verif() {
        boolean bool = true;
        oldPassword.errorMessage("");
        newPassword.errorMessage("");
        newPassword2.errorMessage("");
        if (oldPassword.getText().equals("")) {
            bool = false;
            oldPassword.errorMessage("*Password Actuel est requis");
        }
        if (newPassword.getText().equals("")) {
            bool = false;
            newPassword.errorMessage("*Nouveau Password est requis");
        }
        if (!newPassword2.getText().equals(newPassword.getText())) {
            bool = false;
            newPassword2.errorMessage("*Différent Du Nouveau Password");
        }
        changerPassword.revalidate();
        return bool;
    }
    
}
